package com.aribot.bot.api.interfaces;

import com.aribot.bot.api.wrappers.Tile;

public interface Locatable {
    
    /**
     * Gets the Tile location
     * @return Tile
     */
    public Tile getLocation();
    
}
