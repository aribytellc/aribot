package com.aribot.bot.api.interfaces;

public interface Recognizable {
    
    /**
     * Gets the ID
     * @return int
     */
    public int getId();
    
}
