package com.aribot.bot.api.interfaces;

import java.awt.Graphics;

import com.aribot.bot.api.methods.Model;

public interface Paintable {
    
    /**
     * Gets the Model object
     * @return Model object
     */
    public Model getModel();
    
    /**
     * Draw it
     * @param g Graphics context to draw on
     */
    public void draw(Graphics g);
    
}
