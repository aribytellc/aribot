package com.aribot.bot.api.interfaces;

import java.awt.Point;


public interface Clickable {
    
    /**
     * Gets a random point
     * @return Point
     */
    public Point getRandomPoint();
    
    /**
     * Interacts
     * @param action String action to use
     * @return boolean, true on success
     */
    public boolean interact(String action);
    
    /**
     * Interacts with option
     * @param action String action to use
     * @param option String option to use
     * @return boolean, true on success
     */
    public boolean interact(String action, String option);
    
}
