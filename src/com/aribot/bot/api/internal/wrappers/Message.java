package com.aribot.bot.api.internal.wrappers;

import java.util.EventListener;

import com.aribot.bot.api.internal.events.Event;
import com.aribot.bot.events.Multicaster;

public class Message extends Event {

	private static final long serialVersionUID = 251825286143986366L;
	private int ID;
	private String sender;
	private String message;
	
	/**
	 * Creates a Message object
	 * @param ID The ID that sent the message (Corresponds with Chat ints)
	 * @param sender String name who sent it
	 * @param message String message contents
	 */
	public Message(int ID, String sender, String message) {
		this.ID = ID;
		this.sender = sender;
		this.message = message;
	}
	
	/**
	 * Gets the ID of the message
	 * @return int (Corresponds with Chat ints)
	 */
	public int getID() {
		return this.ID;
	}
	
	/**
	 * Gets the sender
	 * @return Sender name (For server, name is "")
	 */
	public String getSender() {
		return this.sender;
	}
	
	/**
	 * Gets the message
	 * @return message
	 */
	public String getMessage() {
		return this.message;
	}
	
	/**
	 * toString
	 * @return String corresponding to the message in the format of "%i : %s : %s", ID, sender, message
	 */
	@Override
	public String toString() {
		return ID + " : " + sender + " : " + message;
	}

	/**
	 * Internal use
	 */
	@Override
	public void dispatch(EventListener el) {
		((MessageListener) el).messageInbound(this);
	}

	/**
	 * Internal use
	 */
	@Override
	public long getMask() {
		return Multicaster.MESSAGE_EVENT;
	}
	
}
