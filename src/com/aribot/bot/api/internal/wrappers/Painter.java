package com.aribot.bot.api.internal.wrappers;

import java.awt.Graphics;
import java.util.EventListener;

public interface Painter extends EventListener {

	/**
	 * Paint event
	 * @param graphics Graphics object to use when fired
	 */
	public void onPaint(Graphics graphics);
	
}
