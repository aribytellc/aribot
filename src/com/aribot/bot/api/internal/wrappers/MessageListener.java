package com.aribot.bot.api.internal.wrappers;

import java.util.EventListener;


public interface MessageListener extends EventListener {
	
	/**
	 * Abstract of a Message event
	 * @param e Message object created when fired
	 */
	abstract void messageInbound(Message e);
	
}
