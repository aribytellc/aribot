package com.aribot.bot.api.internal.events;

import java.util.EventListener;
import java.util.EventObject;

public abstract class Event extends EventObject {

	private static final long serialVersionUID = 3141289780573435937L;
	private static final Object SOURCE = new Object();

	public Event() {
		super(Event.SOURCE);
	}

	public abstract void dispatch(EventListener el);

	public abstract long getMask();
}
