package com.aribot.bot.api.internal.events;

import java.applet.Applet;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.io.Canvas;
import com.aribot.bot.interfaces.io.Keyboard;
import com.aribot.bot.interfaces.io.Mouse;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class InputManager {
    
    private final static java.util.Random random = new java.util.Random();
    private static byte dragLength = 0;
    public static int side = random(1, 5);
    
    private static boolean isOnCanvas(final int x, final int y) {
	return x > 0 && x < GameLoader.getApplet().getWidth() && y > 0 && y < GameLoader.getApplet().getHeight();
    }
    
    public static boolean clickMouse(final boolean left) {
	GameLoader.getClient().getMouse();
	if (!Mouse.isPresent()) {
	    return false;
	}
	pressMouse(getX(), getY(), left);
	sleepNoException(random(50, 100));
	releaseMouse(getX(), getY(), left);
	return true;
    }
    
    
    public static void dragMouse(final int x, final int y) {
	pressMouse(getX(), getY(), true);
	sleepNoException(random(100, 500));
	moveMouseSub(getX(), getY(), x, y);
	sleepNoException(random(100, 500));
	releaseMouse(x, y, true);
    }
    
    public static void gainFocus() {
	final Canvas cw = getCanvasWrapper();
	if (!cw.hasFocus()) {
	    cw.setFocused(true);
	}
    }
    
    private static Canvas getCanvasWrapper() {
	return (Canvas) getTarget().getComponent(0);
    }
    
    private static Client getClient() {
	return GameLoader.getClient();
    }
    
    public static char getKeyChar(final char c) {
	if (c >= 36 && c <= 40) {
	    return KeyEvent.VK_UNDEFINED;
	}
	return c;
    }
    
    public static Applet getTarget() {
	return GameLoader.getApplet();
    }
    
    public static int getX() {
	return Mouse.getX();
    }
    
    public static int getY() {
	return Mouse.getY();
    }
    
    public static void holdKey(final int keyCode, final int time) {
	KeyEvent ke = new KeyEvent(getTarget(), KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, keyCode, (char) keyCode);
	getClient().getKeyboard().keyPressed(ke);
	if (time > 500) {
	    ke = new KeyEvent(getTarget(), KeyEvent.KEY_PRESSED, System.currentTimeMillis() + 500, 0, keyCode, (char) keyCode);
	    getClient().getKeyboard().keyPressed(ke);
	    final int ms2 = time - 500;
	    for (int i = 37; i < ms2; i += random(20, 40)) {
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_PRESSED, System.currentTimeMillis() + i + 500, 0, keyCode, (char) keyCode);
		getClient().getKeyboard().keyPressed(ke);
	    }
	}
	final int delay2 = time + random(-30, 30);
	ke = new KeyEvent(getTarget(), KeyEvent.KEY_RELEASED, System.currentTimeMillis() + delay2, 0, keyCode, (char) keyCode);
	getClient().getKeyboard().keyReleased(ke);
    }
    
    public static void hopMouse(final int x, final int y) {
	moveMouse(x, y);
    }
    
    private static void loseFocus() {
	final Canvas cw = getCanvasWrapper();
	if (cw.hasFocus()) {
	    cw.setFocused(false);
	}
    }
    
    private static void moveMouse(final int x, final int y) {
	if (Mouse.isPressed()) {
	    final MouseEvent me = new MouseEvent(getTarget(), MouseEvent.MOUSE_DRAGGED, System.currentTimeMillis(), 0, x, y, 0, false);
	    getClient().getMouse().sendEvent(me);
	    if ((dragLength & 0xFF) != 0xFF) {
		dragLength++;
	    }
	}
	
	if (!Mouse.isPresent()) {
	    if (isOnCanvas(x, y)) { // Entered
		final MouseEvent me = new MouseEvent(getTarget(), MouseEvent.MOUSE_ENTERED, System.currentTimeMillis(), 0, x, y, 0, false);
		getClient().getMouse().sendEvent(me);
	    }
	} else if (!isOnCanvas(x, y)) {
	    final MouseEvent me = new MouseEvent(getTarget(), MouseEvent.MOUSE_EXITED, System.currentTimeMillis(), 0, x, y, 0, false);
	    getClient().getMouse().sendEvent(me);
	    final int w = GameLoader.getApplet().getWidth(), h = GameLoader.getApplet().getHeight(), d = 50;
	    if (x < d) {
		if (y < d) {
		    side = 4; // top
		} else if (y > h + d) {
		    side = 2; // bottom
		} else {
		    side = 1; // left
		}
	    } else if (x > w) {
		side = 3;
	    } else {
		side = random(1, 5);
	    }
	} else {
	    getClient().getMouse();
	    if (!Mouse.isPressed()) {
		final MouseEvent me = new MouseEvent(getTarget(), MouseEvent.MOUSE_MOVED, System.currentTimeMillis(), 0, x, y, 0, false);
		getClient().getMouse().sendEvent(me);
	    }
	}
    }
    
    public static void moveMouse(final int x, final int y, final int randomX, final int randomY) {
	moveMouse(MouseHandler.DEFAULT_MOUSE_SPEED, x, y, randomX, randomY);
    }
    
    public static boolean moveMouse(final int speed, final int x, final int y, final int randomX, final int randomY) {
	int thisX = getX();
	int thisY = getY();
	if (!isOnCanvas(thisX, thisY)) {
	    switch (side) {
		case 1:
		    thisX = -1;
		    thisY = random(0, GameLoader.getApplet().getHeight());
		    break;
		case 2:
		    thisX = random(0, GameLoader.getApplet().getWidth());
		    thisY = GameLoader.getApplet().getHeight() + 1;
		    break;
		case 3:
		    thisX = GameLoader.getApplet().getWidth() + 1;
		    thisY = random(0, GameLoader.getApplet().getHeight());
		    break;
		case 4:
		    thisX = random(0, GameLoader.getApplet().getWidth());
		    thisY = -1;
		    break;
	    }
	}
	return moveMouseSub(speed, thisX, thisY, random(x, x + randomX), random(y, y + randomY));
    }
    
    public static void pressKey(final char ch) {
	KeyEvent ke;
	ke = new KeyEvent(getTarget(), KeyEvent.KEY_PRESSED, System.currentTimeMillis(), 0, ch, getKeyChar(ch));
	Client c = getClient();
	Keyboard k = c.getKeyboard();
	k.keyPressed(ke);
    }
    
    private static void pressMouse(final int x, final int y, final boolean left) {
	getClient().getMouse();
	getClient().getMouse();
	if (Mouse.isPressed() || !Mouse.isPresent()) {
	    return;
	}
	final MouseEvent me = new MouseEvent(getTarget(), MouseEvent.MOUSE_PRESSED, System.currentTimeMillis(), 0, x, y, 1, false, left ? MouseEvent.BUTTON1 : MouseEvent.BUTTON3);
	getClient().getMouse().sendEvent(me);
    }
    
    public static int random(final int min, final int max) {
	final int n = Math.abs(max - min);
	return Math.min(min, max) + (n == 0 ? 0 : random.nextInt(n));
    }
    
    public static void releaseKey(final char ch) {
	KeyEvent ke = new KeyEvent(getTarget(), KeyEvent.KEY_RELEASED, System.currentTimeMillis(), InputEvent.ALT_DOWN_MASK, ch, getKeyChar(ch));
	getClient().getKeyboard().keyReleased(ke);
    }
    
    private static void releaseMouse(final int x, final int y, final boolean leftClick) {
	if (!Mouse.isPressed()) {
	    return;
	}
	MouseEvent me = new MouseEvent(getTarget(), MouseEvent.MOUSE_RELEASED, System.currentTimeMillis(), 0, x, y, 1, false, leftClick ? MouseEvent.BUTTON1 : MouseEvent.BUTTON3);
	getClient().getMouse().sendEvent(me);
	
	if ((dragLength & 0xFF) <= 3) {
	    me = new MouseEvent(getTarget(), MouseEvent.MOUSE_CLICKED, System.currentTimeMillis(), 0, x, y, 1, false, leftClick ? MouseEvent.BUTTON1 : MouseEvent.BUTTON3);
	    getClient().getMouse().sendEvent(me);
	}
	dragLength = 0;
    }
    
    public static void sendKey(final char c) {
	sendKey(c, 0);
    }
    
    private static void sendKey(final char ch, final int delay) {
	boolean shift = false;
	int code = ch;
	if (ch >= 'a' && ch <= 'z') {
	    code -= 32;
	} else if (ch >= 'A' && ch <= 'Z') {
	    shift = true;
	}
	KeyEvent ke;
	if (code == KeyEvent.VK_LEFT || code == KeyEvent.VK_UP || code == KeyEvent.VK_DOWN) {
	    ke = new KeyEvent(getTarget(), KeyEvent.KEY_PRESSED, System.currentTimeMillis() + delay, 0, code, getKeyChar(ch), KeyEvent.KEY_LOCATION_STANDARD);
	    getClient().getKeyboard().keyPressed(ke);
	    final int delay2 = random(50, 120) + random(0, 100);
	    ke = new KeyEvent(getTarget(), KeyEvent.KEY_RELEASED, System.currentTimeMillis() + delay2, 0, code, getKeyChar(ch), KeyEvent.KEY_LOCATION_STANDARD);
	    getClient().getKeyboard().keyReleased(ke);
	} else {
	    if (!shift) {
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_PRESSED, System.currentTimeMillis() + delay, 0, code, getKeyChar(ch), KeyEvent.KEY_LOCATION_STANDARD);
		getClient().getKeyboard().keyPressed(ke);
		// Event Typed
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_TYPED, System.currentTimeMillis() + 0, 0, 0, ch, 0);
		getClient().getKeyboard().keyTyped(ke);
		// Event Released
		final int delay2 = random(50, 120) + random(0, 100);
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_RELEASED, System.currentTimeMillis() + delay2, 0, code, getKeyChar(ch), KeyEvent.KEY_LOCATION_STANDARD);
		getClient().getKeyboard().keyReleased(ke);
	    } else {
		// Event Pressed for shift key
		final int s1 = random(25, 60) + random(0, 50);
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_PRESSED, System.currentTimeMillis() + s1, InputEvent.SHIFT_DOWN_MASK, KeyEvent.VK_SHIFT, (char) KeyEvent.VK_UNDEFINED, KeyEvent.KEY_LOCATION_LEFT);
		getClient().getKeyboard().keyPressed(ke);
		
		// Event Pressed for char to send
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_PRESSED, System.currentTimeMillis() + delay, InputEvent.SHIFT_DOWN_MASK, code, getKeyChar(ch), KeyEvent.KEY_LOCATION_STANDARD);
		getClient().getKeyboard().keyPressed(ke);
		// Event Typed for char to send
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_TYPED, System.currentTimeMillis() + 0, InputEvent.SHIFT_DOWN_MASK, 0, ch, 0);
		getClient().getKeyboard().keyTyped(ke);
		// Event Released for char to send
		final int delay2 = random(50, 120) + random(0, 100);
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_RELEASED, System.currentTimeMillis() + delay2, InputEvent.SHIFT_DOWN_MASK, code, getKeyChar(ch), KeyEvent.KEY_LOCATION_STANDARD);
		getClient().getKeyboard().keyReleased(ke);
		
		// Event Released for shift key
		final int s2 = random(25, 60) + random(0, 50);
		ke = new KeyEvent(getTarget(), KeyEvent.KEY_RELEASED, System.currentTimeMillis() + s2, InputEvent.SHIFT_DOWN_MASK, KeyEvent.VK_SHIFT, (char) KeyEvent.VK_UNDEFINED, KeyEvent.KEY_LOCATION_LEFT);
		getClient().getKeyboard().keyReleased(ke);
	    }
	}
    }
    
    public static void sendKeys(final String text, final boolean pressEnter) {
	sendKeys(text, pressEnter, 50, 100);
    }
    
    public void sendKeys(final String text, final boolean pressEnter, final int delay) {
	sendKeys(text, pressEnter, delay, delay);
    }
    
    public static void sendKeys(final String text, final boolean pressEnter, final int minDelay, final int maxDelay) {
	final char[] chs = text.toCharArray();
	for (final char element : chs) {
	    sendKey(element, random(minDelay, maxDelay));
	    sleepNoException(random(minDelay, maxDelay));
	}
	if (pressEnter) {
	    sendKey((char) KeyEvent.VK_ENTER, random(minDelay, maxDelay));
	}
    }
    
    public static void sendKeysInstant(final String text, final boolean pressEnter) {
	for (final char c : text.toCharArray()) {
	    sendKey(c, 0);
	}
	if (pressEnter) {
	    sendKey((char) KeyEvent.VK_ENTER, 0);
	}
    }
    
    public static void sleepNoException(final long t) {
	try {
	    Thread.sleep(t);
	} catch (final Exception ignored) {
	}
    }
    
    public static boolean moveMouseSub(final int curX, final int curY, final int targetX, final int targetY) {
	return moveMouseSub(MouseHandler.DEFAULT_MOUSE_SPEED, curX, curY, targetX, targetY);
    }
    
    public static boolean moveMouseSub(final int speed, final int curX, final int curY, final int targetX, final int targetY) {
	return MouseHandler.moveMouse(speed, curX, curY, targetX, targetY, 0, 0);
    }
}
