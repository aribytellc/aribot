package com.aribot.bot.api.wrappers;

import com.aribot.bot.api.methods.Model;
import com.aribot.bot.interfaces.Animable;
import com.aribot.bot.interfaces.Models;

public class Animables {
	private final Animable accessor;

	public Animables(Animable accessor) {
		this.accessor = accessor;
	}

	/**
	 * Gets the model height
	 * @return int
	 */
	public int getModelHeight() {
		return this.accessor.getModelHeight() * this.accessor.getMultiplierModelHeight();
	}

	/**
	 * Gets the model object
	 * @param x World X position
	 * @param y World Y position
	 * @return Model object
	 */
	public Model getModel(int x, int y) {
		return new Model((Models) this.accessor, x, y);
	}
}