package com.aribot.bot.api.wrappers;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;

import com.aribot.bot.api.interfaces.Clickable;
import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.methods.walking.Path;
import com.aribot.bot.api.util.Random;

public class Tile implements Clickable {
    
    private final int x;
    private final int y;
    private final int z;
    
    public static interface Flag {
	public static final int NW = 0x1;
	public static final int N = 0x2;
	public static final int NE = 0x4;
	public static final int E = 0x8;
	public static final int SE = 0x10;
	public static final int S = 0x20;
	public static final int SW = 0x40;
	public static final int W = 0x80;
	public static final int BLOCKED = 0x100;
	public static final int WATER = 0x1280100;
    }
    
    /**
     * Creates a Tile object (Z is defined as current plane)
     * @param x X location
     * @param y Y location
     */
    public Tile(final int x, final int y) {
	this(x, y, Game.getPlane());
    }
    
    /**
     * Creates a Tile object
     * @param x X location
     * @param y Y location
     * @param z Z location
     */
    public Tile(final int x, final int y, final int z) {
	this.x = x;
	this.y = y;
	this.z = z;
    }
    
    /**
     * Creates a Tile object
     * @param x X location
     * @param y Y location
     * @param z Z location
     * @param offsets Boolean, true if the values given are offsets from the current mapbase, false acts just like (x, y, z)
     */
    public Tile(final int x, final int y, final int z, final boolean offsets) {
	if (offsets) {
	    this.x = Game.getMapBaseX() + x;
	    this.y = Game.getMapBaseY() + y;
	    this.z = z;
	} else {
	    this.x = x;
	    this.y = y;
	    this.z = z;
	}
    }
    
    /**
     * Gets the Tile X position
     * @return int
     */
    public int getX() {
	return x;
    }
    
    /**
     * Gets the X offset (Can malfunction if not in current mapbase)
     * @return int
     */
    public int getXOffset() {
	return x - Game.getMapBaseX();
    }
    
    /**
     * Gets the Raw X offset. Useful only for internal use.
     * @return int
     */
    public int getRawXOffset() {
	return ((x - Game.getMapBaseX()) << 7);
    }
    
    /**
     * Gets the Tile Y position
     * @return int
     */
    public int getY() {
	return y;
    }
    
    /**
     * Gets the Y offset (Can malfunction if not in current mapbase)
     * @return int
     */
    public int getYOffset() {
	return y - Game.getMapBaseY();
    }
    
    /**
     * Gets the Raw Y offset. Useful only for internal use.
     * @return int
     */
    public int getRawYOffset() {
	return ((y - Game.getMapBaseY()) << 7);
    }
    
    /**
     * Gets the Tile Z position
     * @return int
     */
    public int getZ() {
	return z;
    }
    
    /**
     * Gets the Tile Flag (Can malfunction if not in current mapbase)
     * @return int
     */
    public int getFlag() {
	return Game.getTileFlags(z)[getXOffset()][getYOffset()];
    }
    
    /**
     * Is Tile Questionable (Tile has directional flags)
     * @return boolean
     */
    public boolean isQuestionable() {
	return (getFlag() & (Flag.NW | Flag.N | Flag.NE | Flag.E | Flag.SE | Flag.S | Flag.SW | Flag.W)) != 0;
    }
    
    /**
     * Is Tile walkable (Tile has Blocked or Water flags)
     * @return boolean
     */
    public boolean isWalkable() {
	return (getFlag() & (Flag.BLOCKED | Flag.WATER)) == 0;
    }
    
    /**
     * Is this Tile adjacent to another
     * @param other Tile
     * @return boolean
     */
    public boolean isAdjacentTo(Tile other) {
	int dx = other.getX() - this.getX();
	int dy = other.getY() - this.getY();
	return Math.abs(dx) <= 1 && Math.abs(dy) <= 1;
    }
    
    /**
     * Is this Tile Diagonal to another
     * @param other Tile
     * @return boolean
     */
    public boolean isDiagonalTo(Tile other) {
	int dx = other.getX() - this.getX();
	int dy = other.getY() - this.getY();
	return Math.abs(dx) == 1 && Math.abs(dy) == 1;
    }
    
    /**
     * Determines whether a Tile can be crossed in a given direction.
     * 
     * @param flag A Flag constant
     * @return true/false whether or not the above descripter is met.
     */
    public boolean isBlockedAt(int flag) {
	return (getFlag() & flag) != 0;
    }
    
    /**
     * Checks if another tile can navigate to this tile without any obstacles.
     * 
     * @param other Another (adjacent) tile to check.
     * @return true if there is no obstacle between the given tile and this tile. false otherwise.
     */
    public boolean canWalkTo(Tile other) {
	boolean blockedTo = isBlocked(this, other);
	boolean blockedFrom = isBlocked(other, this);
	return other.isWalkable() && !blockedTo && !blockedFrom;
    }
    
    /**
     * Can we reach this Tile
     * @return boolean
     */
    public boolean canReach() {
	return Path.generatePath(Players.getMyPlayer().getLocation(), this).toTileArray() != null;
    }
    
    /**
     * Checks if the flag for t1 is blocked in the direction towards t2.
     * If diagonal, checks if the vert/horiz tile is walkable as well. If it isn't, then you cannot walk diagonally.
     * 
     * @param t1
     * @param t2
     * @return boolean
     */
    private static boolean isBlocked(Tile t1, Tile t2) {
	if (!t1.isAdjacentTo(t2)) {
	    System.err.println("Comparing two non-adjacent Tiles");
	    return true;
	}
	int dx = t2.getX() - t1.getX();
	int dy = t2.getY() - t1.getY();
	switch(dx) {
	    case -1: //West
		switch(dy) {
		    case -1: //Southwest
			return t1.derive(-1, 0).isWalkable() && t1.derive(0, -1).isWalkable() && t1.isBlockedAt(Flag.SW) || t1.isBlockedAt(Flag.S) || t1.isBlockedAt(Flag.W) ;
		    case 0: //West
			return t1.isBlockedAt(Flag.W);
		    case 1: //Northwest
			return !t1.derive(0, 1).isWalkable() || !t1.derive(-1, 0).isWalkable() ||  t1.isBlockedAt(Flag.NW) || t1.isBlockedAt(Flag.N) || t1.isBlockedAt(Flag.W);
		}
	    case 0: //North or South
		switch(dy) {
		    case -1: //South
			return t1.isBlockedAt(Flag.S);
		    case 0:
			return false; //Self
		    case 1: //North
			return t1.isBlockedAt(Flag.N);
		}
	    case 1: //East
		switch(dy) {
		    case -1: //Southeast
			return !t1.derive(0, -1).isWalkable() || !t1.derive(1, 0).isWalkable() || t1.isBlockedAt(Flag.SE) || t1.isBlockedAt(Flag.S) || t1.isBlockedAt(Flag.E);
		    case 0: //East
			return t1.isBlockedAt(Flag.E);
		    case 1: //Northeast
			return !t1.derive(0, 1).isWalkable() || !t1.derive(1, 0).isWalkable() ||  t1.isBlockedAt(Flag.NE) || t1.isBlockedAt(Flag.N) || t1.isBlockedAt(Flag.E);
		}
	}
	return false;
    }
    
    /**
     * Is Tile Special (Tile has Blocked and Water)
     * @return boolean
     */
    public boolean isSpecial() {
	return (getFlag() & Flag.BLOCKED) == 0 && (getFlag() & Flag.WATER) != 0;
    }
    
	/**
	 * IS this Tile reachable from our current position
	 * @return boolean, true if possible
	 */
    public boolean isReachable() {
	return Path.generatePath(Players.getMyPlayer().getLocation(), this).toTileArray() != null;
    }
    
    /**
     * Is Tile on screen (Should be whether the center of the tile is on screen, but needs testing)
     * @return boolean
     */
    public boolean isOnScreen() {
		return Calculations.isOnScreen(Calculations.tileToScreen(this, 0.5, 0.5, getZ())); // Should be the center of the tile. Need to test.
    }
    
    /**
     * @param point A Point to check if this tile contains.
     * @return true/false whether the given point falls within this tiles bounds.
     */
    public boolean contains(Point point) {
	Polygon bounds = getBounds();
	return bounds.npoints == 4 && bounds.contains(point);
    }
    
    /**
     * Derive the Tile with X and Y shift
     * @param xShift int
     * @param yShift int
     * @return Tile
     */
    public Tile derive(int xShift, int yShift) {
	return new Tile(this.getX() + xShift, this.getY() + yShift, this.getZ());
    }
    
    /**
     * @return A Polygon binding the tile based on its 4 corners.
     */
    public Polygon getBounds() {
	final Tile nw = this;
	final Tile ne = this.derive(1, 0);
	final Tile sw = this.derive(0, 1);
	final Tile se = this.derive(1, 1);
	
	final Point pnw = Calculations.tileToScreen(nw, 0, 0, nw.getZ());
	final Point pne = Calculations.tileToScreen(ne, 0, 0, nw.getZ());
	final Point psw = Calculations.tileToScreen(sw, 0, 0, nw.getZ());
	final Point pse = Calculations.tileToScreen(se, 0, 0, nw.getZ());
	
	if (Calculations.isOnScreen(pnw) && Calculations.isOnScreen(pne) && Calculations.isOnScreen(psw) && Calculations.isOnScreen(pse)) {
	    return new Polygon(new int[] { psw.x, pse.x, pne.x, pnw.x }, new int[] { psw.y, pse.y, pne.y, pnw.y }, 4);
	}
	return new Polygon();
    }
    
    @Override
    public Point getRandomPoint() {
	    return Calculations.tileToScreen(this, Random.getRandom().nextDouble(), Random.getRandom().nextDouble(), getZ());
    }
    
    /**
     * A String representation of the Tile object
     * @return String in the format of "(X: %i, Y: %i, Z: %i)", x, y, z
     */
    @Override
    public String toString() {
	return "(X: " + x + ", Y:" + y + ", Z:" + z + ")";
    }
    
    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + x;
	result = prime * result + y;
	result = prime * result + z;
	return result;
    }

    /**
	 * Whether an object is equal to this Tile object (Matching x, y AND z)
     * @return boolean
     */
    @Override
    public boolean equals(final Object obj) {
	if (obj == this) {
	    return true;
	}
	if (obj instanceof Tile) {
	    final Tile tile = (Tile) obj;
	    return tile.x == x && tile.y == y && tile.z == z;
	}
	return false;
    }
    
    /**
     * Draw the Tile in the color: Color(0, 0, 0, 155)
     * @param g Graphics context
     */
    public void draw(Graphics g) {
	draw(g, new Color(0, 0, 0, 155), null, "");
    }
    
    /**
     * Draw the Tile in the color specified with colored text next to it.
     * @param g Graphics context
     * @param c Color to paint it.
     * @param textColor Color to put the text in, defaults to Green
     * @param text Text to put on the side of the tile
     */
    public void draw(Graphics g, Color c, Color textColor, String... text) {
	final Tile nw = this;
	final Tile ne = this.derive(1, 0);
	final Tile sw = this.derive(0, 1);
	final Tile se = this.derive(1, 1);
	
	final Point pnw = Calculations.tileToScreen(nw, 0, 0, nw.getZ());
	final Point pne = Calculations.tileToScreen(ne, 0, 0, nw.getZ());
	final Point psw = Calculations.tileToScreen(sw, 0, 0, nw.getZ());
	final Point pse = Calculations.tileToScreen(se, 0, 0, nw.getZ());
	if (pnw.x != -1 && pnw.y != -1 && pne.x != -1 && pne.y != -1 && psw.x != -1 && psw.y != -1 && pse.x != -1 && pse.y != -1) {
	    if (Calculations.isOnScreen(pnw) && Calculations.isOnScreen(pne) && Calculations.isOnScreen(psw) && Calculations.isOnScreen(pse)) {
		Color OC = g.getColor();
		if (c != null) {
		    g.setColor(new Color(0, 0, 0, 255));
		    g.drawPolygon(new int[] { psw.x, pse.x, pne.x, pnw.x }, new int[] { psw.y, pse.y, pne.y, pnw.y }, 4);
		    g.setColor(c);
		    g.fillPolygon(new int[] { psw.x, pse.x, pne.x, pnw.x }, new int[] { psw.y, pse.y, pne.y, pnw.y }, 4);
		}
		g.setColor(((textColor != null) ? textColor : Color.GREEN));
		for (int i = 0; i < text.length; i++) {
		    g.drawString(text[i], Math.max(Math.max(pnw.x, pne.x), Math.max(psw.x, pse.x)) + 5, Math.min(Math.min(pnw.y, pne.y), Math.min(psw.y, pse.y)) + ((i * 15) + 15)); 
		}
		g.setColor(OC);
	    }
	}
    }
    
    @Override
    public boolean interact(String action) {
	return Mouse.interact(this, action, null);
    }
    
    @Override
    public boolean interact(String action, String option) {
	return Mouse.interact(this, action, option);
    }
}
