package com.aribot.bot.api.wrappers;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;

import com.aribot.bot.api.interfaces.Locatable;
import com.aribot.bot.api.interfaces.Recognizable;
import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.methods.Model;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.methods.nodes.Menu;
import com.aribot.bot.interfaces.Animable;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.Models;
import com.aribot.bot.interfaces.ObjectDefinition;
import com.aribot.bot.interfaces.SceneObject;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Interactable implements Locatable, Recognizable {
    private final SceneObject accessor;
    private static final Client c = GameLoader.getClient();
    
    public Interactable(SceneObject accessor) {
	this.accessor = accessor;
    }
    
    /**
     * Gets the hash of the object
     * @return int
     */
    public int getHash() {
	return (this.accessor.getHash() * this.accessor.getMultiplierHash());
    }
    
    /**
     * Gets the ID of the object
     * @return int
     */
    @Override
    public int getId() {
	return (this.accessor.getHash() * this.accessor.getMultiplierHash()) >> 14 & 0x7FFF;
    }
    
    /**
     * Gets the X of the object
     * @return int
     */
    public int getX() {
	return ((this.accessor.getHash() * this.accessor.getMultiplierHash()) & 0x7F) + (Game.getMapBaseX());
    }
    
    /**
     * Gets the Y of the object
     * @return int
     */
    public int getY() {
	return ((this.accessor.getHash() * this.accessor.getMultiplierHash()) >> 7 & 0x7F) + (Game.getMapBaseY());
    }
    
    /**
     * Gets the plane of the object (Current plane)
     * @return int
     */
    public int getZ() {
	return Game.getPlane();
    }
    
    /**
     * Gets the height of the object
     * @return int
     */
    public int getHeight() {
	return (this.accessor.getHeight() * this.accessor.getMultiplierHeight());
    }
    
    /**
     * Gets the location of the object
     * @return Tile
     */
    @Override
    public Tile getLocation() {
	return new Tile(getX(), getY(), getZ());
    }
    
    /**
     * Gets the X grid of the object
     * @return int
     */
    public int getGridX() {
	return (this.accessor.getWorldX() * this.accessor.getMultiplierWorldX());
    }
    
    /**
     * Gets the Y grid of the object
     * @return int
     */
    public int getGridY() {
	return (this.accessor.getWorldY() * this.accessor.getMultiplierWorldY());
    }
    
    /**
     * Gets the Animable of the object
     * @return Animable
     */
    public Animable getAnimable() {
	return this.accessor.getAnimable();
    }
    
    /**
     * Gets the Model of the object
     * @return Model
     */
    public Model getModel() {
	Animable a = getAnimable();
	if (a instanceof Models) {
	    return new Animables(a).getModel(getGridX(), getGridY());
	}
	return null;
    }
    
    /**
     * Clicks the object Uses a random point on the model
     * @param left boolean left click
     */
    public void click(boolean left) {
	Model model = getModel();
	if (model == null) {
	    return;
	}
	Point p = model.getRandomPoint();
	Mouse.click(p.x, p.y, left);
    }
    
    /**
     * Clicks the object Uses the center point on the model
     * @param left boolean left click
     */
    public void clickCenter(boolean left) {
	Model model = getModel();
	if (model == null) {
	    return;
	}
	Point p = model.getPoint();
	Mouse.click(p.x, p.y, left);
    }
    
    /**
     * Interacts with an Interactable object
     * @param action String, action to execute
     * @return boolean, false on error.
     */
    public boolean interact(String action) {
	List<String> actions = Arrays.asList(getActions());
	if (actions.contains(action)) {
	    click(false);
	    return Menu.select(action);
	}
	return false;
    }
    
    /**
     * Is object on screen
     * @return boolean
     */
    public boolean isOnScreen() {
	return Calculations.isOnScreen(Calculations.tileToScreen(getLocation(), 0, 0, getLocation().getZ()));
    }
    
    /**
     * Gets the Object Definition of the object
     * @return ObjectDefinition
     */
    public ObjectDefinition getDefinition() {
	return c.getObjDef(getId());
    }
    
    /**
     * Gets the actions of the object
     * @return String array of the actions
     */
    public String[] getActions() {
	return getDefinition().getActions();
    }
    
    /**
     * Gets the name of the object
     * @return String name
     */
    public String getName() {
	return getDefinition().getName();
    }
    
}