package com.aribot.bot.api.wrappers;

import java.awt.Graphics;
import java.awt.Polygon;
import java.util.ArrayList;

import com.aribot.bot.api.interfaces.Paintable;
import com.aribot.bot.api.methods.Model;
import com.aribot.bot.interfaces.Models;
import com.aribot.bot.interfaces.PlayerDefinition;

public class Player extends Character implements Paintable {
    
    private final com.aribot.bot.interfaces.Player accessor;
    
    public Player(com.aribot.bot.interfaces.Player accessor) {
	super(accessor);
	this.accessor = accessor;
    }
    
    /**
     * Gets the player name
     * @return String name
     */
    public String getName() {
	return accessor.getName();
    }
    
    /**
     * Gets the player level
     * @return int
     */
    public int getLevel() {
	return accessor.getLevel() * accessor.getMultiplierLevel();
    }
    
    /**
     * Gets the Players definition
     * @return PlayerDefinition
     */
    public PlayerDefinition getPlayerDef() {
	return accessor.getPlayerDefinition();
    }
    
    /**
     * Gets the Players equipment (Int[] with correct IDs)
     * @return int[]
     */
    public int[] getRawEquipment() {
	int[] old = getPlayerDef().getEquipment();
	int[] fixed = new int[old.length];
	for (int i = 0; i < old.length; i++) {
	    fixed[i] = ((old[i] >= 0x200) ? (old[i] ^ 0x200) : -1);
	}
	return fixed;
    }
    
    /**
     * Gets the Players equipment
     * @return Item array
     */
    public Item[] getEquipment() {
	int[] raw = getRawEquipment();
	ArrayList<Item> equip = new ArrayList<>();
	for (int i = 0; i < raw.length; i++) {
	    if (raw[i] > 0) {
		equip.add(new Item(raw[i])); // Stack size and slot?
	    }
	}
	return equip.toArray(new Item[equip.size()]);
    }
    
    /**
     * Gets the Menu actions of a Player (Hard coded)
     * @return String array or actions
     */
    @Override
    public String[] getActions() {
	return new String[] { "Walk here", "Trade with", "Follow", "Cancel", "Attack" }; // Hard coded because these don't change and there is no field for it.
    }
    
    @Override
    public Model getModel() {
	// Gets the cache that is located in playerdef.
	// Use the lookup custom method to find a NodeSub (initial, could be Node due to path)
	// The lookup long is the ID of the player Node long getID()
	// OffsetX and Y are used for positioning the model.
	return new Model((Models) getPlayerDef().getCache().lookup(getPlayerDef().getLID() * getPlayerDef().getMultiplierLID()), rawOffsetX(), rawOffsetY(), this.accessor);
    }
    
    /**
     * Gets the NPC ID of the Player
     * @return int
     */
    public int getNPCID() {
	return getPlayerDef().getNPCID() * getPlayerDef().getMultiplierNPCID();
    }
    
    /**
     * Is this player Male or Female
     * @return boolean if Male
     */
    public boolean isMale() {
	return !getPlayerDef().isFemale();
    }
    
    @Override
    public void draw(Graphics g) {
	for (Polygon poly : this.getModel().getTriangles()) {
	    g.drawPolygon(poly);
	}
    }
}