package com.aribot.bot.api.wrappers;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;

import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.methods.nodes.Menu;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.ItemDef;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Item {

	private static Client c = GameLoader.getClient();

	private int index;
	private int stack;
	private int slot;

	/**
	 * Creates an Item object
	 * @param index The ID of the item
	 * @param stack The stack size of the item
	 * @param slot The slot number of the item
	 */
	public Item(int index, int stack, int slot) {
		this.index = index;
		this.stack = stack;
		this.slot = slot;
	}

	/**
	 * Creates an Item object. Sets slot to -1
	 * @param index The ID of the item
	 * @param stack The stack size of the item
	 */
	public Item(int index, int stack) {
		this(index, stack, -1);
	}

	/**
	 * Creates an Item object. Sets stack size to 1, and slot to -1
	 * @param index The ID of the item
	 */
	public Item(int index) {
		this(index, 1);
	}

	/**
	 * Gets the stack size of the item
	 * @return int
	 */
	public int getStackSize() {
		return stack;
	}

	/**
	 * Gets the ID of the item
	 * @return int
	 */
	public int getID() {
		return index;
	}

	/**
	 * Gets the screen location of the center of the Item / Tile  (Math calculated, no found item bounds)
	 * @return Point on screen
	 */
	public Point getScreenLocation() { // No field or set of fields for this, so some math and hard coords.
		if (slot != -1) {
			int col = slot % 4;
			int row = slot / 4;
			int x = 575 + col * 42;
			int y = 230 + row * 36;
			return new Point(x, y);
		}
		return new Point(-1, -1);
	}

	/**
	 * Gets item slot
	 * @return int
	 */
	public int getSlot() {
		return slot;
	}

	/**
	 * Get Item Definition
	 * @return ItemDef
	 */
	public ItemDef getDefinition() {
		return c.getItemDef(index);
	}

	/**
	 * Get actions of the item
	 * @return String array of actions
	 */
	public String[] getActions() {
		return getDefinition().getActions();
	}

	/**
	 * Checks if the actions contains a String
	 * @param action String to check for
	 * @return boolean, true if found
	 */
	public boolean actionsContain(String action) {
		List<String> actions = Arrays.asList(getActions());
		if (actions.contains(action)) {
			return true;
		}
		return false;
	}

	/**
	 * Get name of the item
	 * @return String name
	 */
	public String getName() {
		return getDefinition().getName();
	}

	/**
	 * Get ground actions of the item
	 * @return String array of ground actions
	 */
	public String[] getGroundActions() {
		return getDefinition().getGroundActions();
	}

	/**
	 * Checks if the ground actions contains a String
	 * @param action String to check for
	 * @return boolean, true if found
	 */
	public boolean groundActionsContain(String action) {
		List<String> actions = Arrays.asList(getGroundActions());
		if (actions.contains(action)) {
			return true;
		}
		return false;
	}

	/**
	 * Clicks the mouse on the Item object
	 * @param left boolean left or right click
	 */
	public void click(boolean left) {
		click(left, 5, 5);
	}

	/**
	 * Click the item
	 * @param left boolean left click
	 * @param randX Random x
	 * @param randY Random y
	 */
	public void click(boolean left, int randX, int randY) {
		Mouse.click(getScreenLocation(), randX, randY, left);
	}

	/**
	 * Interacts with an item.
	 * @param action String action to do
	 * @return boolean, false on fail
	 */
	public boolean interact(String action) {
		if (actionsContain(action)) {
			click(false);
			return Menu.select(action);
		}
		return false;
	}

}
