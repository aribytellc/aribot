package com.aribot.bot.api.wrappers;

import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;

import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.interfaces.Interfaces;

public class Widget {
    
    private final com.aribot.bot.interfaces.Interfaces accessor;
    
    public Widget() {
	this.accessor = null;
    }
    
    public Widget(com.aribot.bot.interfaces.Interfaces accessor) {
	this.accessor = accessor;
    }
    
    /**
     * Gets the X offset (only its own offset)
     * @return int
     */
    public int getOffsetX() {
	return accessor == null? -1 : accessor.getX() * accessor.getMultiplierX();
    }
    
    /**
     * Gets the Y offset (Only its own offset)
     * @return int
     */
    public int getOffsetY() {
	return accessor == null ? -1 : accessor.getY() * accessor.getMultiplierY();
    }
    
    /**
     * Gets the width (only its own)
     * @return int
     */
    public int getWidth() {
	return accessor == null ? -1 : accessor.getWidth() * accessor.getMultiplierWidth();
    }
    
    /**
     * Gets the height (Only its own)
     * @return int
     */
    public int getHeight() {
	return accessor == null ? -1 : accessor.getHeight() * accessor.getMultiplierHeight();
    }
    
    /**
     * Gets the parent Widget
     * @return Widget (null if non-existent)
     */
    public Widget getParent() {
	if (accessor == null) {
	    return new Widget();
	}
	if (getParentID() != -1) {
	    return Widgets.getInterface(getParentID() >> 16, getParentID() & 0xFFFF);
	}
	return null;
    }
    
    /**
     * Gets the "Step-dad" ID (<u>149</u>.0)
     * @return int
     */
    public int getStepDadID() {
	return accessor == null ? -1 : getID() >> 16;
    }
    
    /**
     * Gets the short ID (149.<u>0</u>)
     * @return int
     */
    public int getShortID() {
	return accessor == null ? -1 : getID() & 0xFFFF;
    }
    
    /**
     * Gets the ID of the interface (Internal use)
     * @return int
     */
    public int getID() {
	return accessor == null ? -1 : accessor.getUID() * accessor.getMultiplierUID();
    }
    
    /**
     * Gets the Parent ID of the interface (Internal use)
     * @return int (-1 is no parent ID)
     */
    public int getParentID() {
	return accessor == null ? -1 : accessor.getParentID() * accessor.getMultiplierParentID();
    }
    
    /**
     * Gets the children of a Widget.
     * @return Widget array of children, null if children do not exist.
     */
    public Widget[] getChildren() {
	if (accessor == null) {
	    return new Widget[0];
	}
	Interfaces[] comps = accessor.getComponents();
	ArrayList<Widget> children = new ArrayList<>();
	if (comps != null) {
	    for (int i = 0; i < comps.length; i++) {
		if (comps[i] != null) {
		    children.add(new Widget(comps[i]));
		}
	    }
	    return children.toArray(new Widget[children.size()]);
	}
	return null;
    }
    
    /**
     * Checks whether this Widget is a valid widget. A valid widget exists in the game.
     * 
     * @return true if this Widget is valid, false otherwise.
     */
    public boolean isValid() {
	return this.accessor != null;
    }
    
    /**
     * Is Widget hidden
     * @return boolean
     */
    public boolean isHidden() {
	return this.isValid() && accessor.isHidden();
    }
    
    /**
     * Is Widget an inventory interface (Still unknown and does not operate as expected)
     * @return boolean
     */
    public boolean isInventory() {
	return accessor == null ? false : accessor.isInventory();
    }
    
    /**
     * Gets the final location of the interface (Own plus all parent X's and Y's to get the final position)<br />
     * (At current, there is no reliable method to determine whether an interface is in fact an inventory interface or not.
     * A list is currently used to determine this.)
     * @return Point
     */
    public Point getFinalPoint() {
	if (accessor == null) {
	    return new Point(-1, -1);
	}
	int parentID = getParentID();
	int totalX = 0;
	int totalY = 0;
	/* boolean isInventory = false; */
	try {
	    /*
	     * if (!isInventory) {
	     * isInventory = isInventory();
	     * }
	     */
	    while (parentID != -1) {
		Widget parent = Widgets.getParent(parentID);
		totalX += parent.getOffsetX();
		totalY += parent.getOffsetY();
		parentID = parent.getParentID();
		/*
		 * if (!isInventory) {
		 * isInventory = parent.isInventory();
		 * }
		 */
	    }
	    // TODO: Somehow fix this. Find a way to determine if it is an inventory parent interface or not.
	    //int[] tabWidgets = { 92, 182, 192, 239, 261, 271, 274, 320, 387, 464, 550, 551, 589 };
	    int[] tabWidgets = { 89, 90, 182, 192, 239, 261, 271, 274, 320, 387, 464, 550, 551, 589 };
	    //if (isInventory) {
	    if (Arrays.binarySearch(tabWidgets, getStepDadID()) >= 0) {
		totalX += Widgets.getInterface(548, 95).getOffsetX();
		totalY += Widgets.getInterface(548, 95).getOffsetY();
	    }
	    totalX += getOffsetX();
	    totalY += getOffsetY();
	    
	} catch (Exception e) {
	}
	return new Point(totalX, totalY);
    }
    
    /**
     * Gets the final X position (Own and all parents)
     * @return int
     */
    public int getFinalX() {
	return accessor == null ? -1 : getFinalPoint().x;
    }
    
    /**
     * Gets the final X position (Own and all parents)
     * @return int
     */
    public int getFinalY() {
	return accessor == null ? -1 : getFinalPoint().y;
    }
    
    /**
     * Gets the text on the Widget
     * @return String of text.
     */
    public String getText() {
	return accessor == null ? "" : accessor.getText();
    }
    
    /**
     * Gets the String array of actions
     * @return String array of actions, null is actions do not exist
     */
    public String[] getActions() {
	return accessor == null ? new String[0] : accessor.getActions();
    }
    
    /**
     * Gets the scroll X
     * @return int
     */
    public int getScrollX() {
	return accessor == null ? -1 : accessor.getScrollX() * accessor.getMultiplierScrollX();
    }
    
    /**
     * Gets the scroll Y
     * @return int
     */
    public int getScrollY() {
	return accessor == null ? -1 : accessor.getScrollY() * accessor.getMultiplierScrollY();
    }
    
    /**
     * Gets the texture ID
     * @return int
     */
    public int getTextureID() {
	return accessor == null ? -1 : accessor.getTextureID() * accessor.getMultiplierTextureID();
    }
    
    /**
     * Gets the model ID of the interface
     * @return int
     */
    public int getModelID() {
	return accessor == null ? -1 : accessor.getModelID() * accessor.getMultiplierModelID(); // Might be wrong field as everything is -1, and I cannot find a model in any interfaces.
    }
    
    /**
     * Gets the interface tooltip
     * @return String
     */
    public String getTooltip() {
	return accessor == null ? "" : accessor.getToolTip();
    }
    
    /**
     * Gets the name of the interface
     * @return String
     */
    public String getName() {
	return accessor == null ? "" : accessor.getName();
    }
    
    /**
     * Gets the border thickness
     * @return int
     */
    public int getBorderThickness() {
	return accessor == null ? -1 : accessor.getBorderThickness() * accessor.getMultiplierBorderThickness();
    }
    
    /**
     * Draws an outline (Final position) of the widget using supplied Graphics
     * @param g Graphics
     */
    public void drawOutline(Graphics g) {
	if (accessor == null) {
	    return;
	}
	g.drawRect(getFinalX(), getFinalY(), getWidth(), getHeight());
    }
    
    /**
     * Gets the center of the widget (Uses final position)
     * @return Point
     */
    public Point getCenter() {
	return accessor == null ? new Point(-1, -1) : new Point(getFinalX() - (getWidth() / 2), getFinalY() + (getHeight() / 2));
    }
    
    /**
     * Clicks on the center of the interface. Left clicks.
     */
    public boolean click() {
	return click(true);
    }
    
    /**
     * Clicks the center of the component
     * @param left boolean left click
     */
    public boolean click(boolean left) {
	return click(true, 0, 0);
    }
    
    /**
     * Clicks on the center of the component with specified randomness.<br>
     * If X/Y is greater than half width/height, it is set to the max (half width/height)
     * @param left boolean left click
     * @param randX int Random X to add or subtract from center
     * @param randY int Random Y to add or subtract from center
     */
    public boolean click(boolean left, int randX, int randY) {
	if (accessor == null) {
	    return false;
	}
	randX = ((randX > (getWidth() / 2)) ? (getWidth() / 2) : randX); // Negatives are possible and are taken care of, though you have to be stupid to use a neg random
	randY = ((randY > (getHeight() / 2)) ? (getHeight() / 2) : randY);
	return Mouse.click(getCenter(), randX, randY, left);
    }
    
    /**
     * Clicks at the specified X and Y positions on the component. (0, 0) is the top left of the component.
     * @param x X on the component
     * @param y Y on the component
     * @param left boolean left click
     */
    public boolean click(int x, int y, boolean left) {
	if (accessor == null) {
	    return false;
	}
	x = ((x > getWidth()) ? getWidth() : x) + getFinalX();
	y = ((y > getHeight()) ? getHeight() : y) + getFinalY();
	return Mouse.click(x, y, left);
    }
    
    /**
     * Gets the Item IDs in the Slots if present
     * @return int[]
     */
    public int[] getSlotIDs() {
	return accessor == null ? new int[0] : accessor.getSlotIDs();
    }
    
    /**
     * Gets the Item Stack Sizes if present
     * @return int[]
     */
    public int[] getStackSizes() {
	return accessor == null ? new int[0] : accessor.getStackSize();
    }
    
}
