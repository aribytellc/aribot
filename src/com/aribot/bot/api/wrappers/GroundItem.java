package com.aribot.bot.api.wrappers;

import java.awt.Point;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Model;
import com.aribot.bot.interfaces.Models;

public class GroundItem extends Item {
    
    private Tile t;
    
    /**
     * Gets the Tile that the GroundItem is located on
     * @return Tile
     */
    public Tile getLocation() {
	return t;
    }
    
    /**
     * Creates a GroundItem object
     * @param ti Tile location where GroundItem is located
     * @param ID int ID of the Item
     * @param stackSize int Stack size of the item
     */
    public GroundItem(Tile ti, int ID, int stackSize) {
	super(ID, stackSize);
	this.t = ti;
    }
    
    @Override
    public Point getScreenLocation() {
	return getModel().getRandomPoint();
    }
    
    /**
     * Gets the center of the Tile where the item is located
     * @return Point that is on the screen
     */
    public Point getCenterLocation() {
	return Calculations.tileToScreen(t, 0.5, 0.5, t.getZ());
    }
    
    /**
     * Gets the Model of the Ground Item.
     * @return Model
     */
    public Model getModel() {
	return new Model((Models) getDefinition().getCache().lookup(getID()), t.getRawXOffset(), t.getRawYOffset(), this);
    }
    
}
