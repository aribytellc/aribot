package com.aribot.bot.api.wrappers;

import java.awt.Point;

import com.aribot.bot.api.interfaces.Clickable;
import com.aribot.bot.api.interfaces.Locatable;
import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.methods.Model;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.methods.interactables.Players;

public abstract class Character extends Animables implements Locatable, Clickable {
    
    private final com.aribot.bot.interfaces.Character accessor;
    
    public Character(com.aribot.bot.interfaces.Character accessor) {
	super(accessor);
	this.accessor = accessor;
    }
    
    /**
     * Gets the raw X offset
     * @return int
     */
    public int rawOffsetX() {
	return accessor.getX() * accessor.getMultiplierX();
    }
    
    /**
     * Gets the raw Y offset
     * @return int
     */
    public int rawOffsetY() {
	return accessor.getY() * accessor.getMultiplierY();
    }
    
    /**
     * Gets the X offset
     * @return int
     */
    public int offsetX() {
	return ((accessor.getX() * accessor.getMultiplierX()) >> 7);
    }
    
    /**
     * Gets the Y offset
     * @return int
     */
    public int offsetY() {
	return ((accessor.getY() * accessor.getMultiplierY()) >> 7);
    }
    
    /**
     * Gets the X position
     * @return int
     */
    public int getX() {
	return (Game.getMapBaseX() + offsetX());
    }
    
    /**
     * Gets the Y position
     * @return int
     */
    public int getY() {
	return (Game.getMapBaseY() + offsetY());
    }
    
    /**
     * Gets the Z position (current plane)
     * @return int
     */
    public int getZ() {
	return Game.getPlane();
    }
    
    /**
     * Gets the current position
     * @return Tile
     */
    public Tile getLocation() {
	return new Tile(getX(), getY(), getZ());
    }
    
    /**
     * Is Character on screen
     * @return boolean
     */
    public boolean isOnScreen() {
	return Calculations.isOnScreen(Calculations.tileToScreen(getLocation(), 0, 0, getLocation().getZ()));
    }
    
    /**
     * Gets the Characters current animation
     * @return int (-1 if not animating)
     */
    public int getAnimation() {
	return accessor.getAnimation() * accessor.getMultiplierAnimation();
    }
    
    /**
     * Is entity animating
     * @return boolean
     */
    public boolean isAnimating() { // IDK why they want this. It isn't that difficult to check to see if getAnimation is -1... :| lazy asses
	return getAnimation() != -1;
    }
    
    /**
     * Gets the moving int
     * @return int (0 is not moving, 1, 2, and sometimes 3 are walking, 2, 3 and sometimes 1 are running)
     */
    public int getMoving() {
	return accessor.getMoving() * accessor.getMultiplierMoving();
    }
    
    /**
     * Is entity moving
     * @return boolean
     */
    public boolean isMoving() { // Again, why can't they do this themselves. super lazy... :|
	return getMoving() != 0;
    }
    
    /**
     * Gets the interacting ID
     * @return int (-1 if not interacting)
     */
    public int getInteracting() {
	return accessor.getInteractingID() * accessor.getMultiplierInteractingID();
    }
    
    /**
     * Is entity interacting
     * @return boolean
     */
    public boolean isInteracting() { // I am just assuming they will want this because they are already lazy enough... :|
	return getInteracting() != -1;
    }
    
    /**
     * Gets the Characters cycle (Internal use)
     * @return int
     */
    public int getCycle() {
	return accessor.getCycle() * accessor.getMultiplierCycle();
    }
    
    /**
     * Is Characters fighting (Fighting is defined as whether a health bar is shown)
     * @return boolean
     */
    public boolean isFighting() {
	return Game.getCycle() < getCycle(); // Fighting defined as health bar shown
    }
    
    /**
     * Gets the Characters current health
     * @return double
     */
    public double getHealth() {
	return accessor.getHealth() * accessor.getMultiplierHealth();
    }
    
    /**
     * Gets the Characters max health
     * @return double
     */
    public double getMaxHealth() {
	return accessor.getMaxHealth() * accessor.getMultiplierMaxHealth();
    }
    
    /**
     * Gets the Characters current health percent
     * @return int (100 if not fighting)
     */
    public int getHPPercent() {
	return isFighting() ? (int) ((getHealth() / getMaxHealth()) * 100) : 100; // Doesn't publicly update when not fighting.
    }
    
    /**
     * Gets the rotation of the Character
     * @return int
     */
    public int getRotation() {
	return accessor.getRotation() * accessor.getMultiplierRotation();
    }
    
    /**
     * Gets the Model of a character (Should be @Override on the sub-classes)
     * @return Model object
     */
    public abstract Model getModel();
    
    /**
     * Clicks on a Character
     * @param left left click
     */
    public void click(boolean left) {
	click(left, true);
    }
    
    /**
     * Clicks on a Character
     * @param left left click
     * @param center click in the center of the character
     */
    public void click(boolean left, boolean center) {
	Model model = getModel();
	if (model == null) {
	    return;
	}
	Point p = ((center) ? model.getPoint() : model.getRandomPoint());
	Mouse.click(p.x, p.y, left);
    }
    
    /**
     * Gets the Menu actions of the Character (Should be @Override on the sub-classes)
     * @return String array of right click actions
     */
    public abstract String[] getActions();
    
    @Override
    public Point getRandomPoint() {
	return this.getModel().getRandomPoint();
    }
    
    /**
     * Interacts with a Character
     * @param action String action to use
     * @return boolean true on success
     */
    @Override
    public boolean interact(String action) {
	if (this.getModel() != null) {
	    return this.getModel().interact(action);
	}
	return this.getLocation().interact(action);
    }
    
    @Override
    public boolean interact(String action, String option) {
	if (this.getModel() != null) {
	    return this.getModel().interact(action, option);
	}
	return this.getLocation().interact(action, option);
    }
    
    /**
     * Is this Character interacting with our player?
     * @return boolean
     */
    public boolean isInteractingWithLocal() {
	return (getInteracting() - 32768) == Game.getSelfInteratctingIndex();
    }
    
    public Character getInteractingCharacter() { // I don't have a Lookup method for Node yet, and the standard Client.getCache is not the correct location to lookup the definitions by ID
	if (isInteractingWithLocal()) {
	    return Players.getMyPlayer();
	} 
	if (getInteracting() == -1) {
	    return null;
	} else if (getInteracting() < 32768) {
	    // NPC
	} else {
	    // Player
	}
	return null;
    }
    
}
