package com.aribot.bot.api.wrappers;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.interactables.Interactables;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.util.Filter;

public class Area {
	
	private final Tile[] TILES;
	
	/**
	 * Creates an area of Tiles.
	 * @param bound1 Tile boundary 1 (Top left)
	 * @param bound2 Tile boundary 2 (Bottom right)
	 */
	public Area(final Tile bound1, final Tile bound2) {
		
		TILES = new Tile[Math.abs(bound2.getX() - bound1.getX()) * Math.abs(bound2.getY() - bound1.getY())];
		
		int incX = ((bound1.getX() < bound2.getX()) ? 1 : ((bound1.getX() > bound2.getX()) ? -1 : 0));
		int incY = ((bound1.getY() < bound2.getY()) ? 1 : ((bound1.getY() > bound2.getY()) ? -1 : 0));
		
		int i = 0;
		for (int x = bound1.getX(); x <= bound2.getX(); x += incX) {
			for (int y = bound1.getY(); y <= bound2.getY(); y += incY, i++) {
				TILES[i] = new Tile(x, y, bound1.getZ());
			}
		}
	}
	
	/**
	 * Creates an area of Tiles
	 * @param tiles Tiles
	 */
	public Area(final Tile...tiles) {
		TILES = new Tile[tiles.length];
		for (int i = 0; i < tiles.length; i++) {
			TILES[i] = tiles[i];
		}
	}
	
	/**
	 * Gets the Tiles in this Area
	 * @return Tile array
	 */
	public Tile[] getTiles() {
		return this.TILES;
	}
	
	/**
	 * Does the Area contain an Interactable object?
	 * @param loc Interactable object
	 * @return Boolean
	 */
	public boolean contains(final Interactable loc) {
		for (final Tile t : this.getTiles()) {
			if (t.equals(loc.getLocation())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Gets the nearest Interactable object specified by Filter
	 * @param filter Filter
	 * @return Interactable object
	 */
	public Interactable getNearest(Filter<Interactable> filter) {
	    return getNearest(Players.getMyPlayer().getLocation(), filter);
	}
	
	/**
	 * Gets the nearest Interactable object specified by base Tile and Filter
	 * @param base Tile to base on
	 * @param filter Filter
	 * @return Interactable object
	 */
	public Interactable getNearest(Tile base, Filter<Interactable> filter) {
	    Interactable nearest = null;
	    double lastDistance = -1.0D;
	    for (final Tile t : getTiles()) {
		for (Interactable i : Interactables.getAt(t)) {
		    if (filter.apply(i)) {
			double distance = Calculations.distanceBetween(base, i.getLocation());
			if ((lastDistance == -1.0D) || (lastDistance > distance)) {
			    lastDistance = distance;
			    nearest = i;
			}
		    }
		}
	    }
	    return nearest;
	}
}
