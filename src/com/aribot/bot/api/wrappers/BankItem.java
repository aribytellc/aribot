package com.aribot.bot.api.wrappers;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.methods.widgets.Bank;

public class BankItem extends Item {
    
    private int column;
    private int row;
    
    public BankItem(int index, int stack, int column, int row) {
	super(index, stack);
	this.column = column;
	this.row = row;
    }
    
    @Override
    public Point getScreenLocation() {
	/*
	 * Math here..
	 * There is B space between rows =
	 * There is D space between columns ||
	 * E is offset to add to startX to center on the item
	 * F is offset to add to startY to center on the item
	 */
	int B = 36, D = 47, E = 58, F = 23;
// 95, 80 | 140, 80 | 190, 78 | 235, 78 | 282, 78
	// 95, 116 | 95, 152 | 
	// startX = 37
	// startY = 55
	Widget bank = Widgets.getInterface(Bank.PARENT, 91);
	int startX = bank.getFinalX() + E;
	int startY = bank.getFinalY() + F;
	int finalX = startX + (D * this.column);
	int finalY = startY + (B * this.row);
	if ((finalY < bank.getScrollY()) || (finalY > (bank.getScrollY() + bank.getHeight()))) {
	    return new Point(-1, -1);
	}
	return new Point(finalX, finalY);
    }
    
    public void draw(Graphics g) {
	g.setColor(Color.green);
	Point p = getScreenLocation();
	g.drawRect(p.x, p.y, 2, 2);
    }
    
}
