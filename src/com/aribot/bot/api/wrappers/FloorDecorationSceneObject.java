package com.aribot.bot.api.wrappers;

import java.awt.Point;

import com.aribot.bot.api.interfaces.Clickable;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.methods.Model;
import com.aribot.bot.interfaces.Animable;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.FloorDecorationObject;
import com.aribot.bot.interfaces.Models;
import com.aribot.bot.interfaces.ObjectDefinition;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class FloorDecorationSceneObject implements Clickable {
    
    private final FloorDecorationObject accessor;
    private Client c = GameLoader.getClient();
    
    public FloorDecorationSceneObject(FloorDecorationObject accessor) {
	this.accessor = accessor;
    }
    
    /**
     * Gets the hash of the object
     * @return int
     */
    public int getHash() {
	return (this.accessor.getHash() * this.accessor.getMultiplierHash());
    }
    
    /**
     * Gets the ID of the object
     * @return int
     */
    public int getID() {
	return (this.accessor.getHash() * this.accessor.getMultiplierHash()) >> 14 & 0x7FFF;
    }
    
    /**
     * Gets the X of the object
     * @return int
     */
    public int getX() {
	return ((this.accessor.getHash() * this.accessor.getMultiplierHash()) & 0x7F) + (Game.getMapBaseX());
    }
    
    /**
     * Gets the Y of the object
     * @return int
     */
    public int getY() {
	return ((this.accessor.getHash() * this.accessor.getMultiplierHash()) >> 7 & 0x7F) + (Game.getMapBaseY());
    }
    
    /**
     * Gets the Z of the object (current plane)
     * @return int
     */
    public int getZ() {
	return Game.getPlane();
    }
    
    /**
     * Gets the height of the object
     * @return int
     */
    public int getHeight() {
	return (this.accessor.getHeight() * this.accessor.getMultiplierHeight());
    }
    
    /**
     * Gets the location of the object
     * @return Tile
     */
    public Tile getLocation() {
	return new Tile(getX(), getY(), getZ());
    }
    
    /**
     * Gets the X grid of the object
     * @return int
     */
    public int getGridX() {
	return (this.accessor.getWorldX() * this.accessor.getMultiplierWorldX());
    }
    
    /**
     * Gets the Y grid of the object
     * @return int
     */
    public int getGridY() {
	return (this.accessor.getWorldY() * this.accessor.getMultiplierWorldY());
    }
    
    /**
     * Gets the Animable of the object
     * @return Animable object
     */
    public Animable getAnimable() {
	return this.accessor.getAnimable();
    }
    
    /**
     * Gets the Model of the object
     * @return Model
     */
    public Model getModel() {
	Animable a = getAnimable();
	if (a instanceof Models) {
	    return new Animables(a).getModel(getGridX(), getGridY());
	}
	return null;
    }
    
    /**
     * Gets the definition for the Floor decoration
     * @return ObjectDefinition object
     */
    public ObjectDefinition getDef() {
	return c.getObjDef(getHash());
    }
    
    /**
     * Gets the Floor decoration actions. (Most do not have actions, so null)
     * @return String array
     */
    public String[] getActions() {
	return getDef().getActions();
    }
    
    /**
     * Gets the Floor decoration name. (Most do not have a name so null)
     * @return String
     */
    public String getName() {
	return getDef().getName();
    }
    
    @Override
    public Point getRandomPoint() {
	return this.getModel().getRandomPoint();
    }
    
    @Override
    public boolean interact(String action) {
	if (this.getModel() != null && this.getModel().isValid()) {
	    return this.getModel().interact(action);
	}
	return this.getLocation().interact(action);
    }
    
    @Override
    public boolean interact(String action, String option) {
	if (this.getModel() != null && this.getModel().isValid()) {
	    return this.getModel().interact(action, option);
	}
	return this.getLocation().interact(action, option);
    }
    
}