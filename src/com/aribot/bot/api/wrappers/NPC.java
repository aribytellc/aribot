package com.aribot.bot.api.wrappers;

import java.awt.Graphics;
import java.awt.Polygon;

import com.aribot.bot.api.interfaces.Paintable;
import com.aribot.bot.api.interfaces.Recognizable;
import com.aribot.bot.api.methods.Model;
import com.aribot.bot.interfaces.Models;
import com.aribot.bot.interfaces.NPCDefinition;

public class NPC extends Character implements Recognizable, Paintable {

	private final com.aribot.bot.interfaces.NPC accessor;

	public NPC(com.aribot.bot.interfaces.NPC accessor) {
		super(accessor);
		this.accessor = accessor;
	}

	/**
	 * Get the NPCDefinition
	 * @return NPCDefinition
	 */
	public NPCDefinition getDef() {
		return accessor.getNPCDefinition();
	}

	/**
	 * Gets the NPC name
	 * @return String name
	 */
	public String getName() {
		return getDef().getName();
	}

	/**
	 * Gets the NPC level
	 * @return int
	 */
	public int getLevel() {
		return getDef().getLevel() * getDef().getMultiplierLevel();
	}

	/**
	 * Gets the NPC ID
	 * @return int
	 */
	@Override
	public int getId() {
		return getDef().getLID() * getDef().getMultiplierLID();
	}

	/**
	 * Gets the right click actions of the NPC
	 * @return String array of actions
	 */
	@Override
	public String[] getActions() {
		return getDef().getMenuOptions();
	}

	@Override
	public String toString() {
		return this.getName();
	}

	@Override
	public Model getModel() {
		// Gets the cache that is located in npcdef.
		// Use the lookup custom method to find a NodeSub (initial, could be Node due to path)
		// The lookup long is the ID of the npc Node long getID()
		// OffsetX and Y are used for positioning the model.
		// No casting needed as it is upcasting from int to long.
		return new Model((Models) getDef().getCache().lookup(getId()), rawOffsetX(), rawOffsetY(), this.accessor);
	}

	@Override
	public void draw(Graphics g) {
		for (Polygon poly : this.getModel().getTriangles()) {
			g.drawPolygon(poly);
		}
	}

}