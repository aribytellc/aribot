package com.aribot.bot.api.methods;

import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Game {

    private static Client c = GameLoader.getClient();

    /**
     * Current map base x
     * @return int
     */
    public static int getMapBaseX() {
	return c.getMapBaseX() * c.getMultiplierMapBaseX();
    }

    /**
     * Current map base y
     * @return int
     */
    public static int getMapBaseY() {
	return c.getMapBaseY() * c.getMultiplierMapBaseY();
    }

    /**
     * Current camera x
     * @return int
     */
    public static int getCameraX() {
	return c.getCameraX() * c.getMultiplierCameraX();
    }

    /**
     * Current camera y
     * @return int
     */
    public static int getCameraY() {
	return c.getCameraY() * c.getMultiplierCameraY();
    }

    /**
     * Current camera z
     * @return int
     */
    public static int getCameraZ() {
	return c.getCameraZ() * c.getMultiplierCameraZ();
    }

    /**
     * Current camera pitch
     * @return int
     */
    public static int getPitch() {
	return c.getPitch() * c.getMultiplierPitch();
    }

    /**
     * Current camera yaw
     * @return int
     */
    public static int getYaw() {
	return c.getYaw() * c.getMultiplierYaw();
    }

    /**
     * Login index
     * @return int
     */
    public static int getLoginIndex() {
	return c.getLoginIndex() * c.getMultiplierLoginIndex();
    }

    /**
     * Current minimap rotation
     * @return int
     */
    public static int getMinimapRotation() {
	return c.getMinimapRotation() * c.getMultiplierMinimapRotation();
    }

    /**
     * Current minimap zoom
     * @return int
     */
    public static int getMinimapZoom() {
	return c.getMinimapZoom() * c.getMultiplierMinimapZoom();
    }

    /**
     * Current view rotation
     * @return int
     */
    public static int getViewRotation() {
	return c.getViewRotation() * c.getMultiplierViewRotation();
    }

    /**
     * Current players in local area
     * @return int
     */
    public static int getPlayerCount() {
	return c.getPlayerCount() * c.getMultiplierPlayerCount();
    }

    /**
     * Current NPCs in local area
     * @return int
     */
    public static int getNPCCount() {
	return c.getNPCCount() * c.getMultiplierNPCCount();
    }

    /**
     * Current plane
     * @return int
     */
    public static int getPlane() {
	return c.getPlane() * c.getMultiplierPlane();
    }

    /**
     * Current client cycle
     * @return int
     */
    public static int getCycle() {
	return c.getCycle() * c.getMultiplierCycle();
    }

    /**
     * Tile flags
     * @return int[][]
     */
    public static int[][] getTileFlags() {
	return getTileFlags(getPlane());
    }

    /**
     * Gets the tile flags on a specified plane
     * @param plane plane
     * @return int[][] Tile flags
     */
    public static int[][] getTileFlags(int plane) {
	return c.getTileSettings()[plane].getFlags();
    }
    
    /**
     * Gets the Self Interacting Index
     * @return int
     */
    public static int getSelfInteratctingIndex() {
	return c.getSelfInteratctingIndex() * c.getMultiplierSelfInteratctingIndex();
    }
    
}
