package com.aribot.bot.api.methods;

import com.aribot.bot.api.interfaces.Locatable;
import com.aribot.bot.api.methods.input.Keyboard;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.wrappers.Tile;

public class Camera {
    
    public enum Angle {
	NORTH(0),
	NORTHWEST(45),
	WEST(90),
	SOUTHWEST(135),
	SOUTH(180),
	SOUTHEAST(225),
	EAST(270),
	NORTHEAST(315);
	
	public int degrees;
	
	Angle(int degrees) {
	    this.degrees = degrees;
	}
    }
    
    /**
     * Turns the camera to the provided Locatable
     * 
     * @param loc A Locatable object.
     */
    public static void turnTo(Locatable loc) {
	int angle = getAngleTo(loc);
	setAngle(angle);
    }
    
    /**
     * Sets the camera orientation in degrees
     * @param degrees The angle
     */
    public static void setAngle(int degrees) {
	char left = '%';
	char right = '\'';
	char whichDir = left;
	int start = getAngle();
	/*if (start < 180) {
	    start += 360;
	}
	if (degrees < 180) {
	    degrees += 360;
	}*/
	if (degrees > start) {
	    if (degrees - 180 < start) {
		whichDir = right;
	    }
	} else if ((start > degrees) && (start - 180 >= degrees)) {
	    whichDir = right;
	}
	Keyboard.pressKey(whichDir);
	int timeWaited = 0;
	while ((getAngle() > degrees + 5) || (getAngle() < degrees - 5)) {
	    Calculations.sleep(10);
	    timeWaited += 10;
	    if (timeWaited > 500) {
		int time = timeWaited - 500;
		if (time == 0) {
		    Keyboard.pressKey(whichDir);
		} else if (time % 40 == 0) {
		    Keyboard.pressKey(whichDir);
		}
	    }
	}
	Keyboard.releaseKey(whichDir);
    }
    
    /**
     * Set the Camera angle
     * @param angle Angle enum
     */
    public static void setAngle(Angle angle) {
	setAngle(angle.degrees);
    }
    
    /**
     * Gets the current camera rotation.
     * @return int degree
     */
    public static int getAngle() {
	return (int) (((Game.getYaw()) / 2050D) * 360);
    }
    
    /**
     * Set the pitch of the camera
     * @param up camera pitch all the way up.
     * @return boolean if successful
     */
    public static boolean setPitch(boolean up) {
	try {
	    char key = up ? '&' : '(';
	    Keyboard.pressKey(key);
	    Calculations.sleep(Calculations.random(1000, 1500));
	    Keyboard.releaseKey(key);
	} catch (Exception e) {
	    return false;
	}
	return true;
    }
    
    /**
     * Gets the angle to an Locatable object
     * @param loc object
     * @return int the angle that it is at
     */
    public static int getAngleTo(Locatable loc) {
	return getTileAngle(loc.getLocation());
    }
    
    /**
     * Gets the angle to a tile
     * @param t Tile
     * @return int angle
     */
    public static int getTileAngle(Tile t) {
	int a = (angleToTile(t) - 90) % 360;
	return a < 0 ? a + 360 : a;
    }
    
    /**
     * Gets the angle from the current
     * @param t Tile
     * @return int angle
     */
    public static int angleToTile(Tile t) {
	Tile me = Players.getMyPlayer().getLocation();
	int angle = (int) Math.toDegrees(Math.atan2(t.getY() - me.getY(), t.getX() - me.getX()));
	return angle >= 0 ? angle : 360 + angle;
    }
    
}
