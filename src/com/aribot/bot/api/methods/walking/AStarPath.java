package com.aribot.bot.api.methods.walking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;

import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.wrappers.Tile;

/**
 * A Path utility that is meant to generate a Path between the given Tiles.
 * @see {@link Path}
 */
public final class AStarPath extends Path {
    
    public Node start, finish;
    private int loops = 0;
    
    public HashSet<Node> open = new HashSet<>();
    public HashSet<Node> closed = new HashSet<>();
    
    public AStarPath(final Tile[] tiles) {
	super(tiles);
    }
    
    public AStarPath(Tile start, Tile finish) {
	this.start = new Node(start.getX(), start.getY());
	this.finish = new Node(finish.getX(), finish.getY());
    }
    
    @Override
    public Tile[] toTileArray() {
	if (this.tiles == null) {
	    long startTime = System.currentTimeMillis();
	    Node current = this.start;
	    current.setHeuristic();
	    current.open();
	    while (!this.open.isEmpty()) {
		if (loops++ > 10000) {
		    System.out.println("Infinite loop detected. Breaking and returning null");
		    break;
		}
		current = getLowestTotalCost(this.open);
		if (current.equals(this.finish)) {
		    //System.out.println("Found the end in " + (System.currentTimeMillis() - startTime) + "ms.");
		    this.tiles = traceBack(current, start);
		    break;
		}
		current.close();
		Node[] neighbors = current.getNeighbors();
		for (Node neighbor : neighbors) {
		    if (!this.closed.contains(neighbor)) {
			if (!this.open.contains(neighbor)) {
			    neighbor.open();
			    neighbor.parent = current;
			    neighbor.findValues();
			}
			else {
			    Node theoretical = new Node(neighbor.getX(), neighbor.getY()); //Imaginary node parented to the current to calculate its movement cost.
			    theoretical.parent = current;
			    theoretical.setMovementCost();
			    if (theoretical.getMovementCost() < neighbor.getMovementCost()) {
				neighbor.parent = current;
			    }
			}
		    }
		}
	    }
	}
	return tiles;
    }
    
    private static Tile[] traceBack(Node end, Node start) {
	LinkedList<Tile> path = new LinkedList<>();
	Node current = end;
	while (current != null) {
	    path.add(new Tile(current.getX(), current.getY(), Game.getPlane()));
	    current = current.getParent();
	}
	return path.toArray(new Tile[path.size()]);
    }
    
    private static Node getLowestTotalCost(HashSet<Node> nodes) {
	Node best = null;
	for (Node n : nodes) {
	    if (best == null || n.getTotalCost() < best.getTotalCost()) {
		best = n;
	    }
	}
	return best;
    }
    
    private class Node extends Tile {
	
	private int heuristic;
	private int movement;
	private int total;
	private Node parent;
	
	public Node(int x, int y) {
	    super(x, y);
	}
	
	private int getHeuristic() {
	    return this.heuristic;
	}
	
	private void setHeuristic() {
	    Tile finish = AStarPath.this.finish;
	    this.heuristic = Math.abs(finish.getX() - this.getX()) + Math.abs(finish.getY() - this.getY());
	}
	
	private void findValues() {
	    this.setHeuristic();
	    this.setMovementCost();
	    this.setTotal();
	}
	
	private int getMovementCost() {
	    return this.movement;
	}
	
	private void setMovementCost() {
	    int value = 0;
	    Node current = this;
	    Node parent;
	    while (!current.equals(AStarPath.this.start)) {
		parent = current.getParent();
		if (parent == null) {
		    throw new RuntimeException("Node at " + this + " has an invalid parent.");
		}
		value += (int) (Math.hypot(Math.abs(parent.getX() - current.getX()), Math.abs(parent.getY() - current.getY())) * 10);
		current = parent;
	    }
	    this.movement = value;
	}
	
	private int getTotalCost() {
	    return this.total;
	}
	
	private void setTotal() {
	    this.total = this.getHeuristic() + this.getMovementCost();
	}
	
	private void open() {
	    AStarPath.this.open.add(this);
	    AStarPath.this.closed.remove(this);
	}
	
	private void close() {
	    AStarPath.this.open.remove(this);
	    AStarPath.this.closed.add(this);
	}
	
	private Node[] getNeighbors() {
	    ArrayList<Node> neighbors = new ArrayList<>();
	    Tile north = this.derive(0, -1);
	    Tile east = this.derive(1, 0);
	    Tile south = this.derive(0, 1);
	    Tile west = this.derive(-1, 0);
	    Tile northWest = this.derive(-1, -1);
	    Tile northEast = this.derive(1, -1);
	    Tile southEast = this.derive(1, 1);
	    Tile southWest = this.derive(-1, 1);
	    for (Tile t : new Tile[] { north, east, south, west, northWest, northEast, southEast, southWest } ) {
		if (!closed.contains(t) && this.canWalkTo(t)) {
		    neighbors.add(new Node(t.getX(), t.getY()));
		}
	    }
	    return neighbors.toArray(new Node[neighbors.size()]);
	}
	
	private Node getParent() {
	    return this.parent;
	}
	
	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + getX();
	    result = prime * result + getY();
	    return result;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj instanceof Node) {
		Node n = (Node) obj;
		return this.getX() == n.getX() && this.getY() == n.getY();
	    }
	    return false;
	}
    }
}
