package com.aribot.bot.api.methods.walking;

import javax.swing.JFrame;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.wrappers.Tile;

/**
 * A Path utility to represent an array of Tiles to navigate.
 */
public class Path {
    
    protected Tile[] tiles;
    
    protected Path() {
        this(new Tile[0]);
    }
    
    /**
     * A pre-defined path that can extend from the currently loaded map.
     * 
     * @param tiles
     */
    public Path(final Tile[] tiles) {
        this.tiles = tiles;
    }
    
    /**
     * Generates a path between the two given Tiles. For local use only.
     * 
     * @param start The start Tile for the path.
     * @param finish The ending Tile for the path.
     */
    public Path(final Tile start, final Tile finish) {
        this(generatePath(start, finish).toTileArray());
    }
    
    /**
     * Generates a path between the current player's location and the given end Tile.
     * 
     * @param end The ending Tile for the path.
     */
    public Path(final Tile end) {
        this(Players.getMyPlayer().getLocation(), end);
    }
    
    /**
     * @return An array of Tiles that represent this Path.
     */
    public Tile[] toTileArray() {
        return this.tiles;
    }
    
    /**
     * Takes a step to the farthest Tile along the path that is visible.
     * Returns false if either no Tile is on the screen, or no Tile in this Path is reachable.
     * 
     * @param target a PathTarget choice START or END
     * @return true if the step is taken, false otherwise.
     */
    public boolean stepTowards(final PathTarget target) {
        return target == PathTarget.START ? stepTowardsStart() : stepTowardsEnd();
    }
    
    private boolean stepTowardsStart() {
        for (int i = 0; i < this.tiles.length; i++) {
            Tile next = this.tiles[i];
            if (Calculations.isTileOnMap(next) && next.isReachable()) {
                return Walking.walkTileMM(next);
            }
        }
        return false;
    }
    
    private boolean stepTowardsEnd() {
        for (int i = this.tiles.length-1; i >= 0; i--) {
            Tile next = this.tiles[i];
            if (Calculations.isTileOnMap(next) && next.isReachable()) {
                return Walking.walkTileMM(next);
            }
        }
        return false;
    }
    
    /**
     * Generates a path between the current player's location and the given end Tile.
     * 
     * @param end The ending Tile for the path.
     * @return A generated Path.
     */
    public static Path findPathTo(final Tile target) {
        return generatePath(Players.getMyPlayer().getLocation(), target);
    }
    
    /**
     * Generates a path between the given start Tile and the given end Tile.
     * 
     * @param start The starting Tile for the path.
     * @param end The ending Tile for the path.
     * @return A generated Path.
     */
    public static Path generatePath(final Tile start, final Tile finish) {
        return new AStarPath(start, finish);
    }
    
    public enum PathTarget {
        START,
        END
    }
    
}