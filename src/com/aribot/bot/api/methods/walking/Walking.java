package com.aribot.bot.api.methods.walking;

import java.awt.Point;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Walking {

	private static Client c = GameLoader.getClient();

	/**
	 * Gets the closest tile on the minimap
	 * @param tile Tile
	 * @return Tile object that is the closest
	 */
	public static Tile getClosestTileOnMap(Tile tile) {
		if (!Calculations.isTileOnMap(tile)) {
			Tile loc = Players.getMyPlayer().getLocation();
			Tile walk = new Tile((loc.getX() + tile.getX()) / 2, (loc.getY() + tile.getY()) / 2, Game.getPlane());
			return Calculations.isTileOnMap(walk) ? walk : getClosestTileOnMap(walk);
		}
		return tile;
	}

	/**
	 * Walks to a tile using the minimap
	 * @param dest Tile object to go to
	 * @return boolean did we walk there
	 */
	public static boolean walkTileMM(Tile dest) {
		while (Players.getMyPlayer().getMoving() != 0) {
			Calculations.sleep(1000);
		}
		if (!Calculations.isTileOnMap(dest)) {
			dest = getClosestTileOnMap(dest);
		}
		final Point p = Calculations.tileToMinimap(dest);
		if (p.x != -1 && p.y != -1) {
			Mouse.move(p);
			final Point p2 = Calculations.tileToMinimap(dest);
			if (p2.x != -1 && p2.y != -1) {
				if (!Mouse.getLocation().equals(p2)) {
					Mouse.move(p2);
				}
				Mouse.click(true);
				while (Players.getMyPlayer().getMoving() != 0) {
					Calculations.sleep(1000);
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets your current energy
	 * @return int
	 */
	public static int getEnergy() {
		return c.getEnergy() * c.getMultiplierEnergy();
	}

	/**
	 * Gets your current weight in-game.
	 * @return int
	 */
	public static int getWeight() {
		return c.getWeight() * c.getMultiplierWeight();
	}

}
