package com.aribot.bot.api.methods;

import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Chat {

	private static Client c = GameLoader.getClient();
	
	public static final int SERVER = 0;
	public static final int STANDARD = 2;
	public static final int SERVER_FRIEND = 3;
	
	/**
	 * Gets the chat message array
	 * @return String[] Just messages
	 */
	public static String[] getMessages() {
		return c.getChat();
	}
	
	/**
	 * Gets the chat name array
	 * @return String[] Just names
	 */
	public static String[] getNames() {
		return c.getChatNames();
	}
	
	/**
	 * Gets the last chatline (most recent)
	 * @return String Specified by "Type : Name : Message"
	 */
	public static String getLastLine() {
		return getTypes()[0] + " : " + getNames()[0] + " : " + getMessages()[0];
	}
	
	/**
	 * Gets the chat type array
	 * @return int[] Just types
	 */
	public static int[] getTypes() {
		return c.getChatTypes();
	}
	
}
