package com.aribot.bot.api.methods;

import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Settings {

	private static Client c = GameLoader.getClient();
	
	/**
	 * Gets the settings array
	 * @return int array
	 */
	public static int[] getSettings() {
		return c.getSettings();
	}
	
	/**
	 * Gets a specific setting
	 * @param ID Setting ID
	 * @return int Value
	 */
	public static int getSetting(int ID) {
		return getSettings()[ID];
	}
	
}
