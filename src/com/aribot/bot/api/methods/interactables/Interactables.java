package com.aribot.bot.api.methods.interactables;

import java.util.ArrayList;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.Interactable;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.Ground;
import com.aribot.bot.interfaces.SceneObject;
import com.aribot.bot.interfaces.World;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Interactables {

	private static Client c = GameLoader.getClient();

	/**
	 * Gets the loaded Interactable objects based on a plane
	 * @param plane
	 * @return Interactable object array
	 */
	public static Interactable[] getLoaded(int plane) {
		ArrayList<Interactable> objects = new ArrayList<>();
		for (int x = 0; x < 104; x++) {
			for (int y = 0; y < 104; y++) {
				Interactable[] o = getAt(x, y, plane);
				if (o != null) {
					for (Interactable i : o) {
						if ((i != null) && (!contains(i, objects))) {
							objects.add(i);
						}
					}
				}
			}
		}
		return objects.toArray(new Interactable[objects.size()]);
	}

	/**
	 * Gets the loaded Interactables by filter
	 * @param filter Filter
	 * @return Interactable array
	 */
	public static Interactable[] getLoaded(Filter<Interactable> filter) {
		ArrayList<Interactable> inters = new ArrayList<>();
		Interactable[] all = getLoaded();
		for (Interactable i : all) {
			if (i != null && filter.apply(i)) {
				inters.add(i);
			}
		}
		return inters.toArray(new Interactable[inters.size()]);
	}

	/**
	 * Gets loaded Interactables based on current plane.
	 * @return Interactable array.
	 */
	public static Interactable[] getLoaded() {
		return getLoaded(Game.getPlane());
	}

	/**
	 * Gets the Interactable objects at a location based on 0 plane.
	 * @param x x tile
	 * @param y y tile
	 * @return Interactable array

    private static Interactable[] getInteractablesAt(int x, int y) {
	return getInteractablesAt(x, y, 0);
    }
	 */

	/**
	 * Gets the Interactable objects at a location based on specified plane.
	 * @param x x tile
	 * @param y y tile
	 * @param z z tile
	 * @return Interactable array
	 */
	private static Interactable[] getAt(int x, int y, int z) {
		try {
			World world = c.getWorld();
			Ground ground = world.getGroundArray()[z][x][y];
			ArrayList<Interactable> interactables = new ArrayList<>();
			if (ground != null) {
				for (SceneObject i : ground.getSceneObject()) {
					if (i != null) {
						Interactable interactable = new Interactable(i);
						interactables.add(interactable);
					}
				}
				return interactables.toArray(new Interactable[interactables.size()]);
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Gets the Interactables at a Tile location
	 * @param tile Tile
	 * @return Interactable array
	 */
	public static Interactable[] getAt(Tile tile) {
		return getAt(tile.getX(), tile.getY(), tile.getZ());
	}

	/**
	 * Is Interactable object in an array list of Interactables?
	 * @param i object
	 * @param in ArrayList of Interactable objects
	 * @return boolean Contains
	 */
	public static boolean contains(Interactable i, ArrayList<Interactable> in) {
		for (Interactable d : in) {
			if (d.getHash() == i.getHash()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the nearest Interactable object filtered by ID
	 * @param id int[] of Interactable IDs
	 * @return Single Interactable object
	 */
	public static Interactable getNearest(int...id) {
		return getNearest(Players.getMyPlayer().getLocation(), id);
	}

	/**
	 * Gets the nearest Interactable object filtered by name
	 * @param names String[] of names
	 * @return Interactable
	 */
	public static Interactable getNearest(final String...names) {
		return getNearest(Players.getMyPlayer().getLocation(), new Filter<Interactable>() {
			@Override
			public boolean apply(Interactable t) {
				for (String n : names) {
					if (t.getName().equals(n)) {
						return true;
					}
				}
				return false;
			} 
		});
	}

	/**
	 * Gets the nearest Interactable by filter
	 * @param filter Filter
	 * @return Closest Interactable
	 */
	public static Interactable getNearest(Filter<Interactable> filter) {
		return getNearest(Players.getMyPlayer().getLocation(), filter);
	}

	/**
	 * Gets the nearest Interactable object filtered by ID and a specified tile
	 * @param tile Location to use as a base
	 * @param id int[] of Interactable IDs
	 * @return Single Interactable object
	 */
	public static Interactable getNearest(Tile tile, final int...id) {
		return getNearest(tile, new Filter<Interactable>() {
			@Override
			public boolean apply(Interactable t) {
				for (int i : id) {
					if (t.getId() == i) {
						return true;
					}
				}
				return false;
			} 
		});
	}

	/**
	 * Gets the nearest Interactable from Base tile with specified filter
	 * @param tile Base Tile
	 * @param filter Filter
	 * @return Closest Interactable
	 */
	public static Interactable getNearest(Tile tile, Filter<Interactable> filter) {
		Interactable nearest = null;
		double lastDistance = -1.0D;
		Interactable[] objects = getLoaded();
		if (objects == null) {
			return null;
		}
		for (Interactable object : objects) {
			if (filter.apply(object)) {
				double distance = Calculations.distanceBetween(tile, object.getLocation());
				if ((lastDistance == -1.0D) || (lastDistance > distance)) {
					lastDistance = distance;
					nearest = object;
				}
			}
		}
		return nearest;
	}

}