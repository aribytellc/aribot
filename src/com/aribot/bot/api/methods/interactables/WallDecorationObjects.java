package com.aribot.bot.api.methods.interactables;

import java.util.ArrayList;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.api.wrappers.WallDecorationSceneObject;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.Ground;
import com.aribot.bot.interfaces.World;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class WallDecorationObjects {

	private static Client c = GameLoader.getClient();

	/**
	 * Gets the loaded WallDecorationSceneObject objects based on a plane
	 * @param plane
	 * @return WallDecorationSceneObject object array
	 */
	public static WallDecorationSceneObject[] getLoaded(int plane) {
		ArrayList<WallDecorationSceneObject> objects = new ArrayList<>();
		for (int x = 0; x < 104; x++) {
			for (int y = 0; y < 104; y++) {
				WallDecorationSceneObject o = getAt(x, y, plane);
				if (o != null) {
					if (!contains(o, objects)) {
						objects.add(o);
					}
				}
			}
		}
		return objects.toArray(new WallDecorationSceneObject[objects.size()]);
	}

	/**
	 * Gets loaded WallDecorationSceneObjects based on current plane.
	 * @return WallDecorationSceneObject array.
	 */
	public static WallDecorationSceneObject[] getLoaded() {
		return getLoaded(Game.getPlane());
	}

	/**
	 * Gets the WallDecorationSceneObject at a location based on 0 plane.
	 * @param x x tile
	 * @param y y tile
	 * @return WallDecorationSceneObject
	 */
	public static WallDecorationSceneObject getAt(int x, int y) {
		return getAt(x, y, 0);
	}

	/**
	 * Gets the WallDecorationSceneObject at a location based on specified plane.
	 * @param x x tile
	 * @param y y tile
	 * @param z z tile
	 * @return WallDecorationSceneObject
	 */
	public static WallDecorationSceneObject getAt(int x, int y, int z) {
		try {
			World world = c.getWorld();
			Ground ground = world.getGroundArray()[z][x][y];
			if (ground != null && ground.getWallDecorationObject() != null) {
				return new WallDecorationSceneObject(ground.getWallDecorationObject());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Is WallDecorationSceneObject object in an array list of WallDecorationSceneObjects?
	 * @param i object
	 * @param in ArrayList of WallDecorationSceneObject objects
	 * @return boolean Contains
	 */
	public static boolean contains(WallDecorationSceneObject i, ArrayList<WallDecorationSceneObject> in) {
		for (WallDecorationSceneObject d : in) {
			if (d.getHash() == i.getHash()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the nearest WallDecorationSceneObject filtered by ID
	 * @param id int[] of WallDecorationSceneObject IDs
	 * @return Single WallDecorationSceneObject object
	 */
	public static WallDecorationSceneObject getNearest(int[] id) {
		return getNearest(Players.getMyPlayer().getLocation(), id);
	}

	/**
	 * Gets the nearest WallDecorationSceneObject filtered by ID and a specified tile
	 * @param tile Location to use as a base
	 * @param id int[] of WallDecorationSceneObject IDs
	 * @return Single WallDecorationSceneObject object
	 */
	public static WallDecorationSceneObject getNearest(Tile tile, int[] id) {
		WallDecorationSceneObject nearest = null;
		double lastDistance = -1.0D;
		WallDecorationSceneObject[] objects = getLoaded();
		if (objects == null) {
			return null;
		}
		for (WallDecorationSceneObject object : objects) {
			for (int index : id) {
				if (object.getID() == index) {
					double distance = Calculations.distanceBetween(tile, object.getLocation());

					if ((lastDistance == -1.0D) || (lastDistance > distance)) {
						lastDistance = distance;
						nearest = object;
					}
				}
			}
		}
		return nearest;
	}
}