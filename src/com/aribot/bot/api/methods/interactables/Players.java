package com.aribot.bot.api.methods.interactables;

import java.util.ArrayList;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.Player;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Players {

	private static Client c = GameLoader.getClient();

	/**
	 * Gets the loaded Player array in the current area
	 * @return Player object array
	 */
	public static Player[] getLoaded() {
		com.aribot.bot.interfaces.Player[] loadedPlayers = c.getPlayerArray();
		ArrayList<Player> players = new ArrayList<>();
		for (com.aribot.bot.interfaces.Player i : loadedPlayers) {
			if (i != null) {
				Player player = new Player(i);
				players.add(player);
			}
		}
		return players.toArray(new Player[players.size()]);
	}

	/**
	 * Gets the loaded Players by a Filter
	 * @param filter Filter
	 * @return Player array done by the filter
	 */
	public static Player[] getLoaded(Filter<Player> filter) {
		ArrayList<Player> players = new ArrayList<>();
		Player[] all = getLoaded();
		for (Player i : all) {
			if (i != null && filter.apply(i)) {
				players.add(i);
			}
		}
		return players.toArray(new Player[players.size()]);
	}

	/**
	 * Gets the local player (Your own player object)
	 * @return Player object
	 */
	public static Player getMyPlayer() {
		return new Player(c.getMyPlayer());
	}

	/**
	 * Gets player objects at an X and Y position
	 * @param x X position
	 * @param y Y position
	 * @return Player object array
	 */
	public static Player[] getAt(int x, int y) {
		try {
			Player[] loadedPlayers = getLoaded();
			ArrayList<Player> validPlayers = new ArrayList<>();
			if (loadedPlayers != null) {
				for (Player i : loadedPlayers) {
					if (i != null && i.getX() == x && i.getY() == y) {
						validPlayers.add(i);
					}
				}
				return validPlayers.toArray(new Player[validPlayers.size()]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Does Player ArrayList contian a certain Player object
	 * @param i Player object
	 * @param in Player ArrayList
	 * @return boolean
	 */
	public static boolean contains(Player i, ArrayList<Player> in) {
		for (Player d : in) {
			if (d.getName().equals(i.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the nearest Player object by name
	 * @param names String array of names to search by
	 * @return Player object
	 */
	public static Player getNearest(String[] names) {
		return getNearest(getMyPlayer().getLocation(), names);
	}

	/**
	 * Gets the nearest Player by filter
	 * @param filter Filter
	 * @return Closest Player
	 */
	public static Player getNearest(Filter<Player> filter) {
		return getNearest(getMyPlayer().getLocation(), filter);
	}

	/**
	 * Gets the nearest Player object by name with a base tile
	 * @param tile Base tile
	 * @param names String array of names to search by
	 * @return Player object
	 */
	public static Player getNearest(Tile tile, String[] names) {
		Player nearest = null;
		double lastDistance = -1.0D;
		Player[] players = getLoaded();
		if (players == null) {
			return null;
		}
		for (Player player : players) {
			for (String name : names) {
				if (player.getName().equals(name)) {
					double distance = Calculations.distanceBetween(tile, player.getLocation());
					if ((lastDistance == -1.0D) || (lastDistance > distance)) {
						lastDistance = distance;
						nearest = player;
					}
				}
			}
		}
		return nearest;
	}

	/**
	 * Gets the nearest Player from base Tile specified by Filter
	 * @param tile Base tile
	 * @param filter Filter
	 * @return Closest Player
	 */
	public static Player getNearest(Tile tile, Filter<Player> filter) {
		Player nearest = null;
		double lastDistance = -1.0D;
		Player[] players = getLoaded();
		if (players == null) {
			return null;
		}
		for (Player player : players) {
			if (filter.apply(player)) {
				double distance = Calculations.distanceBetween(tile, player.getLocation());
				if ((lastDistance == -1.0D) || (lastDistance > distance)) {
					lastDistance = distance;
					nearest = player;
				}
			}

		}
		return nearest;
	}

}