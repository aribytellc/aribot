package com.aribot.bot.api.methods.interactables;

import java.util.ArrayList;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.FloorDecorationSceneObject;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.Ground;
import com.aribot.bot.interfaces.World;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class FloorDecorationObjects {
    
    private static Client c = GameLoader.getClient();
    
    /**
     * Gets the loaded FloorDecorationSceneObjects based on a plane
     * @param plane
     * @return FloorDecorationSceneObject array
     */
    public static FloorDecorationSceneObject[] getLoaded(int plane) {
	ArrayList<FloorDecorationSceneObject> objects = new ArrayList<>();
	for (int x = 0; x < 104; x++) {
	    for (int y = 0; y < 104; y++) {
		FloorDecorationSceneObject o = getAt(x, y, plane);
		if (o != null) {
		    if (!contains(o, objects)) {
			objects.add(o);
		    }
		}
	    }
	}
	return objects.toArray(new FloorDecorationSceneObject[objects.size()]);
    }
    
    /**
     * Gets loaded FloorDecorationSceneObjects based on current plane.
     * @return FloorDecorationSceneObject array.
     */
    public static FloorDecorationSceneObject[] getLoaded() {
	return getLoaded(Game.getPlane());
    }
    
    /**
     * Gets the loaded FloorDecorationSceneObjects with a specified filter
     * @param filter Filter
     * @return FloorDecorationSceneObject array
     */
    public static FloorDecorationSceneObject[] getLoaded(Filter<FloorDecorationSceneObject> filter) {
	ArrayList<FloorDecorationSceneObject> inters = new ArrayList<>();
	FloorDecorationSceneObject[] all = getLoaded();
	for (FloorDecorationSceneObject i : all) {
	    if (i != null && filter.apply(i)) {
		inters.add(i);
	    }
	}
	return inters.toArray(new FloorDecorationSceneObject[inters.size()]);
    }
    
    /**
     * Gets the FloorDecorationSceneObject at a location based on 0 plane.
     * @param x x tile
     * @param y y tile
     * @return FloorDecorationSceneObject
     */
    public static FloorDecorationSceneObject getAt(int x, int y) {
	return getAt(x, y, 0);
    }
    
    /**
     * Gets the FloorDecorationSceneObject at a location based on specified plane.
     * @param x x tile
     * @param y y tile
     * @param z z tile
     * @return FloorDecorationSceneObject
     */
    public static FloorDecorationSceneObject getAt(int x, int y, int z) {
	try {
	    World world = c.getWorld();
	    Ground ground = world.getGroundArray()[z][x][y];
	    if (ground != null && ground.getFloorDecorationObject() != null) {
		return new FloorDecorationSceneObject(ground.getFloorDecorationObject());
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return null;
    }
    
    /**
     * Is FloorDecorationSceneObject object in an array list of FloorDecorationSceneObjects?
     * @param i object
     * @param in ArrayList of FloorDecorationSceneObjects
     * @return boolean Contains
     */
    public static boolean contains(FloorDecorationSceneObject i, ArrayList<FloorDecorationSceneObject> in) {
	for (FloorDecorationSceneObject d : in) {
	    if (d.getHash() == i.getHash()) {
		return true;
	    }
	}
	return false;
    }
    
    /**
     * Gets the nearest FloorDecorationSceneObject object filtered by ID
     * @param id int[] of FloorDecorationSceneObject IDs
     * @return Single FloorDecorationSceneObject object
     */
    public static FloorDecorationSceneObject getNearest(int... id) {
	return getNearest(Players.getMyPlayer().getLocation(), id);
    }
    
    /**
     * Gets the nearest FloorDecorationSceneObject with a specified filter
     * @param filter Filter
     * @return Single FloorDecorationSceneObject
     */
    public static FloorDecorationSceneObject getNearest(Filter<FloorDecorationSceneObject> filter) {
	return getNearest(Players.getMyPlayer().getLocation(), filter);
    }
    
    /**
     * Gets the nearest FloorDecorationSceneObject object filtered by ID and a specified tile
     * @param tile Location to use as a base
     * @param id int[] of FloorDecorationSceneObject IDs
     * @return Single FloorDecorationSceneObject object
     */
    public static FloorDecorationSceneObject getNearest(Tile tile, int... id) {
	FloorDecorationSceneObject nearest = null;
	double lastDistance = -1.0D;
	FloorDecorationSceneObject[] objects = getLoaded();
	if (objects == null) {
	    return null;
	}
	for (FloorDecorationSceneObject object : objects) {
	    for (int index : id) {
		if (object.getID() == index) {
		    double distance = Calculations.distanceBetween(tile, object.getLocation());
		    if ((lastDistance == -1.0D) || (lastDistance > distance)) {
			lastDistance = distance;
			nearest = object;
		    }
		}
	    }
	}
	return nearest;
    }
    
    /**
     * Gets the nearest FloorDecorationSceneObject object with specified filter and base Tile
     * @param tile Location to use as a base
     * @param filter Filter
     * @return Single FloorDecorationSceneObject object
     */
    public static FloorDecorationSceneObject getNearest(Tile tile, Filter<FloorDecorationSceneObject> filter) {
	FloorDecorationSceneObject nearest = null;
	double lastDistance = -1.0D;
	FloorDecorationSceneObject[] objects = getLoaded();
	if (objects == null) {
	    return null;
	}
	for (FloorDecorationSceneObject object : objects) {
	    if (filter.apply(object)) {
		double distance = Calculations.distanceBetween(tile, object.getLocation());
		if ((lastDistance == -1.0D) || (lastDistance > distance)) {
		    lastDistance = distance;
		    nearest = object;
		}
	    }
	}
	return nearest;
    }
    
}