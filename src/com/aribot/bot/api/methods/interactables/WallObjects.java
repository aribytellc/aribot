package com.aribot.bot.api.methods.interactables;

import java.util.ArrayList;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.api.wrappers.WallSceneObject;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.Ground;
import com.aribot.bot.interfaces.World;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class WallObjects {
    
    private static Client c = GameLoader.getClient();
    
    /**
     * Gets the loaded WallSceneObjects based on a plane
     * @param plane
     * @return WallSceneObject object array
     */
    public static WallSceneObject[] getLoaded(int plane, Filter<WallSceneObject> filter) {
	ArrayList<WallSceneObject> objects = new ArrayList<>();
	for (int x = 0; x < 104; x++) {
	    for (int y = 0; y < 104; y++) {
		WallSceneObject o = getAt(x, y, plane);
		if (o != null) {
		    if (!contains(o, objects) && (filter == null || filter.apply(o))) {
			objects.add(o);
		    }
		}
	    }
	}
	return objects.toArray(new WallSceneObject[objects.size()]);
    }
    
    /**
     * Gets loaded WallSceneObjects based on current plane.
     * @return WallSceneObject array.
     */
    public static WallSceneObject[] getLoaded() {
	return getLoaded(Game.getPlane(), null);
    }
    
    /**
     * Gets the loaded Wall objects based on Filter
     * @param filter Filter
     * @return WallSceneObject array
     */
    public static WallSceneObject[] getLoaded(Filter<WallSceneObject> filter) {
	return getLoaded(Game.getPlane(), filter);
    }
    
    /**
     * Get the WallSceneObject at a Tile
     * @param tile Tile
     * @return WallSceneObject
     */
    public static WallSceneObject getAt(Tile tile) {
	return getAt(tile.getX(), tile.getY(), tile.getZ());
    }
    
    /**
     * Gets the WallSceneObject at a location based on 0 plane.
     * @param x x tile
     * @param y y tile
     * @return WallSceneObject
     */
    public static WallSceneObject getAt(int x, int y) {
	return getAt(x, y, 0);
    }
    
    /**
     * Gets the WallSceneObject at a location based on specified plane.
     * @param x x tile
     * @param y y tile
     * @param z z tile
     * @return WallSceneObject
     */
    public static WallSceneObject getAt(int x, int y, int z) {
	try {
	    World world = c.getWorld();
	    Ground ground = world.getGroundArray()[z][x][y];
	    if (ground != null && ground.getWallObject() != null) {
		return new WallSceneObject(ground.getWallObject());
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return null;
    }
    
    /**
     * Is WallSceneObject object in an array list of WallSceneObjects?
     * @param i object
     * @param in ArrayList of WallSceneObject objects
     * @return boolean Contains
     */
    public static boolean contains(WallSceneObject i, ArrayList<WallSceneObject> in) {
	for (WallSceneObject d : in) {
	    if (d.getHash() == i.getHash()) {
		return true;
	    }
	}
	return false;
    }
    
    /**
     * Gets the nearest WallSceneObject by Filter and base Tile
     * @param filter Filter
     * @param tile Tile
     * @return WallSceneOject
     */
    public static WallSceneObject getNearest(Filter<WallSceneObject> filter, Tile tile) {
	WallSceneObject nearest = null;
	double lastDistance = -1.0D;
	WallSceneObject[] objects = getLoaded();
	if (objects == null) {
	    return null;
	}
	for (WallSceneObject object : objects) {
	    if (filter.apply(object)) {
		double distance = Calculations.distanceBetween(tile, object.getLocation());
		if ((lastDistance == -1.0D) || (lastDistance > distance)) {
		    lastDistance = distance;
		    nearest = object;
		}
	    }
	}
	return nearest;
    }
    
    /**
     * Gets the nearest WallSceneObject based on Filter
     * @param filter Filter 
     * @return WallSceneObject
     */
    public static WallSceneObject getNearest(Filter<WallSceneObject> filter) {
	return getNearest(filter, Players.getMyPlayer().getLocation());
    }
    
    /**
     * Gets the nearest WallSceneObject object filtered by ID
     * @param id int[] of WallSceneObject IDs
     * @return Single WallSceneObject object
     */
    public static WallSceneObject getNearest(int...id) {
	return getNearest(Players.getMyPlayer().getLocation(), id);
    }
    
    /**
     * Gets the nearest WallSceneObject object filtered by ID and a specified tile
     * @param tile Location to use as a base
     * @param id int[] of WallSceneObject IDs
     * @return Single WallSceneObject object
     */
    public static WallSceneObject getNearest(Tile tile, final int...id) {
	return getNearest(new Filter<WallSceneObject>() {
	    @Override
	    public boolean apply(WallSceneObject t) {
		for (int i : id) {
		    if (t.getId() == i) {
			return true;
		    }
		}
		return false;
	    }
	}, tile);
    }
}