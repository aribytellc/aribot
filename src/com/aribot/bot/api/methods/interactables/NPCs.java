package com.aribot.bot.api.methods.interactables;

import java.util.ArrayList;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.NPC;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class NPCs {

	private static Client c = GameLoader.getClient();

	/**
	 * Gets the loaded NPCs in the minimap area
	 * @return NPC object array
	 */
	public static NPC[] getLoaded() {
		com.aribot.bot.interfaces.NPC[] loadedNPCs = c.getNPCArray();
		ArrayList<NPC> npcs = new ArrayList<>();
		for (com.aribot.bot.interfaces.NPC i : loadedNPCs) {
			if (i != null) {
				NPC npc = new NPC(i);
				npcs.add(npc);
			}
		}
		return npcs.toArray(new NPC[npcs.size()]);
	}

	/**
	 * Gets the loaded NPCs selected by a specified filter
	 * @param filter Filter
	 * @return NPC array of valid NPCs
	 */
	public static NPC[] getLoaded(Filter<NPC> filter) {
		ArrayList<NPC> npcs = new ArrayList<>();
		NPC[] all = getLoaded();
		for (NPC i : all) {
			if (i != null && filter.apply(i)) {
				npcs.add(i);
			}
		}
		return npcs.toArray(new NPC[npcs.size()]);
	}

	/**
	 * Gets the NPCs at a location
	 * @param x X location
	 * @param y Y location
	 * @return NPC object array of NPCs located at point
	 */
	public static NPC[] getAt(int x, int y) {
		try {
			NPC[] loadedNPCs = getLoaded();
			ArrayList<NPC> validNPCs = new ArrayList<>();
			if (loadedNPCs != null) {
				for (NPC i : loadedNPCs) {
					if (i != null && i.getX() == x && i.getY() == y) {
						validNPCs.add(i);
					}
				}
				return validNPCs.toArray(new NPC[validNPCs.size()]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Does NPC ArrayList contain a certain NPC object
	 * @param i NPC object
	 * @param in NPC ArrayList
	 * @return boolean
	 */
	public static boolean contains(NPC i, ArrayList<NPC> in) {
		for (NPC d : in) {
			if (d.getName().equals(i.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the nearest NPC by name
	 * @param names String array of names to search by
	 * @return Single NPC object
	 */
	public static NPC getNearest(String...names) {
		return getNearest(Players.getMyPlayer().getLocation(), names);
	}

	/**
	 * Gets the nearest NPC with a base tile
	 * @param tile Base tile to search from
	 * @param names String array of names to search by
	 * @return Single NPC object
	 */
	public static NPC getNearest(Tile tile, String...names) {
		NPC nearest = null;
		double lastDistance = -1.0D;
		NPC[] npcs = getLoaded();
		if (npcs == null) {
			return null;
		}
		for (NPC npc : npcs) {
			for (String name : names) {
				if (npc.getName().equals(name)) {
					double distance = Calculations.distanceBetween(tile, npc.getLocation());
					if ((lastDistance == -1.0D) || (lastDistance > distance)) {
						lastDistance = distance;
						nearest = npc;
					}
				}
			}
		}
		return nearest;
	}

	/**
	 * Gets the nearest NPC specified by Filter
	 * @param filter Filter
	 * @return Closest NPC
	 */
	public static NPC getNearest(Filter<NPC> filter) {
		return getNearest(Players.getMyPlayer().getLocation(), filter);
	}

	/**
	 * Gets the nearest NPC from the set of NPCs matching the given ids.
	 * @param ids IDs to get the nearest NPC from
	 * @return Closest NPC matching the given ids
	 */
	public static NPC getNearest(final int...ids) {
		return getNearest(Players.getMyPlayer().getLocation(), new Filter<NPC>() {
			@Override
			public boolean apply(NPC t) {
				for (int id : ids) {
					if (t.getId() == id) {
						return true;
					}
				}
				return false;
			} 
		});
	}

	/**
	 * Gets the nearest NPC from a specified base tile, using a filter
	 * @param tile Base Tile
	 * @param filter Filter
	 * @return Closest NPC to Tile with Filter
	 */
	public static NPC getNearest(Tile tile, Filter<NPC> filter) {
		NPC nearest = null;
		double lastDistance = -1.0D;
		NPC[] npcs = getLoaded();
		if (npcs == null) {
			return null;
		}
		for (NPC npc : npcs) {
			if (filter.apply(npc)) {
				double distance = Calculations.distanceBetween(tile, npc.getLocation());
				if ((lastDistance == -1.0D) || (lastDistance > distance)) {
					lastDistance = distance;
					nearest = npc;
				}
			}
		}
		return nearest;
	}

}