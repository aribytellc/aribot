/**
 * 
 */
package com.aribot.bot.api.methods.nodes;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.util.Random;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Menu {

	private static Client client = GameLoader.getClient();
	public static final Pattern HTML_COLOR_TAG = Pattern.compile("(^[^<]+>|<[^>]+>|<[^>]+$)");

	private static String[] correctOrderActions() {
		String[] actions = client.getMenuActions();
		int optionsSize = getCount();
		String[] revActions = new String[optionsSize];
		for (int i = 0; i < optionsSize; i++) {
			revActions[i] = stripFormatting(actions[optionsSize - 1 - i]);
		}
		return revActions;
	}

	private static String[] correctOrderOptions() {
		String[] options = client.getMenuOptions();
		int optionsSize = getCount();
		String[] revOptions = new String[optionsSize];
		for (int i = 0; i < optionsSize; i++) {
			revOptions[i] = stripFormatting(options[optionsSize - 1 - i]);
		}
		return revOptions;
	}

	public static String stripFormatting(final String input) {
		return HTML_COLOR_TAG.matcher(input).replaceAll("");
	}

	/**
	 * Gets the actions of the current menu (<u>Follow</u> ABC (1))
	 * @return String array of the actions
	 */
	public static String[] getActions() {
		return correctOrderActions();
	}

	/**
	 * Checks if a specified action is in the current menu list
	 * @param action String action to check for
	 * @return boolean
	 */
	public static boolean actionsContain(String action) {
		List<String> actions = Arrays.asList(getActions());
		if (actions.contains(action)) {
			return true;
		}
		return false;
	}

	/**
	 * Checks if a specified option is in the current menu list
	 * @param option String action to check for
	 * @return boolean
	 */
	public static boolean optionsContain(String option) {
		List<String> options = Arrays.asList(getOptions());
		if (options.contains(option)) {
			return true;
		}
		return false;
	}


	/**
	 * Gets the options of the current menu (Follow <u>ABC (1)</u>)
	 * @return String array of the options
	 */
	public static String[] getOptions() {
		return correctOrderOptions();
	}

	/**
	 * Is the menu open
	 * @return boolean
	 */
	public static boolean isOpen() {
		return client.isMenuOpen();
	}

	/**
	 * Gets the point of the last open menu
	 * @return Point of the last open menu (top left location)
	 */
	public static Point getPosition() {
		int a = getX();
		int b = getY();
		return new Point(a, b);
	}

	/**
	 * Interacts with a menu (menu needs to be open)
	 * @param action The String action to interact with
	 * @return booelan of if it interacted correctly
	 */
	public static boolean select(String action) {
		return select(action, null);

	}

	/**
	 * Selects an action and option, taking into consideration the location of the wanted action
	 * @param action String action
	 * @param option String option
	 * @return boolean
	 */
	public static boolean select(String action, String option) {
		int index = getIndex(action, option);
		if (index < 0) {
			if (isOpen()) {
				Mouse.move(0, 0);
			}
			return false;
		}
		if (index == 0) {
			System.out.println("Action on top. Clicking");
			return Mouse.click(true);
		}
		if (isOpen()) {
			Point p = getPosition();
			int x = (int) (p.x + (Random.randomDouble(0.2, 0.8) * getWidth()));
			int y = p.y + 20 + (index * 15) + Random.nextInt(2, 12);
			Point target = new Point(x, y);
			return Mouse.move(target) && isOpen() && Mouse.click(true);
		}
		return Mouse.click(false) && select(action, option);
	}

	/**
	 * Gets the index of an action
	 * @param action String actions
	 * @return index of it
	 */
	public static int getIndex(String action) {
		return getIndex(action, null);
	}

	/**
	 * Gets the index of a specified action and option
	 * @param action String action
	 * @param option String option
	 * @return int
	 */
	public static int getIndex(String action, String option) {
		String[] actions = getActions();
		String[] options = getOptions();
		for (int i = 0; i < actions.length; i++) {
			if (actions[i].toLowerCase().contains(action.toLowerCase()) && (option != null ? options[i].toLowerCase().contains(option.toLowerCase()) : true)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Gets the last open menu X position (Top left)
	 * @return int of the point
	 */
	public static int getX() {
		return client.getMenuX() * client.getMultiplierMenuX();
	}

	/**
	 * Gets the last open menu Y position (Top left)
	 * @return int of the point
	 */
	public static int getY() {
		return client.getMenuY() * client.getMultiplierMenuY();
	}

	/**
	 * Gets the last open menu height
	 * @return int of the height
	 */
	public static int getHeight() {
		return client.getMenuHeight() * client.getMultiplierMenuHeight();
	}

	/**
	 * Gets the last open menu width
	 * @return int of the width
	 */
	public static int getWidth() {
		return client.getMenuWidth() * client.getMultiplierMenuWidth();
	}

	/**
	 * Gets the menu count (how many options are there to select)
	 * @return int of count
	 */
	public static int getCount() {
		return client.getMenuActionRow() * client.getMultiplierMenuActionRow();
	}

}
