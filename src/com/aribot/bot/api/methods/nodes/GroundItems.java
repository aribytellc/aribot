package com.aribot.bot.api.methods.nodes;

import java.util.ArrayList;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.GroundItem;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.Item;
import com.aribot.bot.interfaces.Node;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class GroundItems {

    private static Client c = GameLoader.getClient();

    /**
     * Gets the Ground Items that are loaded on the specified plane
     * @param plane
     * @return Array of GroundItem objects
     */
    public static GroundItem[] getLoaded(int plane) {
	com.aribot.bot.interfaces.GroundItems[][][] loadedItems = c.getGroundItems();
	ArrayList<GroundItem> items = new ArrayList<>();
	for (int i = 0; i < loadedItems[plane].length; i++) {
	    for (int j = 0; j < loadedItems[plane][i].length; j++) {
		if (loadedItems[plane][i][j] != null) {
		    /*
		     * This is how all this works.
		     * GroundItems is part of a sub-chain to Node, and it inherits the methods.
		     * Head is a GroundItems method which marks the top of the list
		     * We loop through the next in the stack, going downward.
		     * The list ends when we have the original top  of the list (the GroundItems reference)
		     * Since GroundItems inherits Node, we can do this recursively and are required to do so.
		     * A cast is needed for the Item which is okay, as it is a sub of Node.
		     */
		    Node n = loadedItems[plane][i][j].getHead();
		    Node ite = n.getNext();
		    while (ite != n) {
			items.add(new GroundItem(new Tile(i, j, plane, true), (((Item) ite).getID() * ((Item) ite).getMultiplierID())
				, (((Item) ite).getStackSize() * ((Item) ite).getMultiplierStackSize())));
			ite = ite.getNext();
		    }
		}
	    }
	}
	return items.toArray(new GroundItem[items.size()]);
    }

    /**
     * Gets the loaded GroundItems on the current plane
     * @return GroundItem object array
     */
    public static GroundItem[] getLoaded() {
	return getLoaded(Game.getPlane());
    }

    /**
     * Gets the GroundItems at a specified location (USES OFFSETS FROM MAPBASE!)
     * @param x X location
     * @param y Y location
     * @param z Z location
     * @return GroundItem object array.
     */
    public static GroundItem[] getGroundItemsAt(int x, int y, int z) { // using offsets
	com.aribot.bot.interfaces.GroundItems[][][] loadedItems = c.getGroundItems();
	ArrayList<GroundItem> items = new ArrayList<>();
	if (loadedItems[z][x][y] != null) {
	    Node n = loadedItems[z][x][y].getHead();
	    Node ite = n.getNext();
	    while (ite != n) {
		items.add(new GroundItem(new Tile(x, y, z, true), (((Item) ite).getID() * ((Item) ite).getMultiplierID())
			, (((Item) ite).getStackSize() * ((Item) ite).getMultiplierStackSize())));
		ite = ite.getNext();
	    }
	}
	return items.toArray(new GroundItem[items.size()]);
    }

    /**
     * Gets the GroundItems at a specified location
     * @param t Tile.
     * @return GroundItem object array
     */
    public static GroundItem[] getGroundItemsAt(Tile t) {
	return getGroundItemsAt(t.getXOffset(), t.getYOffset(), t.getZ());
    }

    /**
     * Gets the nearest GroundItem with the specified ID and base tile
     * @param tile Tile Tile to base the search from
     * @param IDs int array of IDs
     * @return GroundItem object that's the closest
     */
    public static GroundItem getNearest(Tile tile, int[] IDs) {
	GroundItem nearest = null;
	double lastDistance = -1.0D;
	GroundItem[] items = getLoaded();
	if (items == null) {
	    return null;
	}
	for (GroundItem item : items) {
	    for (int ids : IDs) {
		if (item.getID() == ids) {
		    double distance = Calculations.distanceBetween(tile, item.getLocation());
		    if ((lastDistance == -1.0D) || (lastDistance > distance)) {
			lastDistance = distance;
			nearest = item;
		    }
		}
	    }
	}
	return nearest;
    }

    /**
     * Gets the nearest GroundItem with specified IDs, starting from player location
     * @param IDs int array of IDs to look for
     * @return GroundItem object that is the closest
     */
    public static GroundItem getNearest(int...IDs) {
	return getNearest(Players.getMyPlayer().getLocation(), IDs);
    }
    
    /**
     * Gets the loaded GroundItem with a specified filter
     * @param filter Filter
     * @return GroundItem array
     */
    public static GroundItem[] getLoaded(Filter<GroundItem> filter) {
	ArrayList<GroundItem> inters = new ArrayList<>();
	GroundItem[] all = getLoaded();
	for (GroundItem i : all) {
	    if (i != null && filter.apply(i)) {
		inters.add(i);
	    }
	}
	return inters.toArray(new GroundItem[inters.size()]);
    }
    
    /**
     * Gets the nearest GroundItem with a specified filter
     * @param filter Filter
     * @return Single GroundItem
     */
    public static GroundItem getNearest(Filter<GroundItem> filter) {
	return getNearest(Players.getMyPlayer().getLocation(), filter);
    }
    
    /**
     * Gets the nearest GroundItem object with specified filter and base Tile
     * @param tile Location to use as a base
     * @param filter Filter
     * @return Single GroundItem object
     */
    public static GroundItem getNearest(Tile tile, Filter<GroundItem> filter) {
	GroundItem nearest = null;
	double lastDistance = -1.0D;
	GroundItem[] objects = getLoaded();
	if (objects == null) {
	    return null;
	}
	for (GroundItem object : objects) {
	    if (filter.apply(object)) {
		double distance = Calculations.distanceBetween(tile, object.getLocation());
		if ((lastDistance == -1.0D) || (lastDistance > distance)) {
		    lastDistance = distance;
		    nearest = object;
		}
	    }
	}
	return nearest;
    }
    
}
