package com.aribot.bot.api.methods.tabs;

public class Equipment {
 
    public static final int PARENT = 387;
    
    public static final int HEAD = 12;
    public static final int CAPE = 13;
    public static final int NECKLACE = 14;
    public static final int ARROWS = 15;
    public static final int SWORD = 16;
    public static final int BODY = 17;
    public static final int SHIELD = 18;
    public static final int LEGS = 19;
    public static final int BOOTS = 20;
    public static final int GLOVES = 21;
    public static final int RING = 22;
    
    public static final int ITEMS_ON_DEATH = 50;
    public static final int EQUIPMENT_STATS = 51;
    
}
