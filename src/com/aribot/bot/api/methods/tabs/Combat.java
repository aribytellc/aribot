package com.aribot.bot.api.methods.tabs;

import com.aribot.bot.api.methods.Tabs;
import com.aribot.bot.api.methods.Tabs.Tab;
import com.aribot.bot.api.methods.Widgets;

/**
 * Layout of the Attack style tab is as follows with this horribly done ASCII art.<br>
 * <br><pre>
 * |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|     |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|<br>
 * |_____One_____|     |_____Two_____|<br>
 * <br>
 * |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|     |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|<br>
 * |____Three____|     |_____Four____|</pre><br>
 * <br>
 * And there you have the horrible ASCII art of the attack styles interface.
 * <br>
 * Right now, there are no ways to detect which attack style is currently selected. This is a work in progress to figure it out.
 */
public class Combat {
    
    public static final int PARENT = 89;
    
    public static final int AUTO_RETALIATE = 31;
    public static final int ATTACK_SYTYLE_ONE = 32;
    public static final int ATTACK_SYTYLE_TWO = 33;
    public static final int ATTACK_SYTYLE_THREE = 34;
    public static final int ATTACK_SYTYLE_FOUR = 35;
    
    // 387.* equipment
    // 12 - 22 equipment
    
    /*
     * 92.* attack options
     * 0 unarmed
     * 24 auto retal 28 actual text
     */
    /*
     * About this...
     * There is NO WAY to detect which attack style is currently selected. There is not settings, and the Widgets do not reveal anything.
     * This has been put on the backburner until a method comes up to detect.
     */
    
    /**
     * Gets the Weapon name
     * @return String the weapon name
     */
    public static String getWeapon() {
	return Widgets.getInterface(PARENT, 0).getText();
    }
    
    /**
     * 1(stab) 2(lunge)
     * 3(slash) 4(block)
     */
    
    /**
     * Gets the First style option
     * @return String Text that is the style
     */
    public static String getStyleOne() {
	return Widgets.getInterface(PARENT, ATTACK_SYTYLE_ONE).getText();
    }
    
    /**
     * Gets the Second style option
     * @return String Text that is the style
     */
    public static String getStyleTwo() {
	return Widgets.getInterface(PARENT, ATTACK_SYTYLE_TWO).getText();
    }
    
    /**
     * Gets the Third style option
     * @return String Text that is the style
     */
    public static String getStyleThree() {
	return Widgets.getInterface(PARENT, ATTACK_SYTYLE_THREE).getText();
    }
    
    /**
     * Gets the Fourth style option
     * @return String Text that is the style
     */
    public static String getStyleFour() {
	return Widgets.getInterface(PARENT, ATTACK_SYTYLE_FOUR).getText();
    }
    
    /**
     * Is Auto retaliate on
     * @return boolean true if it is on
     */
    public static boolean isAutoRetal() {
	return Widgets.getInterface(PARENT, AUTO_RETALIATE).getTooltip().contains("Off");
    }
    
    /**
     * Sets the First attack option
     * @return boolean on succuess
     */
    public static boolean setStyleOne() {
	if (Tabs.isTabOpen(Tab.COMBAT_OPTIONS)) {
	    Widgets.getInterface(PARENT, ATTACK_SYTYLE_ONE).click();
	    return true;
	}
	return false;
    }
    
    /**
     * Sets the Second attack option
     * @return boolean on success
     */
    public static boolean setStyleTwo() {
	if (Tabs.isTabOpen(Tab.COMBAT_OPTIONS)) {
	    Widgets.getInterface(PARENT, ATTACK_SYTYLE_TWO).click();
	    return true;
	}
	return false;
    }
    
    /**
     * Sets the Third attack option
     * @return boolean on success
     */
    public static boolean setStyleThree() {
	if (Tabs.isTabOpen(Tab.COMBAT_OPTIONS)) {
	    Widgets.getInterface(PARENT, ATTACK_SYTYLE_THREE).click();
	    return true;
	}
	return false;
    }
    
    /**
     * Sets the Fourth attack option
     * @return boolean on success
     */
    public static boolean setStyleFour() {
	if (Tabs.isTabOpen(Tab.COMBAT_OPTIONS)) {
	    Widgets.getInterface(PARENT, ATTACK_SYTYLE_FOUR).click();
	    return true;
	}
	return false;
    }
    
}
