package com.aribot.bot.api.methods.tabs;

import java.util.ArrayList;
import java.util.HashSet;

import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.Item;
import com.aribot.bot.interfaces.Interfaces;

public class Inventory {
    
    /**
     * Get Items in inventory
     * @return Item object array
     */
    public static Item[] getItems() {
	Interfaces inv = Widgets.getInventory();
	ArrayList<Item> itemArrayList = new ArrayList<>();
	int[] items = inv.getSlotIDs();
	int[] stacks = inv.getStackSize();
	for (int i = 0; i < items.length; i++) {
	    if (items[i] != 0) {
		itemArrayList.add(new Item(items[i] - 1, stacks[i], i)); // -1 is forced due to item def. On inventory, the ID is 1 greater than it should. No apparent reason why.
	    }
	}
	return itemArrayList.toArray(new Item[itemArrayList.size()]);
    }
    
    
    /**
     * Gets the Item at a specified slot
     * @param slot int
     * @return Item
     */
    public static Item getItemAtSlot(int slot) {
	for (Item i : getItems()) {
	    if (i.getSlot() == slot) {
		return i;
	    }
	}
	return null;
    }
    
    /**
     * Gets Item object of the specified ID
     * @param id
     * @return Item object
     */
    public static Item getItem(int id) {
	for (Item i : getItems()) {
	    if (i.getID() == id) {
		return i;
	    }
	}
	return null;
    }
    
    /**
     * Searches for ANY of the given ids.
     * 
     * @param ids - IDs to check if one is in the inventory.
     * @return true if an item from the list of ids is found. false if none are found.
     */
    public static boolean contains(int...ids) {
	for (int i : ids) {
	    if (getItem(i) != null) {
		return true;
	    }
	}
	return false;
    }
    
    /**
     * Searches for ALL of the given ids.
     * 
     * @param ids List of IDs to check the inventory with.
     * @return true if each ID is found in the inventory. false if one or more is not found.
     */
    public static boolean containsAllOf(int...ids) {
	HashSet<Integer> idset = new HashSet<>();
	for (int i : ids) idset.add(i); //Get rid of duplicates
	
	for (Item i : getItems()) {
	    int id = i.getID();
	    if (idset.contains(id)) {
		idset.remove(id);
	    }
	}
	return idset.isEmpty();
    }
    
    /**
     * Counts the amount of Items in the inventory that match Filter
     * @param filter Filter
     * @return int
     */
    public static int getCount(Filter<Item> filter) {
	int count = 0;
	for (Item i : getItems()) {
	    if (filter.apply(i)) {
		count++;
	    }
	}
	return count;
    }
    
    /**
     * Gets the count of the Items that match one of the specified IDs
     * @param ids IDs to match
     * @return int
     */
    public static int getCount(final int...ids) {
	return(getCount(new Filter<Item>() {
	    @Override
	    public boolean apply(Item t) {
		for (int i : ids) {
		    if (t.getID() == i) {
			return true;
		    }
		}
		return false;
	    }
	}));
    }
    
    /**
     * Gets the count of the Items that match one of the specified names
     * @param names Names to match
     * @return int
     */
    public static int getCount(final String...names) {
	return getCount(new Filter<Item>() {
	    @Override
	    public boolean apply(Item t) {
		for (String s : names) {
		    if (t.getName().equals(s)) {
			return true;
		    }
		}
		return false;
	    } 
	});
    }
    
    /**
     * Gets the count of items in the inventory
     * @return int
     */
    public static int getCount() {
	return getItems().length;
    }
    
    /**
     * Is the inventory full
     * @return boolean, true on 28
     */
    public static boolean isFull() {
	return getCount() == 28;
    }
    
}