package com.aribot.bot.api.methods.tabs;

import java.util.ArrayList;
import java.util.Arrays;

import com.aribot.bot.api.methods.Widgets;

public class Prayer {

    public static final int PARENT = 271;

    public enum Prayers {

	THICK_SKIN(1, 5),
	BURST_OF_STRENGTH(4, 7),
	CLARITY_OF_THOUGHT(7, 9),
	SHARP_EYE(8, 11),
	MYSTIC_WILL(9, 13),
	ROCK_SKIN(10, 15),
	SUPERHUMAN_STRENGTH(13, 17),
	IMPROVED_REFLEXES(16, 19),
	RAPID_RESTORE(19, 21),
	RAPID_HEAL(22, 23),
	PROTECT_ITEMS(25, 25),
	HAWK_EYE(26, 27),
	MYSTIC_LORE(27, 29),
	STEEL_SKIN(28, 31),
	ULTIMATE_STRENGTH(31, 33),
	INCREDIBLE_REFLEXES(34, 35),
	PROTECT_FROM_MAGIC(37, 37),
	PROTECT_FROM_MISSILES(40, 39),
	PROTECT_FROM_MELEE(43, 41),
	EAGLE_EYE(44, 43),
	MYSTIC_MIGHT(45, 45),
	RETRIBUTION(46, 47),
	REDEMPTION(49, 49),
	BLESS_SYMBOL(50, 0), // Not in menu
	CURE_POISON(50, 0), // Not in menu
	SMITE(52, 51),
	CHIVALRY(60, 53),
	PIETY(70, 55);

	private int level;
	private int component;

	private Prayers(int l, int c) {
	    this.level = l;
	    this.component = c;
	}

	/**
	 * Gets the required level to use the prayer
	 * @return int
	 */
	public int getLevel() {
	    return level;
	}

	/**
	 * Gets the interface component for the prayer on the tab
	 * @return int
	 */
	public int getComponent() {
	    return component;
	}

	/**
	 * Can we use the prayer
	 * @return boolean
	 */
	public boolean canUse() {
	    return Prayer.getPrayerPoints() != 0 && Skills.getMaxLevel(Skills.PRAYER) >= level;
	}

	/**
	 * Is prayer activated
	 * @return boolean
	 */
	public boolean isActivated() {
	    return Arrays.asList(Widgets.getInterface(PARENT, component).getActions()).contains("Deactivate");
	}

    }

    /**
     * Gets the Prayer amount string. "Current / Max"
     * @return String in the form of Current / Max
     */
    public static String getPrayerAmounts() {
	return Widgets.getInterface(PARENT, 1).getText();
    }

    /**
     * Gets the remaining prayer left in percent.
     * @return int 0 to 100
     */
    public static int getPrayerPercent() {
	int maxPoints = getMaxPrayerPoints();
	if (maxPoints <= 0)
	    return 0;
	return (int) (((double) getPrayerPoints() / getMaxPrayerPoints()) * 100);
    }

    /**
     * Get how many prayer points we have currently
     * @return int
     */
    public static int getPrayerPoints() {
	return Skills.getRealLevel(Skills.PRAYER);
    }

    /**
     * Get the amount of prayer points we can have (max)
     * @return int
     */
    public static int getMaxPrayerPoints() {
	return Skills.getMaxLevel(Skills.PRAYER);
    }

    /**
     * Get the activated prayers
     * @return PRAYERS array
     */
    public static Prayers[] getActivated() {
	ArrayList<Prayers> activ = new ArrayList<>();
	for (Prayers pr : Prayers.values()) {
	    if (pr.isActivated()) {
		activ.add(pr);
	    }
	}
	return activ.toArray(new Prayers[activ.size()]);
    }

}
