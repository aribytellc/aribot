package com.aribot.bot.api.methods.tabs;

import com.aribot.bot.api.methods.Tabs;
import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.wrappers.Item;

/**
 * Layout of the Magic attack style tab is as follows with this horribly done ASCII art.<br>
 * <br><pre>
 * |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|     |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|<br>
 * |_____One_____|     |     Two     |<br>
 * |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|     |_____________|<br>
 * |____Three____|     |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|<br>
 * |&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;&oline;|     |     Five    |<br>
 * |_____Four____|     |_____________|</pre><br>
 * <br>
 * And there you have the horrible ASCII art of the magic attack styles interface.
 * <br>
 * Right now, there are no ways to detect which magic attack style is currently selected. This is a work in progress to figure it out.
 */
public class Magic {
    
    // 1(bash)    2(spell with def exp)
    // 3(pound)
    // 4(focus)   5(spell Raw magic)
    
    public static final int PARENT = 192;
    public static final int STYLE_OPTIONS_PARENT = 90; // With staff
    
    public enum Spell {
	
	HOME_TELEPORT(0, 0, 0),
	WIND_STRIKE(1, 1, 5.5),
	CONFUSE(3, 2, 13),
	BOLT_ENCHANTMENT(4, 3, 0),
	WATER_STRIKE(5, 4, 7.5),
	ENCHANT_LEVEL_1_JEWELRY(7, 5, 17.5),
	EARTH_STRIKE(9, 6, 9.5),
	WEAKEN(11, 7, 20.5),
	FIRE_STRIKE(13, 8, 11.5),
	BONES_TO_BANANAS(15, 9, 25),
	WIND_BOLT(17, 10, 13.5),
	CURSE(19, 11, 29),
	BIND(20, 12, 30),
	LOW_LEVEL_ALCHEMY(21, 13, 31),
	WATER_BOLT(23, 14, 16.5),
	VARROCK_TELEPORT(25, 15, 35),
	ENCHANT_LEVEL_2_JEWELRY(27, 16, 37),
	EARTH_BOLT(29, 17, 19.5),
	LUMBRIDGE_TELEPORT(31, 18, 41),
	TELEKINETIC_GRAB(33, 19, 43),
	FIRE_BOLT(35, 20, 22.5),
	FALADOR_TELEPORT(37, 21, 48),
	CRUMBLE_UNDEAD(39, 22, 24.5),
	HOUSE_TELEPORT(40, 23, 30),
	WIND_BLAST(41, 24, 25.5),
	SUPERHEAT_ITEM(43, 25, 53),
	CAMELOT_TELEPORT(45, 26, 55.5),
	WATER_BLAST(47, 27, 38.5),
	ENCHANT_LEVEL_3_JEWELRY(49, 28, 59),
	IBAN_BLAST(50, 29, 42.5),
	SNARE(50, 30, 60.5),
	MAGIC_DART(50, 31, 30),
	ARDOUGNE_TELEPORT(51, 32, 61),
	EARTH_BLAST(53, 33, 31.5),
	HIGH_LEVEL_ALCHEMY(55, 34, 65),
	CHARGE_WATER_ORB(56, 35, 66),
	ENCHANT_LEVEL_4_JEWELRY(57, 36, 67),
	WATCHTOWER_TELEPORT(58, 37, 68),
	FIRE_BLAST(59, 38, 34.5),
	BONES_TO_PEACHES(60, 39, 35.5),
	CHARGE_EARTH_ORB(60, 40, 70),
	CLAWS_OF_GUTHIX(60, 41, 35),
	SARADOMIN_STRIKE(60, 42, 35),
	FLAMES_OF_ZAMORAK(60, 43, 35),
	TROLLHEIM_TELEPORT(61, 44, 68),
	WIND_WAVE(62, 45, 36),
	CHARGE_FIRE_ORB(63, 46, 73),
	APE_ATOLL_TELEPORT(64, 47, 74),
	WATER_WAVE(65, 48, 37.5),
	CHARGE_AIR_ORB(66, 49, 76),
	VULNERABILITY(66, 50, 76),
	ENCHANT_LEVEL_5_JEWELRY(68, 51, 78),
	EARTH_WAVE(70, 52, 40),
	ENFEEBLE(73, 53, 83),
	TELEOTHER_LUMBRIDGE(74, 54, 84),
	FIRE_WAVE(75, 55, 42.5),
	ENTANGLE(79, 56, 91),
	STUN(80, 57, 90),
	CHARGE(80, 58, 180),
	TELEOTHER_FALADOR(82, 59, 92),
	TELE_BLOCK(85, 60, 80),
	ENCHANT_LEVEL_6_JEWELRY(87, 61, 97),
	TELEOTHER_CAMELOT(90, 62, 100);
	
	private int level;
	private int component;
	private double baseExp;
	
	private Spell(int level, int component, double baseExp) {
	    this.level = level;
	    this.component = component;
	    this.baseExp = baseExp;
	}
	
	/**
	 * Get the level required to cast the spell
	 * @return int
	 */
	public int getLevel() {
	    return level;
	}
	
	/**
	 * Get the component of the spell
	 * @return int
	 */
	public int getComponent() {
	    return component;
	}
	
	/**
	 * Get the experience gained from casting the spell (not including any extra damage experience)
	 * @return double
	 */
	public double getBaseExp() {
	    return baseExp;
	}
	
	/**
	 * Can we cast this spell
	 * @return boolean
	 */
	public boolean canCast() {
	    return Skills.getRealLevel(Skills.MAGIC) >= level;
	}
	
	/**
	 * Click on the spell
	 * @return boolean true on success
	 */
	public boolean click() {
	    if (!Tabs.Tab.MAGIC.open()) {
		return false;
	    }
	    Widgets.getInterface(PARENT, component).click();
	    return true;
	}
	
	/**
	 * Cast the spell on a character
	 * @param character Character instance (Player or NPC)
	 * @return boolean true on success
	 */
	public boolean castOn(com.aribot.bot.api.wrappers.Character character) {
	    if (click()) {
		character.click(true);
		return true;
	    }
	    return false;
	}
	
	/**
	 * Cast the spell on an item
	 * @param item Item
	 * @return boolean true on success
	 */
	public boolean castOn(Item item) {
	    if (click()) {
		item.click(true);
		return true;
	    }
	    return false;
	}
	
    }
    
    /**
     * Gets the currently selected Weapon (staff)
     * @return String the Weapon
     */
    public static String getWeapon() {
	return Widgets.getInterface(STYLE_OPTIONS_PARENT, 0).getText();
    }
    
    public static final int ATTACK_SYTYLE_ONE = 218;
    public static final int ATTACK_SYTYLE_TWO = 104; // This is not the whole button, but the shield image on the button
    public static final int ATTACK_SYTYLE_THREE = 180;
    public static final int ATTACK_SYTYLE_FOUR = 181;
    public static final int ATTACK_SYTYLE_FIVE = 182;
    
    /**
     * Sets an attack style
     * @param style Style to use
     * @return boolean on success
     */
    public static boolean setStyle(int style) {
	if (Tabs.Tab.MAGIC_COMBAT_OPTIONS.open()) {
	    Widgets.getInterface(PARENT, style).click();
	    return true;
	}
	return false;
    }
    
    // 0 staff of air
    // 10 auto retaliate   176 -  tooltip
    // 1(bash)    2(spell with def exp)
    // 3(pound)
    // 4(focus)   5(spell Raw magic)
    // 
    // pound 180
    // focus 181
    // bash 218
    // spell (raw) 182
    // spell (def and shield image) 104
    // 184 - 217 (raw magic spells)
    // 219 - 252 (magic and defense exp spells)
    
    // When style spell is selected, PARENT goes away and there is no record or any of the widgets. in PARENT, there are, however that is before you enter the spell selection.
    
    // Again, no visible way to determine which style is selected.
    
}
