package com.aribot.bot.api.methods.input;

import com.aribot.bot.api.internal.events.InputManager;

public class Keyboard {

	/**
	 * Sends a character (Does not press return)
	 * @param c character
	 */
	public static void sendKey(final char c) {
		InputManager.sendKey(c);
	}

	/**
	 * Sends a string with small key delays (Presses return)
	 * @param text String of text to send
	 * @param pressEnter boolean
	 */
	public static void sendText(final String text, final boolean pressEnter) {
		InputManager.sendKeys(text, pressEnter);
	}

	/**
	 * Sends a string with no delays (Presses return)
	 * @param text String of text to send
	 * @param pressEnter boolean
	 */
	public static void sendTextInstant(final String text, final boolean pressEnter) {
		InputManager.sendKeysInstant(text, pressEnter);
	}

	/**
	 * Presses a character
	 * @param c character
	 */
	public static void pressKey(final char c) {
		InputManager.pressKey(c);
	}

	/**
	 * Releases a character
	 * @param c character
	 */
	public static void releaseKey(final char c) {
		InputManager.releaseKey(c);
	}
}
