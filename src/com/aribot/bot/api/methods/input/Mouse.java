package com.aribot.bot.api.methods.input;

import java.awt.Point;

import com.aribot.bot.api.interfaces.Clickable;
import com.aribot.bot.api.internal.events.InputManager;
import com.aribot.bot.api.internal.events.MouseHandler;
import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.nodes.Menu;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Mouse {

	private static int mouseSpeed = MouseHandler.DEFAULT_MOUSE_SPEED;

	private static int random(int min, int max) {
		return Calculations.random(min, max);
	}

	private static void sleep(int time) {
		Calculations.sleep(time);
	}

	private static Client getClient() {
		return GameLoader.getClient();
	}

	/**
	 * Move the mouse randomly (min of 1)
	 * @param maxDistance int
	 */
	public static void moveRandomly(int maxDistance) {
		moveRandomly(1, maxDistance);
	}

	/**
	 * Move the mouse randomly
	 * @param minDistance
	 * @param maxDistance
	 */
	public static void moveRandomly(int minDistance, int maxDistance) {
		double xvec = Math.random();
		if (random(0, 2) == 1) {
			xvec = -xvec;
		}
		double yvec = Math.sqrt(1 - xvec * xvec);
		if (random(0, 2) == 1) {
			yvec = -yvec;
		}
		double distance = maxDistance;
		Point p = getLocation();
		int maxX = (int) Math.round(xvec * distance + p.x);
		distance -= Math.abs((maxX - Math.max(0, Math.min(GameLoader.getApplet().getWidth(), maxX))) / xvec);
		int maxY = (int) Math.round(yvec * distance + p.y);
		distance -= Math.abs((maxY - Math.max(0, Math.min(GameLoader.getApplet().getHeight(), maxY))) / yvec);
		if (distance < minDistance) {
			return;
		}
		distance = random(minDistance, (int) distance);
		move((int) (xvec * distance) + p.x, (int) (yvec * distance) + p.y);
	}

	/**
	 * Move the mouse off the screen
	 */
	public static void moveOffScreen() {
		if (isPresent()) {
			switch (random(0, 4)) {
			case 0: // up
				move(random(-10, GameLoader.getApplet().getWidth() + 10), random(-100, -10));
				break;
			case 1: // down
				move(random(-10, GameLoader.getApplet().getWidth() + 10), GameLoader.getApplet().getHeight() + random(10, 100));
				break;
			case 2: // left
				move(random(-100, -10), random(-10, GameLoader.getApplet().getHeight() + 10));
				break;
			case 3: // right
				move(random(10, 100) + GameLoader.getApplet().getWidth(), random(-10, GameLoader.getApplet().getHeight() + 10));
				break;
			}
		}
	}

	/**
	 * Drag the mouse
	 * @param x X to stop
	 * @param y Y to stop
	 */
	public static void drag(int x, int y) {
		InputManager.dragMouse(x, y);
	}

	/**
	 * Drag the mouse
	 * @param p Point to stop
	 */
	public static void drag(Point p) {
		drag(p.x, p.y);
	}

	/**
	 * Click the mouse at current position
	 * @param leftClick boolean
	 */
	public static boolean click(boolean leftClick) {
		return click(leftClick, MouseHandler.DEFAULT_MAX_MOVE_AFTER);
	}

	/**
	 * Click the mouse at current position and move after
	 * @param leftClick boolean
	 * @param moveAfterDist int
	 */
	public static boolean click(boolean leftClick, int moveAfterDist) {
		if (InputManager.clickMouse(leftClick)) {
			if (moveAfterDist > 0) {
				sleep(random(50, 350));
				Point pos = getLocation();
				move(pos.x - moveAfterDist, pos.y - moveAfterDist, moveAfterDist * 2, moveAfterDist * 2);
			}
			return true;
		}
		return false;
	}

	/**
	 * Click the mouse at specified position
	 * @param x X position
	 * @param y Y position
	 * @param leftClick boolean
	 */
	public static boolean click(int x, int y, boolean leftClick) {
		return click(x, y, 0, 0, leftClick);
	}

	/**
	 * Click the mouse at specified position
	 * @param x X position
	 * @param y Y position
	 * @param randX Random X
	 * @param randY Random Y
	 * @param leftClick boolean
	 */
	public static boolean click(int x, int y, int randX, int randY, boolean leftClick) {
		move(x, y, randX, randY);
		sleep(random(50, 350));
		return click(leftClick, MouseHandler.DEFAULT_MAX_MOVE_AFTER);
	}

	/**
	 * Click the mouse at specified position
	 * @param x X position
	 * @param y Y position
	 * @param randX Random X
	 * @param randY Random Y
	 * @param leftClick boolean
	 * @param moveAfterDist int
	 */
	public static boolean click(int x, int y, int randX, int randY, boolean leftClick, int moveAfterDist) {
		move(x, y, randX, randY);
		sleep(random(50, 350));
		return click(leftClick, moveAfterDist);
	}

	/**
	 * Click mouse at specified position
	 * @param p Point to click
	 * @param leftClick boolean
	 */
	public static boolean click(Point p, boolean leftClick) {
		return click(p.x, p.y, leftClick);
	}

	/**
	 * Click mouse at specified position
	 * @param p Point to click
	 * @param x Random X
	 * @param y Random Y
	 * @param leftClick boolean
	 */
	public static boolean click(Point p, int x, int y, boolean leftClick) {
		return click(p.x, p.y, x, y, leftClick);
	}

	/**
	 * Click mouse at specified position
	 * @param p Point to click
	 * @param x Random X
	 * @param y Random Y
	 * @param leftClick boolean
	 * @param moveAfterDist int
	 */
	public static boolean click(Point p, int x, int y, boolean leftClick, int moveAfterDist) {
		return click(p.x, p.y, x, y, leftClick, moveAfterDist);
	}

	/**
	 * Click the mouse at the current position with small randomness
	 */
	public static void clickSlightly() {
		Point p = new Point((int) (getLocation().getX() + (Math.random() * 50 > 25 ? 1 : -1) * (30 + Math.random() * 90)), (int) (getLocation().getY() + (Math.random() * 50 > 25 ? 1 : -1) * (30 + Math.random() * 90)));
		if (p.getX() < 1 || p.getY() < 1 || p.getX() > 761 || p.getY() > 499) {
			clickSlightly();
			return;
		}
		click(p, true);
	}

	/**
	 * Gets the current speed of the mouse
	 * @return int
	 */
	public static int getSpeed() {
		return mouseSpeed;
	}

	/**
	 * Sets the speed of the mouse
	 * @param speed int (Low is faster)
	 */
	public static void setSpeed(int speed) {
		mouseSpeed = speed;
	}

	/**
	 * Move the mouse to specified position
	 * @param x X position
	 * @param y Y position
	 */
	public static boolean move(int x, int y) {
		return move(x, y, 0, 0);
	}

	/**
	 * Move the mouse to specified position
	 * @param x X position
	 * @param y Y position
	 * @param afterOffset int
	 */
	public static boolean move(int x, int y, int afterOffset) {
		return move(getSpeed(), x, y, 0, 0, afterOffset);
	}

	/**
	 * Moves mouse to specified position
	 * @param x X position
	 * @param y Y position 
	 * @param randX Random X
	 * @param randY Random Y
	 */
	public static boolean move(int x, int y, int randX, int randY) {
		return move(getSpeed(), x, y, randX, randY, 0);
	}

	/**
	 * Moves mouse to specified position at specified speed
	 * @param speed int (low is faster)
	 * @param x X position
	 * @param y Y position
	 * @param randX Random X
	 * @param randY Random Y
	 */
	public static boolean move(int speed, int x, int y, int randX, int randY) {
		return move(speed, x, y, randX, randY, 0);
	}

	/**
	 * Moves the mouse to a specified position at specified speed
	 * @param speed int (low is faster)
	 * @param x X position
	 * @param y Y position
	 * @param randX Random X
	 * @param randY Random Y
	 * @param afterOffset int
	 */
	public static boolean move(int speed, int x, int y, int randX, int randY, int afterOffset) {
		if (x != -1 || y != -1) {
			if (InputManager.moveMouse(speed, x, y, randX, randY)) {
				if (afterOffset > 0) {
					sleep(random(60, 300));
					Point pos = getLocation();
					move(pos.x - afterOffset, pos.y - afterOffset, afterOffset * 2, afterOffset * 2);
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * Move the mouse at specified speed to speficied point
	 * @param speed int (low is faster)
	 * @param p Point to move to
	 */
	public static boolean move(int speed, Point p) {
		return move(speed, p.x, p.y, 0, 0, 0);
	}

	/**
	 * Move the mouse to a specified point
	 * @param p Point to move to
	 */
	public static boolean move(Point p) {
		return move(p.x, p.y, 0, 0);
	}

	/**
	 * Move the mouse to a specified position
	 * @param p Point to move to
	 * @param afterOffset int
	 */
	public static boolean move(Point p, int afterOffset) {
		return move(getSpeed(), p.x, p.y, 0, 0, afterOffset);
	}

	/**
	 * Move mouse to specified position
	 * @param p Point to move to
	 * @param randX Random X
	 * @param randY Random Y
	 */
	public static boolean move(Point p, int randX, int randY) {
		return move(p.x, p.y, randX, randY);
	}

	/**
	 * Move mouse to specified position
	 * @param p Point to move to
	 * @param randX Random X
	 * @param randY Random Y
	 * @param afterOffset int
	 */
	public static boolean move(Point p, int randX, int randY, int afterOffset) {
		return move(getSpeed(), p.x, p.y, randX, randY, afterOffset);
	}

	/**
	 * Moves mouse instantly to position
	 * @param x X position
	 * @param y Y position
	 */
	public static void hop(int x, int y) {
		InputManager.hopMouse(x, y);
	}

	/**
	 * Moves mouse instantly to position
	 * @param p Point to move to
	 */
	public static void hop(Point p) {
		hop(p.x, p.y);
	}

	/**
	 * Moves mouse instantly to position
	 * @param x X position
	 * @param y Y position
	 * @param randX Random X
	 * @param randY Random Y
	 */
	public static void hop(int x, int y, int randX, int randY) {
		hop(x + random(-randX, randX), y + random(-randX, randY));
	}

	/**
	 * Moves mouse instantly to position
	 * @param p Point to move to
	 * @param randX Random X
	 * @param randY Random Y
	 */
	public static void hop(Point p, int randX, int randY) {
		hop(p.x, p.y, randX, randY);
	}

	/**
	 * Moves mouse with small randomness
	 */
	public static void moveSlightly() {
		Point p = new Point((int) (getLocation().getX() + (Math.random() * 50 > 25 ? 1 : -1) * (30 + Math.random() * 90)), (int) (getLocation().getY() + (Math.random() * 50 > 25 ? 1 : -1) * (30 + Math.random() * 90)));
		if (p.getX() < 1 || p.getY() < 1 || p.getX() > 761 || p.getY() > 499) {
			moveSlightly();
			return;
		}
		move(p);
	}

	/**
	 * Get a random number given a max, and add to mouse current location
	 * @param maxDistance int
	 * @return Mouse current X plus random
	 */
	public static int getRandomX(int maxDistance) {
		Point p = getLocation();
		if (p.x < 0 || maxDistance <= 0) {
			return -1;
		}
		if (random(0, 2) == 0) {
			return p.x - random(0, p.x < maxDistance ? p.x : maxDistance);
		}
		int dist = GameLoader.getApplet().getWidth() - p.x;
		return p.x + random(1, dist < maxDistance && dist > 0 ? dist : maxDistance);
	}

	/**
	 * Get a random number given a max, and add to mouse current location
	 * @param maxDistance int
	 * @return Mouse current Y plus random
	 */
	public static int getRandomY(int maxDistance) {
		Point p = getLocation();
		if (p.y < 0 || maxDistance <= 0) {
			return -1;
		}
		if (random(0, 2) == 0) {
			return p.y - random(0, p.y < maxDistance ? p.y : maxDistance);
		}
		int dist = GameLoader.getApplet().getHeight() - p.y;
		return p.y + random(1, dist < maxDistance && dist > 0 ? dist : maxDistance);

	}

	/**
	 * Gets the current mouse location
	 * @return Point
	 */
	public static Point getLocation() {
		return new Point(com.aribot.bot.interfaces.io.Mouse.getX(), com.aribot.bot.interfaces.io.Mouse.getY());
	}

	/**
	 * Gets the last mouse press location
	 * @return Point
	 */
	public static Point getPressLocation() {
		return new Point(com.aribot.bot.interfaces.io.Mouse.getPressX(), com.aribot.bot.interfaces.io.Mouse.getPressY());
	}

	/**
	 * Gets the amount of time the mouse was pressed, last time it was pressed
	 * @return long 
	 */
	public static long getPressTime() {
		return getClient().getMouse() == null ? 0 : com.aribot.bot.interfaces.io.Mouse.getPressTime();
	}

	/**
	 * Is the mouse present on the screen
	 * @return boolean
	 */
	public static boolean isPresent() {
		return getClient().getMouse() != null && com.aribot.bot.interfaces.io.Mouse.isPresent();
	}

	/**
	 * Is the mouse pressed currently
	 * @return boolean
	 */
	public static boolean isPressed() {
		return getClient().getMouse() != null && com.aribot.bot.interfaces.io.Mouse.isPressed();
	}

	/**
	 * Selects the provided action/option on the provided Clickable target.
	 * 
	 * @param target - An NPC, Player, etc... that implements the Clickable interface.
	 * @param action An action to select, aka "Chop down"
	 * @param option An option to select, aka "Tree"
	 * @return true/false whether the interaction succeeded or not after 3 attempts.
	 */
	public static boolean interact(final Clickable target, final String action, final String option) {
		return interact(target, action, option, 0);
	}

	/**
	 * @see {@link #interact(Clickable, String, String)}
	 */
	private static boolean interact(final Clickable target, final String action, final String option, final int tries) {
		if (tries > 3) {
			return false;
		}
		Point targetPoint = target.getRandomPoint();
		return (Mouse.move(targetPoint) && Menu.actionsContain(action) && Menu.select(action, option)) || interact(target, action, option, tries + 1);
	}
}
