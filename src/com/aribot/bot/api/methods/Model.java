package com.aribot.bot.api.methods;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.ArrayList;

import com.aribot.bot.api.interfaces.Clickable;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.wrappers.Animables;
import com.aribot.bot.interfaces.Models;

public class Model extends Animables implements Clickable {
    
    private int[] indices1;
    private int[] indices2;
    private int[] indices3;
    
    private int[] xPoints;
    private int[] yPoints;
    private int[] zPoints;
    private int[] xPoints2;
    private int[] yPoints2;
    private int[] zPoints2;
    
    private int x;
    private int y;
    
    private int numFaces;
    private Object object;
    
    public Model(Models accessor, int worldX, int worldY) {
	this(accessor, worldX, worldY, null);
    }
    
    public Model(Models accessor, int worldX, int worldY, Object object) {
	super(accessor);
	if (accessor != null) {
	    this.indices1 = accessor.getXTriangles();
	    this.indices2 = accessor.getYTriangles();
	    this.indices3 = accessor.getZTriangles();
	    
	    this.xPoints = accessor.getXVerticies();
	    this.yPoints = accessor.getZVerticies();
	    this.zPoints = accessor.getYVerticies();
	    
	    this.xPoints2 = accessor.getXVerticies();
	    this.yPoints2 = accessor.getZVerticies();
	    this.zPoints2 = accessor.getYVerticies();
	    
	    this.x = worldX;
	    this.y = worldY;
	    
	    // Math.min(xPoints.length, Math.min(yPoints.length, zPoints.length));
	    this.numFaces = Math.min(indices1.length, Math.min(indices2.length, indices3.length));
	    this.object = object;
	}
    }
    
    /**
     * Gets the center Point of the model (Used for clicking)
     * @return Point
     */
    public Point getPoint() {
	try {
	    Polygon[] triangles = getTriangles();
	    int x = 0, y = 0, total = 0;
	    for (Polygon poly : triangles) {
		for (int i = 0; i < poly.npoints; i++) {
		    x += poly.xpoints[i];
		    y += poly.ypoints[i];
		    total++;
		}
	    }
	    Point central = new Point(x / total, y / total);
	    Point curCentral = null;
	    double dist = 20000D;
	    for (Polygon poly : triangles) {
		for (int i = 0; i < poly.npoints; i++) {
		    Point p = new Point(poly.xpoints[i], poly.ypoints[i]);
		    if (!Calculations.isOnScreen(p)) {
			continue;
		    }
		    double dist2 = Math.hypot(p.x - central.x, p.y - central.y);
		    if (curCentral == null || dist2 < dist) {
			curCentral = p;
			dist = dist2;
		    }
		}
	    }
	    return curCentral;
	} catch (Exception ignored) {
	}
	return new Point(-1, -1);
    }
    
    public Point[] getPoints() {
	ArrayList<Point> points = new ArrayList<>();
	for (Polygon p : getTriangles()) {
	    int[] xpoints = p.xpoints;
	    int[] ypoints = p.ypoints;
	    
	    for (int i = 0; i < xpoints.length; i++) {
		int x = xpoints[i];
		int y = ypoints[i];
		Point point = new Point(x, y);
		points.add(point);
	    }
	}
	return points.toArray(new Point[points.size()]);
    }
    
    public Polygon[] getTriangles() {
	ArrayList<Polygon> polygons = new ArrayList<>();
	
	if (object != null && (object instanceof com.aribot.bot.interfaces.Character)) {
	    int theta = ((com.aribot.bot.interfaces.Character) object).getRotation() * ((com.aribot.bot.interfaces.Character) object).getMultiplierRotation();
	    int sin = Calculations.SIN[theta];
	    int cos = Calculations.COS[theta];
	    int[] xOrig = xPoints2;
	    int[] zOrig = zPoints2;
	    xPoints = new int[xPoints2.length];
	    zPoints = new int[zPoints2.length];
	    for (int i = 0; i < xOrig.length; i++) {
		xPoints[i] = xOrig[i] * cos + zOrig[i] * sin >> 16;
				zPoints[i] = zOrig[i] * cos - xOrig[i] * sin >> 16;
	    }
	}
	if (object != null && (object instanceof com.aribot.bot.api.wrappers.GroundItem)) {
	    x += 64; // Offset the position from the corner of the tile, to the center.
	    y += 64; // 0.5 * 128
	}
	
	int len = indices1.length;
	for (int i = 0; i < len; ++i) {
	    Point p1 = Calculations.translateTilePosition(-yPoints[indices1[i]], x + xPoints[indices1[i]], y + zPoints[indices1[i]]);
	    Point p2 = Calculations.translateTilePosition(-yPoints[indices2[i]], x + xPoints[indices2[i]], y + zPoints[indices2[i]]);
	    Point p3 = Calculations.translateTilePosition(-yPoints[indices3[i]], x + xPoints[indices3[i]], y + zPoints[indices3[i]]);
	    
	    if (p1.x >= 0 && p2.x >= 0 && p2.x >= 0 && p3.x > 0 && p3.y > 0) {
		if (Calculations.isOnScreen(p1) && Calculations.isOnScreen(p2) && Calculations.isOnScreen(p3)) {
		    Polygon p = new Polygon(new int[] { p1.x, p2.x, p3.x }, new int[] { p1.y, p2.y, p3.y }, 3);
		    polygons.add(p);
		}
	    }
	}
	return polygons.toArray(new Polygon[polygons.size()]);
    }
    
    /**
     * Is this a valid model?
     * @return boolean
     */
    public boolean isValid() {
	return indices1 != null;
    }
    
    public boolean contains(Point p) {
	for (Polygon poly : this.getTriangles()) {
	    if (poly.contains(p)) {
		return true;
	    }
	}
	return false;
    }
    
    private Point getPointInRange(final int start, final int end) {
	for (int i = start; i < end; ++i) {
	    final Point one = Calculations.translateTilePosition(-yPoints[indices1[i]], x + xPoints[indices1[i]], y + zPoints[indices1[i]]);
	    int x2 = -1, y2 = -1;
	    if (one.x >= 0) {
		x2 = one.x;
		y2 = one.y;
	    }
	    final Point two = Calculations.translateTilePosition(-yPoints[indices2[i]], x + xPoints[indices2[i]], y + zPoints[indices2[i]]);
	    if (two.x >= 0) {
		if (x >= 0) {
		    x2 = (x2 + two.x) / 2;
		    y2 = (y2 + two.y) / 2;
		} else {
		    x2 = two.x;
		    y2 = two.y;
		}
	    }
	    final Point three = Calculations.translateTilePosition(-yPoints[indices3[i]], x + xPoints[indices3[i]], y + zPoints[indices3[i]]);
	    if (three.x >= 0) {
		if (x >= 0) {
		    x2 = (x2 + three.x) / 2;
		    y2 = (y2 + three.y) / 2;
		} else {
		    x2 = three.x;
		    y2 = three.y;
		}
	    }
	    if (x2 >= 0) {
		return new Point(x2, y2);
	    }
	}
	return new Point(-1, -1);
    }
    
    /**
     * Gets a Random Point on the model (Used for clicking)
     * @return Point
     */
    @Override
    public Point getRandomPoint() {
	final int len = numFaces;
	final int sever = Calculations.random(1, len);
	Point point = getPointInRange(sever, len);
	if (Calculations.isOnScreen(point)) {
	    return point;
	}
	point = getPointInRange(0, sever);
	if (Calculations.isOnScreen(point)) {
	    return point;
	}
	return new Point(-1, -1);
    }
    
    /**
     * Paints a model with Color.WHITE
     * @param g Graphics to paint with
     */
    public void paint(Graphics g) {
	if (isValid()) {
	    Color orig = g.getColor();
	    g.setColor(Color.WHITE);
	    for (Polygon p : getTriangles()) {
		g.drawPolygon(p);
	    }
	    g.setColor(orig);
	}
    }
    
    @Override
    public boolean interact(String action) {
	return Mouse.interact(this, action, null);
    }
    
    @Override
    public boolean interact(String action, String option) {
	return Mouse.interact(this, action, option);
    }
    
}
