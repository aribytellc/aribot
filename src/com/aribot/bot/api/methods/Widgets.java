package com.aribot.bot.api.methods;

import com.aribot.bot.api.wrappers.Widget;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.Interfaces;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Widgets {

    private static Client c = GameLoader.getClient();

    /**
     * Gets the Inventory Widget
     * @return Interfaces object of the inventory
     */
    public static Interfaces getInventory() {
	return c.getInterfaces()[149][0];
    }

    /**
     * Gets the interfaces double array
     * @return Interfaces[][]
     */
    public static Interfaces[][] getInterfaces() {
	return c.getInterfaces();
    }

    /**
     * Gets the parent Widget of an interface
     * @param one folder number (<u>149</u>.0)
     * @param two interface number (149.<u>0</u>)
     * @return Widget that is the parent. Null if non-existent
     */
    public static Widget getParent(int one, int two) {
	Widget inter = getInterface(one, two);
	return inter == null ? null : inter.getParent();
    }

    /**
     * Gets the parent Widget of an interface using the Parent ID
     * @param parentID the Parent ID of the widget
     * @return Widget that is the parent. Null if non-existent
     */
    public static Widget getParent(int parentID) {
	return getParent(parentID >> 16, parentID & 0xFFFF);
    }

    /**
     * Gets a widget from the folder number and interface number (Defaults to 549.0)
     * @param one folder number (<u>149</u>.0)
     * @param two interface number (149.<u>0</u>)
     * @return a Widget of the interface
     */
    public static Widget getInterface(int one, int two) {
	Interfaces[][] interList = c.getInterfaces();
	if (interList[one][two] != null) {
	    return new Widget(interList[one][two]);
	}
	return new Widget();
    }

}
