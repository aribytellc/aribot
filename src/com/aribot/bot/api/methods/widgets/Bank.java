package com.aribot.bot.api.methods.widgets;

import java.util.ArrayList;

import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.methods.input.Keyboard;
import com.aribot.bot.api.methods.tabs.Inventory;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.BankItem;
import com.aribot.bot.api.wrappers.Item;
import com.aribot.bot.api.wrappers.Widget;

public class Bank {
    
    public static final int PARENT = 12;
    
    // 15.0 has an alternate 149.0 for inventory items
    
    /**
     * Gets the Items in the bank
     * @return Item array of items in the bank. null if the bank is not open.
     */
    public static BankItem[] getItems() {
	Widget bank = Widgets.getInterface(PARENT, 89);
	if (bank != null) {
	    ArrayList<BankItem> itemArrayList = new ArrayList<>();
	    int[] items = bank.getSlotIDs();
	    int[] stacks = bank.getStackSizes();
	    int row = 0;
	    int column = 0;
	    for (int i = 0; i < items.length; i++) {
		if (items[i] != 0) {
		    if (column >= 8) {
			column = 0;
			row++;
		    }
		    itemArrayList.add(new BankItem(items[i] + 1, stacks[i], column, row)); // +1 is forced due to item def. On bank, the ID is 1 less than it should. No apparent reason why.
		    column++;
		}
	    }
	    return itemArrayList.toArray(new BankItem[itemArrayList.size()]);
	}
	return null;
    }
    
    /**
     * Gets the count of the items in the bank, not counting all of the stacks
     * @return int
     */
    public static int getCount() {
	return getCount(false);
    }
    
    /**
     * Gets the count of the items in the bank
     * @param stackCounts Include stacks in the count?
     * @return int
     */
    public static int getCount(boolean stackCounts) {
	return getCount(new Filter<BankItem>() {
	    @Override
	    public boolean apply(BankItem t) {
		return true;
	    }
	}, stackCounts);
    }
    
    /**
     * Gets the count of a Filter of items in the bank
     * @param filter Filter
     * @param stackCounts Include stacks in the count?
     * @return int
     */
    public static int getCount(Filter<BankItem> filter, boolean stackCounts) {
	int count = 0;
	for (BankItem item : getItems()) {
	    if (filter.apply(item)) {
		count += ((stackCounts) ? item.getStackSize() : 1);
	    }
	    
	}
	return count;
    }
    
    /**
     * Does the bank contian ANY of the IDs given?
     * @param ids int... IDs
     * @return boolean
     */
    public static boolean contains(int...ids) {
	for (int i : ids) {
	    if (getItem(i) != null) {
		return true;
	    }
	}
	return false;
    }
    
    /**
     * Gets an Item in th ebank with specified ID
     * @param id int ID
     * @return BankItem
     */
    public static BankItem getItem(int id) {
	for (BankItem i : getItems()) {
	    if (i.getID() == id) {
		return i;
	    }
	}
	return null;
    }
    
    /**
     * Deposit IDs into the bank from the Inventory
     * @param IDs IDs to deposit
     * @return boolean
     */
    public static boolean deposit(int... IDs) {
	if (Inventory.contains(IDs)) {
	    for (int i : IDs) {
		deposit(i, 1);
	    }
	    return true;
	}
	return false;
    }
    
    /**
     * Deposit items into the bank from the Inventory
     * @param ID ID to deposit
     * @param count int amount to deposit
     * @return boolean
     */
    public static boolean deposit(int ID, int count) {
	if (Inventory.contains(ID)) {
	    Item i = Inventory.getItem(ID);
	    if (count == i.getStackSize()) {
		if (i.interact("Store All")) {
		    return true;
		}
	    } else {
		if (i.interact("Store X")) {
		    Keyboard.sendText("" + count, true);
		}
	    }
	}
	return false;
    }
    
    /**
     * Deposit all IDs to the Bank from the Inventory
     * @param IDs Ids
     * @return boolean
     */
    public static boolean depositAll(int... IDs) {
	if (Inventory.contains(IDs)) {
	    for (int i : IDs) {
		while(Inventory.getCount(i) != 0) {
		    deposit(i, Inventory.getItem(i).getStackSize());
		}
	    }
	    if (!Inventory.contains(IDs)) {
		return true;
	    }
	}
	return false;
    }
    
    /**
     * Withdraw items from the bank into the Inventory
     * @param IDs IDs to withdraw
     * @return boolean
     */
    public static boolean withdraw(int... IDs) {
	if (Bank.contains(IDs)) {
	    for (int i : IDs) {
		withdraw(i, 1);
	    }
	    return true;
	}
	return false;
    }
    
    /**
     * Withdraw an item from the bank to the Inventory
     * @param ID ID to withdraw
     * @param count Amount to withdraw
     * @return boolean
     */
    public static boolean withdraw(int ID, int count) {
	if (Bank.contains(ID)) {
	    BankItem i = Bank.getItem(ID);
	    if (count == i.getStackSize()) {
		if (i.interact("Withdraw All")) {
		    return true;
		}
	    } else {
		if (i.interact("Withdraw X")) {
		    Keyboard.sendText("" + count, true);
		}
	    }
	}
	return false;
    }
    
    /**
     * Withdraw Items from the bank into the Inventory based on a Filter
     * @param filter Filter
     * @return boolean
     */
    public static boolean withdraw(Filter<BankItem> filter) {
	for (BankItem i : getItems()) {
	    if (filter.apply(i)) {
		withdraw(i.getID(), 1);
	    }
	}
	return true;
    }
    
    /**
     * Withdraw all Items from the bank into the Inventory based on a Filter
     * @param filter Filter
     * @return boolean
     */
    public static boolean withdrawAll(Filter<BankItem> filter) {
	for (BankItem i : getItems()) {
	    if (filter.apply(i)) {
		withdraw(i.getID(), i.getStackSize());
	    }
	}
	return true;
    }
    
}
