package com.aribot.bot.api.methods;

import java.awt.Point;
import java.awt.Rectangle;

import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class Calculations {

	private static Rectangle gameDimensions = new Rectangle(4, 4, 511, 334);
	private static Rectangle minimapDimensions = new Rectangle(545, 5, 170, 155);
	private static Rectangle gameWindowDimensions = new Rectangle(0, 0, 761, 501);
	private static Client client = GameLoader.getClient();
	public final static java.util.Random random = new java.util.Random();
	public static int[] SIN = new int[2048];
	public static int[] COS = new int[2048];

	static {
		for (int i = 0; i < SIN.length; i++) {
			SIN[i] = (int) (65536.0D * Math.sin(i * 0.0030679615D));
			COS[i] = (int) (65536.0D * Math.cos(i * 0.0030679615D));
		}
	}

	/**
	 * Gets the applet width
	 * @return int
	 */
	public static int getAppletWidth() {
		return GameLoader.getApplet().getWidth();
	}

	/**
	 * Gets the applet height
	 * @return int
	 */
	public static int getAppletHeight() {
		return GameLoader.getApplet().getHeight();
	}

	/**
	 * Is a point on the game screen (world view window)
	 * @param p point
	 * @return boolean
	 */
	public static boolean isOnScreen(Point p) {
		return gameDimensions.contains(p);
	}

	/**
	 * Is a Point on the screen (whole window)
	 * @param p Point
	 * @return boolean
	 */
	public static boolean isOnGameScreen(Point p) {
		return gameWindowDimensions.contains(p);
	}

	/**
	 * Gets a point on the screen where a Tile is
	 * @param x X tile location
	 * @param y Y tile location
	 * @param dX X offset
	 * @param dY Y offset
	 * @return Point on the screen. (-1, -1) if not on the screen
	 */
	public static Point tileToScreen(int x, int y, double dX, double dY) {
		return groundToScreen((int) ((x - Game.getMapBaseX() + dX) * 128), (int) ((y - Game.getMapBaseY() + dY) * 128));
	}

	/**
	 * Gets a point from a X and Y point using current plane
	 * @param x X point
	 * @param y Y point
	 * @return Point on the screen. (-1, -1) if not valid.
	 */
	public static Point groundToScreen(int x, int y) {
		return groundToScreen(x, y, Game.getPlane());
	}

	/**
	 * Gets a point from a X and Y point
	 * @param x X point
	 * @param y Y point
	 * @param height Plane
	 * @return Point on the screen. (-1, -1) if not valid.
	 */
	public static Point groundToScreen(int x, int y, int height) {
		if (client.getGroundByteArray() == null || x < 512 || y < 512 || x > 52224 || y > 52224) {
			return new Point(-1, -1);
		}
		return translateTilePosition(height, x, y);
	}

	/**
	 * Gets a point on the minimap of the location of a Tile.
	 * @param tile Tile to minimap
	 * @return Point.
	 */
	public static Point tileToMinimap(Tile tile) {
		int x = tile.getX() - Game.getMapBaseX();
		int y = tile.getY() - Game.getMapBaseY();
		int mmX = x * 4 + 2 - (Players.getMyPlayer().rawOffsetX()) / 32;
		int mmY = y * 4 + 2 - (Players.getMyPlayer().rawOffsetY()) / 32;
		return worldToMinimap(mmX, mmY);
	}

	/**
	 * Is tile on the minimap
	 * @param t Tile
	 * @return boolean
	 */
	public static boolean isTileOnMap(Tile t) {
		return minimapDimensions.contains(tileToMinimap(t));
	}

	/**
	 * Gets a Point
	 * @param x
	 * @param y
	 * @return Point to minimap
	 */
	public static Point worldToMinimap(int x, int y) {
		int i = (Game.getMinimapRotation()) + (Game.getViewRotation()) & 0x7FF;
		int k = SIN[i];
		int m = COS[i];
		k = k * 256 / (Game.getMinimapZoom() + 256);
		m = m * 256 / (Game.getMinimapZoom() + 256);
		int n = y * k + x * m >> 16;
		int i1 = y * m - x * k >> 16;
		return new Point(643 + n, 84 - i1);
	}

	/**
	 * Gets Point of tile position
	 * @param plane Plane
	 * @param x X location
	 * @param y Y location
	 * @return Point of location
	 */
	public static final Point translateTilePosition(int plane, int x, int y) {
		if (x < 128 || y < 128 || x > 13056 || y > 13056) {
			return new Point(-1, -1);
		}

		int z = get_tile_height(Game.getPlane(), x, y) - plane;
		x -= Game.getCameraX();
		y -= Game.getCameraY();
		z -= Game.getCameraZ();

		int pitch_sin = SIN[Game.getPitch()];
		int pitch_cos = COS[Game.getPitch()];
		int yaw_sin = SIN[Game.getYaw()];
		int yaw_cos = COS[Game.getYaw()];

		int _angle = y * yaw_sin + x * yaw_cos >> 16;

		y = y * yaw_cos - x * yaw_sin >> 16;
		x = _angle;
		_angle = z * pitch_cos - y * pitch_sin >> 16;
		y = z * pitch_sin + y * pitch_cos >> 16;
		z = _angle;

		if (y >= 50) {
			return new Point(256 + (x << 9) / y, (_angle << 9) / y + 167);
		}

		return new Point(-1, -1);
	}

	/**
	 * Gets the Tile height as an offset (Internal use only)
	 * @param plane Initial plane
	 * @param x X location
	 * @param y Y location
	 * @return int of the offset
	 */
	public static final int get_tile_height(int plane, int x, int y) {
		int xx = x >> 7;
			int yy = y >> 7;
			if (xx < 0 || yy < 0 || xx > 103 || yy > 103) {
				return 0;
			}

			int plane2 = plane;
			if (plane2 < 3 && (client.getGroundByteArray()[plane][xx][yy] & 0x2) == 2) {
				plane2++;
			}

			int aa = client.getGroundIntArray()[plane2][xx][yy] * (128 - (x & 0x7F)) + client.getGroundIntArray()[plane2][xx + 1][yy] * (x & 0x7F) >> 7;
			int ab = client.getGroundIntArray()[plane2][xx][yy + 1] * (128 - (x & 0x7F)) + client.getGroundIntArray()[plane2][xx + 1][yy + 1] * (x & 0x7F) >> 7;
			return aa * (128 - (y & 0x7F)) + ab * (y & 0x7F) >> 7;
	}

	/**
	 * Point of a tile on the screen
	 * @param tile the Tile object
	 * @param dX X offset
	 * @param dY Y offset
	 * @param height Plane
	 * @return Point
	 */
	public static Point tileToScreen(Tile tile, double dX, double dY, int height) {
		return groundToScreen((int) ((tile.getX() - (Game.getMapBaseX()) + dX) * 128), (int) ((tile.getY() - (Game.getMapBaseY()) + dY) * 128), height);
	}

	/**
	 * Distance between two tiles given as double
	 * @param a Tile a
	 * @param b Tile b
	 * @return double distance
	 */
	public static double distanceBetween(Tile a, Tile b) {
		int x = b.getX() - a.getX();
		int y = b.getY() - a.getY();
		return Math.sqrt((x * x) + (y * y));
	}

	/**
	 * Random int between min and max
	 * @param min
	 * @param max
	 * @return in between
	 */
	public static int random(int min, int max) {
		return min + (max == min ? 0 : random.nextInt(max - min));
	}

	/**
	 * Another random, with calculated mean
	 * @param min
	 * @param max
	 * @param sd
	 * @return int
	 */
	public static int random(int min, int max, int sd) {
		int mean = min + (max - min) / 2;
		int rand;
		do {
			rand = (int) (random.nextGaussian() * sd + mean);
		} while (rand < min || rand >= max);
		return rand;
	}

	/**
	 * Another random with mean
	 * @param min
	 * @param max
	 * @param mean
	 * @param sd
	 * @return int
	 */
	public static int random(int min, int max, int mean, int sd) {
		int rand;
		do {
			rand = (int) (random.nextGaussian() * sd + mean);
		} while (rand < min || rand >= max);
		return rand;
	}

	/**
	 * random with a double return
	 * @param min double
	 * @param max double
	 * @return double
	 */
	public static double random(double min, double max) {
		return min + random.nextDouble() * (max - min);
	}

	/**
	 * Sleep
	 * @param toSleep int
	 */
	public static void sleep(int toSleep) {
		try {
			long start = System.currentTimeMillis();
			Thread.sleep(toSleep);
			long now;
			while (start + toSleep > (now = System.currentTimeMillis())) {
				Thread.sleep(start + toSleep - now);
			}
		} catch (Exception e) {
		}
	}

}
