package com.aribot.bot.api.methods;

import java.util.HashMap;
import java.util.Map;

import com.aribot.bot.Downloader;
import com.aribot.bot.api.methods.input.Keyboard;
import com.aribot.bot.api.methods.input.Mouse;

/**
 * For this whole class, <b><u>if you do not trust any methods here, then do NOT use them.</u></b><br>
 * It is that simple and avoids false accusations on both the scripter and AriBot.
 */
public class Login {
    
    public static boolean inWorldSelect = false;
    public static final int LOGIN_SCREEN_INDEX = 10;
    public static final int LOGGED_IN_INDEX = 30;
    public static final int PROCESSING_LOGIN_INDEX = 20;
    public static final int PROCESSING_CLICK_TO_PLAY_INDEX = 25;
    
    public enum Worlds {
	
	// Thank GOD for a CSV http://oldschool.runescape.com/slu
	// Exclude worlds that can become "full"
	/*
	 * When you click on a full world, the screen does not change and it acts as if you never clicked.
	 * Since we can't detect where we are in the login screen or the world selection page
	 * it is risky to allow a 'click' on the full world and break the other login stuff
	 * 
	 * All because we don't know where we are.
	 * 
	 * 330 might be added to this exclude list later.
	 * 
	 * Note:
	 * Color change happens when hovering over a usable, non-full world.
	 * Color is again, an option for this.
	 * 
	 */
	//W301("United Kingdom", true,"Trade - Varrock"),
	//W302("United Kingdom", true,"Trade - Falador"),
	W303("Germany", true, "-"),
	W304("Germany", true, "Trouble Brewing"),
	W305("United States", true, "Falador Party Room"),
	W306("United States", true, "Barbarian Assault"),
	W308("United States", false, "Wilderness PK - free style"),
	W309("United Kingdom", true, "-"),
	W310("United Kingdom", true, "-"),
	W311("Germany", true, "-"),
	W312("Germany", true, "-"),
	W313("United States", true, "-"),
	W314("United States", true, "Dorgesh-Kaan Agility"),
	W316("United States", false, "Wilderness PK - free style"),
	W317("United Kingdom", true, "-"),
	W318("United Kingdom", true, "Wilderness PK - members"),
	W319("Germany", true, "-"),
	W320("Germany", true, "-"),
	W321("United States", true, "-"),
	W322("United States", true, "Duel Arena"),
	W325("United Kingdom", true, "-"),
	W326("United Kingdom", true, "-"),
	W327("Germany", true, "-"),
	W328("Germany", true, "-"),
	W329("United States", true, "-"),
	W330("United States", true, "House Party, Gilded Altar"),
	W333("United Kingdom", true, "Games Room, Rogues' Den"),
	W334("United Kingdom", true, "Castle Wars 1"),
	W335("Germany", true, "-"),
	W336("Germany", true, "Running - nature rune"),
	W337("United States", true, "-"),
	W338("United States", true, "-"),
	W341("United Kingdom", true, "Running - law rune"),
	W342("United Kingdom", true, "Role-playing"),
	W343("Germany", true, "-"),
	W344("Germany", true, "Pest Control"),
	W345("United States", true, "-"),
	W346("United States", true, "Agility training"),
	W349("United Kingdom", true, "-"),
	W350("United Kingdom", true, "TzHaar Fight Pit"),
	W351("Germany", true, "-"),
	W352("Germany", true, "-"),
	W353("United States", true, "-"),
	W354("United States", true, "Castle Wars 2"),
	W357("United Kingdom", true, "-"),
	W358("United Kingdom", true, "Blast Furnace"),
	W359("Germany", true, "-"),
	W360("Germany", true, "-"),
	W361("United States", true, "-"),
	W362("United States", true, "Pyramid Plunder"),
	W365("United Kingdom", true, "-"),
	W366("United Kingdom", true, "-"),
	W367("Germany", true, "-"),
	W368("Germany", true, "-"),
	W369("United States", true, "Wilderness PK - members"),
	W370("United States", true, "Fishing Trawler"),
	W373("United Kingdom", true, "-"),
	W374("United Kingdom", true, "-"),
	W375("Germany", true, "Barbarian Fishing"),
	W376("Germany", true, "-"),
	W377("United States", true, "Mort'ton temple, Rat Pits"),
	W378("United States", true, "-");
	
	String nationality;
	boolean members;
	String purpose;
	
	private static Map<String, Worlds> worldNameMap; // Has to be filled before use.
	private static Map<String, int[]> worldNameCoordsMap; // Has to be filled before use. Column and row [0] and [1]
	private static Map<String, Worlds[]> worldNamePurposeMap; // Has to be filled before use.
	private static Map<Boolean, Worlds[]> worldNameMemberMap; // Has to be filled before use.
	
	private static void initialSort() { // world number order
	    worldNameMap = new HashMap<>();
	    worldNameCoordsMap = new HashMap<>();
	    worldNamePurposeMap = new HashMap<>();
	    worldNameMemberMap = new HashMap<>();
	    int column = 0;
	    int row = 2; // This is due to the 2 ignored worlds (1 and 2) that are almost always full. As stated in a previous comment, it can cause issues if we try to select a full world.
	    for (Worlds w : values()) {
		worldNameMap.put(w.toString(), w);
		worldNameCoordsMap.put(w.toString(), new int[] { column, row });
		
		if (worldNamePurposeMap.get(w.purpose) == null) {
		    worldNamePurposeMap.put(w.purpose, new Worlds[] { w });
		} else {
		    Worlds[] temp = worldNamePurposeMap.get(w.purpose);
		    Worlds[] temp2 = new Worlds[temp.length + 1];
		    System.arraycopy(temp, 0, temp2, 0, temp.length);
		    System.arraycopy(new Worlds[] { w }, 0, temp2, temp.length, 1);
		    worldNamePurposeMap.put(w.purpose, temp2);  
		}
		
		if (worldNameMemberMap.get(w.members) == null) {
		    worldNameMemberMap.put(w.members, new Worlds[] { w });
		} else {
		    Worlds[] temp = worldNameMemberMap.get(w.members);
		    Worlds[] temp2 = new Worlds[temp.length + 1];
		    System.arraycopy(temp, 0, temp2, 0, temp.length);
		    System.arraycopy(new Worlds[] { w }, 0, temp2, temp.length, 1);
		    worldNameMemberMap.put(w.members, temp2);  
		}
		
		if (++row >= 16) {
		    row = 0;
		    column++;
		}
	    }
	}
	
	/**
	 * Gets worlds by the specified member status
	 * @param member boolean
	 * @return World object array
	 */
	public static Worlds[] getWorldsByMember(boolean member) {
	    if (worldNameMap == null) {
		initialSort();
	    }
	    return worldNameMemberMap.get(member);
	}
	
	/**
	 * Gets worlds by the specified purpose
	 * @param purpose String "Wilderness Pk - Member style"
	 * @return World object array
	 */
	public static Worlds[] getWorldsByPurpose(String purpose) {
	    if (worldNameMap == null) {
		initialSort();
	    }
	    return worldNamePurposeMap.get(purpose);
	}
	
	/**
	 * Gets a World by the number
	 * @param number String in the form of "W3XX"
	 * @return World object
	 */
	public static Worlds getWorld(String number) {
	    if (worldNameMap == null) {
		initialSort();
	    }
	    return worldNameMap.get(number);
	}
	
	private Worlds(String nationality, boolean members, String purpose) {
	    this.nationality = nationality;
	    this.members = members;
	    this.purpose = purpose;
	}
	
	/**
	 * Get the nationality of the server
	 * @return String
	 */
	public String getNationality() {
	    return nationality;
	}
	
	/**
	 * Is this a membbers world
	 * @return boolean
	 */
	public boolean isMembers() {
	    return members;
	}
	
	/**
	 * The Jagex given World purpose
	 * @return String "Wilderness Pk - Member style"
	 */
	public String getPurpose() {
	    return purpose;
	}
	
	/**
	 * Gets the coordinates of the world on our virtual grid.
	 * @return int[] (Column, Row) [0] and [1] respectively.
	 */
	public int[] getCoord() {
	    if (worldNameMap == null) {
		initialSort();
	    }
	    return worldNameCoordsMap.get(this.toString());
	}
	
	/**
	 * Clicks the world<br>
	 * * Being in the world selection screen is determined by using the clickWorldSelect() method
	 * @return boolean False if not in world selection screen*
	 */
	public boolean click() {
	    if (!inWorldSelect) {
		if (!clickWorldSelect()) {
		    return false;
		}
	    }
	    sortWorlds();  // doesn't need checking as it is done above
	    int[] coords = getCoord();
	    // 3, 13
	    // 475, 387
	    Mouse.click((205 + (coords[0] * 80) + (coords[0] * 14)) + Calculations.random(0, 80), (75 + (coords[1] * 15) + (coords[1] * 9)) + Calculations.random(0, 15), true);
	    inWorldSelect = false;
	    return true;
	}
	
    }
    
    /**
     * Selects a world by the player amount<br>
     * <b><u>WILL NOT SELECT THE FIRST TWO WORLDS!</u></b><br>
     * This is due to possible full worlds and the two free worlds<br>
     * * Being in the world selection screen is determined by using the clickWorldSelect() method
     * @param max boolean Select the world with the most amount of players
     * @return boolean False if not at login screen or not in world selection*
     */
    public static boolean selectPlayerAmount(boolean max) {
	if (isLoginScreen() && inWorldSelect) {
	    sortPlayers(max);
	    Mouse.click((205 + (0 * 80) + (0 * 14)) + Calculations.random(0, 80), (75 + (2 * 15) + (2 * 9)) + Calculations.random(0, 15), true); // 0, 2
	    inWorldSelect = false;
	    return true;
	}
	return false;
    }
    
    /**
     * Clicks the sort by world option on the world selection<br>
     * * Being in the world selection screen is determined by using the clickWorldSelect() method
     * @return boolean False if not in login screen or not in world selection* 
     */
    public static boolean sortWorlds() {
	if (isLoginScreen() && inWorldSelect) {
	    Mouse.click(300, 10, true);
	    return true;
	}
	return false;
    }
    
    /**
     * Clicks the sort by amount of players option on the world selection<br>
     * * Being in the world selection screen is determined by using the clickWorldSelect() method
     * @param down Sort the list going down (max players to min players)
     * @return boolean False if not in login screen or not in world selection* 
     */
    public static boolean sortPlayers(boolean down) {
	if (isLoginScreen() && inWorldSelect) {
	    if (down) {
		Mouse.click(396, 10, true);
	    } else {
		Mouse.click(410, 10, true);
	    }
	    return true;
	}
	return false;
    }
    
    /**
     * Selects a random world
     * @return boolean if selected
     */
    public static boolean selectRandomWorld() {
	if (!isLoginScreen()) {
	    return false;
	}
	clickWorldSelect();
	return Login.Worlds.getWorld("W3" + String.format("%02d", Downloader.getValidWorld())).click();
    }
    
    /**
     * Clicks the World selection button
     * @return boolean False if not in login screen
     */
    public static boolean clickWorldSelect() {
	if (isLoginScreen()) {
	    // world button: 7, 466 - 102, 494
	    Mouse.click(7 + Calculations.random(0, 95), 466 + Calculations.random(0, 28), true);
	    inWorldSelect = true;
	    return true;
	}
	return false;
    }
    
    /**
     * Toggles sound on the login menu.
     * @return boolean
     */
    public static boolean toggleSound() {
	if (isLoginScreen()) {
	    // sound: 727, 464 - 760, 497
	    Mouse.click(727 + Calculations.random(0, 33), 464 + Calculations.random(0, 33), true);
	    return true;
	}
	return false;
    }
    
    /**
     * Logs into RuneScape with given credentials and world. <b><u>IF YOU DO NOT TRUST, DO NOT USE!</u></b>
     * @param username String
     * @param password String
     * @param world int 301, 302... 378
     * @return boolean on success
     */
    public static boolean login(String username, String password, int world) {
	if (!isLoginScreen()) {
	    return false;
	}
	clickWorldSelect();
	Login.Worlds.getWorld("W" + world).click();
	return login(username, password);
    }
    
    /**
     * Logs into RuneScape with specified credentials on a predetermined world. <b><u>IF YOU DO NOT TRUST, DO NOT USE!</u></b>
     * @param username String
     * @param password String
     * @return boolean on success
     */
    public static boolean login(String username, String password) {
	
	if (isLoginScreen()) {
	    // existing user: 393, 273 - 530, 308
	    Mouse.click(393 + Calculations.random(0, 137), 273 + Calculations.random(0, 35), true);
	    Calculations.sleep(1000);
	    Keyboard.sendText(username, true);
	    Calculations.sleep(1000);
	    Keyboard.sendText(password, true);
	    while (isLoginScreen()) {
		Calculations.sleep(100);
	    }
	    while (isProcessingLoginScreen()) {
		Calculations.sleep(500);
	    }
	    int count = 0;
	    while (isLoginScreen()) {
		Calculations.sleep(1000);
		if (++count > 3) { // Timeout (invalid creds or similar)
		    return false;
		}
	    }
	    while (isProcessingLoginScreen() || isProcessingClickToPlayScreen()) {
		Calculations.sleep(500);
	    }
	    if (isLoggedIn()) {
		// click here to play: 275, 294 - 495, 379
		Mouse.click(275 + Calculations.random(0, 220), 294 + Calculations.random(0, 85), true);
		return true;
	    }
	    return false;
	}
	
	return true;
    }
    

    /**
     * Logs you out of the game to the login screen
     * @return boolean if successful.
     */
    public static boolean logOut() {
	if (isLoggedIn()) {
	    Tabs.Tab logOut = Tabs.Tab.LOGOUT;
	    if (!Tabs.openTab(logOut)) {
		Widgets.getInterface(logOut.getParent(), 6).click();
		Calculations.sleep(2000);
		return isLoginScreen();
	    }
	}
	return false;
    }
    
    /**
     * Are we logged in?
     * @return boolean
     */
    public static boolean isLoggedIn() {
	return Game.getLoginIndex() == LOGGED_IN_INDEX;
    }

    /**
     * Are we on the login screen?
     * @return boolean
     */
    public static boolean isLoginScreen() {
	return Game.getLoginIndex() == LOGIN_SCREEN_INDEX;
    }

    /**
     * Are we in the process of logging in?
     * @return boolean
     */
    public static boolean isProcessingLoginScreen() {
	return Game.getLoginIndex() == PROCESSING_LOGIN_INDEX;
    }
    
    /**
     * Are we logged in successfully and processing the Click to Play screen?
     * @return boolean
     */
    public static boolean isProcessingClickToPlayScreen() {
	return Game.getLoginIndex() == PROCESSING_CLICK_TO_PLAY_INDEX;
    }
    
}
