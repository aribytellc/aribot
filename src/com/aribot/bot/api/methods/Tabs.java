package com.aribot.bot.api.methods;

public class Tabs {

    public static final int PARENT = 548;

    public enum Tab {

	CLAN_CHAT(30, 0),
	FRIEND_LIST(31, 0),
	IGNORE_LIST(32, 0),
	LOGOUT(33, 182),
	OPTIONS(34, 261), // 0 set run
	EMOTES(35, 464),
	MUSIC(36, 239),
	COMBAT_OPTIONS(47, 92),
	MAGIC_COMBAT_OPTIONS(47, 90),
	STATS(48, 320),
	QUESTS(49, 274),
	INVENTORY(50, 149),
	WORN_EQUIPMENT(51, 387),
	PRAYER(52, 271),
	MAGIC(53, 192);

	private int tab;
	private int parent;

	private Tab(int t, int p) {
	    this.tab = t;
	    this.parent = p;
	}

	/**
	 * Gets the tab number. (For the actual button)
	 * @return int
	 */
	public int getTab() {
	    return tab;
	}

	/**
	 * Gets the parent ID of the interface. (Used for the actual components in the tab)
	 * @return int
	 */
	public int getParent() {
	    return parent;
	}

	/**
	 * Is the tab open
	 * @return boolean
	 */
	public boolean isOpen() {
	    return Tabs.isTabOpen(this);
	}
	
	/**
	 * Open this tab
	 * @return boolean true on success
	 */
	public boolean open() {
	    return Tabs.openTab(this);
	}

    }

    /**
     * Is a tab open
     * @param tab Tab int
     * @return boolean
     */
    public static boolean isTabOpen(Tab tab) {
	return Widgets.getInterface(PARENT, tab.getTab()).getTextureID() != -1;
    }

    /**
     * Gets the currently open tab. (Not tested)
     * @return Tab enum entry
     */
    public static Tab getOpenTab() {
	for (Tab ta : Tab.values()) {
	    if (ta.isOpen()) {
		return ta;
	    }
	}
	return null;
    }

    /**
     * Opens a tab
     * @param tab Tab int
     * @return boolean did tab open
     */
    public static boolean openTab(Tab tab) {
	if (isTabOpen(tab)) {
	    return true;
	}
	Widgets.getInterface(PARENT, tab.getTab()).click();
	if (isTabOpen(tab)) {
	    return true;
	}
	return false;
    }

}
