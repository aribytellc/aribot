package com.aribot.bot.api.methods;

import com.aribot.bot.events.LoopTask;

public abstract class Script extends LoopTask {

    /**
     * Executes when the scripts is started.
     * @return Boolean, True to continue execution to the main scripts, false to stop script and end script before starting the main script.
     */
    protected abstract boolean onStart();

    /**
     * Executes when a script is stopped either by the loop ending or stopping the script via stop button.
     */
    protected abstract void onFinish();

    private void scriptStop() {
	scriptStop("Script stopped.");
    }

    private void scriptStop(String message) {
	System.out.println(message);
    }

    @Override
    public void run() {
	boolean start = false;
	setRandoms();
	try {
	    start = onStart();
	} catch (Exception e) {
	    System.out.println("Error starting script: " + e.getMessage());
	}
	if (start) {
	    setRunning(true);
	    register();
	    System.out.println("Script started.");
	    try {
		while (isRunning()) {
		    if (!runOnce()) {
			break;
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	    onFinish();
	} else {
	    System.out.println("Could not start. onExecute() returned false.");
	}
	scriptStop();
	stop();
	unregister();
    }

    /**
     * Sleeps execution for specified number of milliseconds
     * @param millis int
     */
    public void sleep(int millis) {
	try {
	    Thread.sleep(millis);
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    /**
     * Random generator
     * @param min int
     * @param max int
     * @return int Random number between min and max
     */
    public int random(int min, int max) {
	try {
	    return min + (max == min ? 0 : Calculations.random.nextInt(max - min));
	} catch (Exception e) {
	}
	return min + (max - min);
    }

}