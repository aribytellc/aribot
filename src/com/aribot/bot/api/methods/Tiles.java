package com.aribot.bot.api.methods;

import java.util.LinkedList;

import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.wrappers.Tile;

/**
 * Please note that Tiles that are near the edge of the MapBase might behave differently such as Tile flags reporting things that are not true.
 * This can be corrected by loading a new MapBase containing the Tiles in question.
 */
public class Tiles {
    
    /**
     * Gets the loaded Tiles based on a filter
     * @param filter Filter (can be null)
     * @return Tile array
     */
    public static Tile[] getLoaded(final Filter<Tile> filter) {
	LinkedList<Tile> list = new LinkedList<>();
		for (int x = 0; x < 104; x++) {
			for (int y = 0; y < 104; y++) {
		Tile t = new Tile(Game.getMapBaseX() + x, Game.getMapBaseY() + y, Game.getPlane());
		if (filter == null || filter.apply(t)) {
		    list.add(t);
		}
	    }
	}
	return list.toArray(new Tile[list.size()]);
    }
    
	/**
	 * Gets an array of the loaded Tiles
	 * @return Tile array
	 */
    public static Tile[] getLoaded() {
	return getLoaded(null);
    }
    
}
