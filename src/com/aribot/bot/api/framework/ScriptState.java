package com.aribot.bot.api.framework;

import com.aribot.bot.api.framework.condition.CriteriaSet;


public abstract class ScriptState extends CriteriaSet implements Runnable {
	
	public abstract boolean pass();
	
}
