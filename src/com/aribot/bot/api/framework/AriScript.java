package com.aribot.bot.api.framework;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class AriScript {

	private Set<ScriptState> states = Collections.synchronizedSet(new LinkedHashSet<ScriptState>());
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	int delay = 500;

	public AriScript() {
		Controller.getInstance(this).setScript(this);
	}

	/**
	 * 
	 * @return An instance of ScriptSetup to be executed upon #setup being called.
	 */
	protected abstract boolean setup();
	
	/**
	 * A block of code to be executed upon a script being stopped.
	 */
	protected abstract void onStop();

	public ScriptState getNext() {
		try {
			for (final ScriptState state : getStates()) {
				if (state != null && state.pass()) {
					return state;
				}
			}
			return null;
		} catch (Exception ignored) {
			return null;
		}
	}

	public final boolean add(final ScriptState state) {
		synchronized(this.states) {
			return states.add(state);
		}
	}

	public final boolean revoke(final ScriptState state) {
		synchronized(this.states) {
			return states.remove(state);
		}
	}

	public void shutdown() {
		Controller.getInstance(this).shutdown();
	}

	public void setPaused(final boolean paused) {
		Controller.getInstance(this).setPaused(paused);
	}

	public ExecutorService getExecutor() {
		return this.executor;
	}
	
	public void setDelay(final int delay) {
		this.delay = delay;
	}

	public final ScriptState[] getStates() {
		return states.toArray(new ScriptState[states.size()]);
	}

}
