package com.aribot.bot.api.framework.condition;


public abstract class Criteria {

	public abstract boolean isMet();	

	public static boolean waitFor(final Criteria c, final long timeout) {
		final long finishTime = System.currentTimeMillis() + timeout;
		while (System.currentTimeMillis() < finishTime) {
			if (c.isMet()) {
				return true;
			}
		}
		return false;
	}

	public static final Criteria TRUE = new Criteria() {
		@Override
		public boolean isMet() {
			return true;
		}
	};

	public static final Criteria FALSE = new Criteria() {
		@Override
		public boolean isMet() {
			return false;
		}
	};

}
