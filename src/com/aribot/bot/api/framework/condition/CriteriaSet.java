package com.aribot.bot.api.framework.condition;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class CriteriaSet {

	public enum CriteriaStrength {
		ALL, ANY
	}

	private LinkedHashSet<Criteria> container = new LinkedHashSet<>();

	public CriteriaSet(final Criteria...criterias){
		container.addAll(Arrays.asList(criterias));
	}

	public boolean addCriteria(final Criteria c) {
		return this.container.add(c);
	}

	public boolean revokeCriteria(final Criteria c) {
		return this.container.remove(c);
	}

	public boolean passes(final CriteriaStrength strength) {
		switch(strength) {
		case ALL:
			for (final Criteria c : this.container) {
				if (!c.isMet()) {
					return false;
				}
			}
			return true;
		case ANY:
			for (final Criteria c : this.container) {
				if (c.isMet()) {
					return true;
				}
			}
			return false;
		default:
			return false;
		}
	}

}
