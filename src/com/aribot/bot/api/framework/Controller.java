package com.aribot.bot.api.framework;

import com.aribot.bot.api.util.Time;

public class Controller {

	private static Controller instance;

	private AriScript script;
	private State state = State.TERMINATED;

	private Thread controlThread = new Thread() {
		public void run() {
			while (true) {
				try {
					synchronized(Controller.class) {
						switch(state) {
						case RUNNING:
							ScriptState next = Controller.this.script.getNext();
							if (next != null) {
								next.run();
							}
							break;
						case PAUSED:
							Controller.class.wait();
							break;
						case TERMINATED:

							break;
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					Time.sleep(Controller.this.script.delay);
				}
			}
		}
	};

	public void setScript(final AriScript script) {
		this.script = script;
	}

	public AriScript getActionScript() {
		return script;
	}

	private void setState(final State state) {
		this.state = state;
	}

	public void setPaused(final boolean paused) {
		synchronized(Controller.class) {
			setState(paused ? State.PAUSED : State.RUNNING);
			if (!paused) {
				try {
					Controller.class.notifyAll();
				} catch (Exception ignored) {
				}
			}
		}
	}

	public void start() {
		if (state != State.TERMINATED) {
			throw new RuntimeException("A script is already running!");
		}

	}

	public void shutdown() {
		synchronized(Controller.class) {
			setState(State.TERMINATED);
		}
	}

	/**
	 * @param scriptInstance An instance of ActionScript to get the controller for.
	 * @return The Controller instance that is managing the given ActionScript. 
	 * <tt>null</tt> if the currently set Controller's script is not the given ActionScript.
	 */
	public static Controller getInstance(final AriScript scriptInstance) {
		if (instance.script == scriptInstance) {
			return instance;
		}
		return null;
	}

	public static AriScript getLastScriptInstance() {
		if (instance == null) {
			return null;
		}
		return instance.script;
	}

	public enum State { 
		RUNNING,
		PAUSED,
		TERMINATED
	}

}
