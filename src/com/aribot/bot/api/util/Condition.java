package com.aribot.bot.api.util;

public interface Condition {
    
    public boolean meet();
    
}
