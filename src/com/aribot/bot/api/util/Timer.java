package com.aribot.bot.api.util;

public class Timer {

    private long start;
    private long end;
    
    /**
     * Create a Timer with a specified duration
     * @param duration long in milliseconds
     */
    public Timer(final long duration) {
	start = System.currentTimeMillis();
	end = start + duration;
    }
    
    /**
     * Time since initial start or last reset
     * @return long
     */
    public long getElapsed() {
	return System.currentTimeMillis() - start;
    }
    
    /**
     * Time that is remaining in the timer.
     * @return long Remaining. -1 if timer has expired
     */
    public long getRemaining() {
	return ((isRunning()) ? (end - System.currentTimeMillis()) : -1);
    }
    
    /**
     * Is the timer currently running
     * @return boolean
     */
    public boolean isRunning() {
	return System.currentTimeMillis() < end;
    }
    
    /**
     * Reset the timer so a new cycle can begin.
     */
    public void reset() {
	start = System.currentTimeMillis();
    }
    
}
