package com.aribot.bot.api.util;

public class Time {
    
    private static StringBuilder sb = new StringBuilder();
    
    /**
     * Sleeps the calling thread for the given time in milliseconds.
     * @param millis
     */
    public static void sleep(final int millis) {
        if (millis < 0)
            throw new IllegalArgumentException("Cannot sleep for a negative time period");
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ignored) {
        }
    }
    
    /**
     * Sleeps for a random interval.
     * @param min int minimum amount to sleep
     * @param max int maximum amount to sleep
     */
    public static void sleep(final int min, final int max) {
        sleep(Random.nextInt(min, max));
    }
    
    /**
     * Sleeps until the given Condition is met, or until the timeout has expired.
     * 
     * @param con A Condition with the #meet method overridden.
     * @param timeout A timeout in milliseconds to break the loop.
     * @return true if the condition returns true. false if the timeout expires.
     */
    public static boolean sleepUntil(final Condition con, final int timeout) {
        return sleepUntil(con, new Condition() { 
            public boolean meet() { 
                return false; 
            }
        }, timeout);
    }
    
    /**
     * Sleeps until the given Condition is met. If the reset Condition is met, the timeout is extended to its original length.
     * This reset can occur multiple times, it is up to the user to ensure that the reset Condition does not cause a infinite loop.
     * 
     * @param con A Condition with the #meet method overridden. Signifies the return state of the sleep.
     * @param reset A Condition with the #meet method overridden. Signifies the reset state of the timeout
     * @param timeout A timeout in milliseconds to break the loop.
     * @return true if the condition returns true. false if the timeout expires.
     */
    public static boolean sleepUntil(final Condition con, final Condition reset, final int timeout) {
        long end = System.currentTimeMillis() + timeout;
        while (System.currentTimeMillis() < end) {
            if (con.meet()) {
                return true;
            }
            if (reset.meet()) {
                end = System.currentTimeMillis() + timeout;
            }
            sleep(300);
        }
        return false;
    }
    
    /**
     * Formats a long to a String
     * @param millis long
     * @return String in the form "1h:2m:3s"
     */
    public static String format(final long millis) {
        sb.delete(0, sb.length());
        final long sum_seconds = millis / 1000;
        final long sum_minutes = sum_seconds / 60;
        final long sum_hours = sum_minutes / 60;
        
        final long actual_seconds = sum_seconds % 60;
        final long actual_minutes = sum_minutes % 60;
        if (sum_hours > 0) {
            sb.append(sum_hours).append("h").append(":");
        }
        if (actual_minutes > 0) {
            sb.append(actual_minutes).append("m").append(":");
        }
        sb.append(actual_seconds).append("s");
        sb.trimToSize();
        return sb.toString();
    }
    
}