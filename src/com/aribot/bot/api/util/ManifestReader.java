package com.aribot.bot.api.util;

import java.util.HashMap;

public class ManifestReader {

    public static HashMap<String, String> getManiDef(String clas) {
	HashMap<String, String> def = new HashMap<>();
	try {
	    Class<?> posScript = Class.forName(clas);
	    def = getManiDef(posScript, def);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return def;
    }

    public static HashMap<String, String> getManiDef(Class<?> clas) {
	HashMap<String, String> def = new HashMap<>();
	try {
	    def = getManiDef(clas, def);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return def;
    }

    public static HashMap<String, String> getManiDef(Class<?> clas, HashMap<String, String> def) {
	if (clas.isAnnotationPresent(Manifest.class)) {
	    final Manifest manifest = clas.getAnnotation(Manifest.class);
	    def.put("0", manifest.name());
	    def.put("1", manifest.authors());
	    def.put("2", Double.toString(manifest.version()));
	    def.put("3", manifest.description());
	    def.put("4", "false"); // Force for non-integrated scripts
	} else {
	    System.out.println("Something went very wrong or script " + clas.getName() + " does not have a Manifest.");
	}
	return def;
    }

}
