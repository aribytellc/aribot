package com.aribot.bot.api.util;

public interface Filter<T> {
    /**
     * What to allow through the Filter
     * @param t 
     * @return boolean
     */
    public boolean apply(T t);
    
}
