package com.aribot.bot.api.util;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

// Used http://javapapers.com/core-java/java-annotations/ for a lot of help with this.
@Retention(RetentionPolicy.RUNTIME)
public @interface Manifest {

	String name();
	
	String authors();
	
	double version() default 1.00;
	
	String description() default "[No description]";
	
}
