package com.aribot.bot.api.util;


public class Random {

    private static java.util.Random random = new java.util.Random();

    /**
     * randomInt(0, 100) can yield all values 0-99
     * @return A random value between min(inclusive) and max(exclusive)
     */
    public static int nextInt(final int min, final int max) {
	return random.nextInt(Math.abs(max-min)) + min;
    }
    
    /**
     * randomDouble(1.0, 5.0) can yield all values between 0-4.99~
     * @return A random value between min(inclusive) and max(exclusive)
     */
    public static double randomDouble(final double min, final double max) {
	return (random.nextDouble() * (Math.abs(max - min))) + min;
    }
    
    /**
     * Gets a Random object
     * @return java.util.Random object
     */
    public static java.util.Random getRandom() {
	return random;
    }

}
