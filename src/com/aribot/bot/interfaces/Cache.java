package com.aribot.bot.interfaces;

public interface Cache {

	Queue getQueue();
	
	int getSize();
	
	int getRemainingSpace();
	
	NodeSub getNodeSub();
	
	HashTable getHashTable();
	
	NodeSub lookup(long i);
	
}
