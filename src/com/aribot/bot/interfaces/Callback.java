package com.aribot.bot.interfaces;

public interface Callback {

	public void notifyMessage(int ID, String sender, String message);
	
}
