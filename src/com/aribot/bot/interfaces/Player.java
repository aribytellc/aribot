package com.aribot.bot.interfaces;

public interface Player extends Character {

	String getName();
	
	int getLevel();
	
	int getMultiplierLevel();
	
	Models getModel();
	
	PlayerDefinition getPlayerDefinition();
	
}
