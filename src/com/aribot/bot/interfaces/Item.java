package com.aribot.bot.interfaces;

public interface Item extends Animable {

	int getMultiplierID();
	
	int getID();
	
	int getStackSize();
	
	int getMultiplierStackSize();
	
}
