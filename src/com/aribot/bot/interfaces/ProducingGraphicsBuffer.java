package com.aribot.bot.interfaces;

import java.awt.image.ColorModel;
import java.awt.image.ImageConsumer;

public interface ProducingGraphicsBuffer {
	
	ColorModel getColorModel();
	
	ImageConsumer getImageConsumer();

}
