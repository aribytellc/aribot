package com.aribot.bot.interfaces;

public interface WallObject {
    
    int getMultiplierHash();
    
    int getMultiplierWorldY();
    
    int getMultiplierWorldX();
    
    int getHash();
    
    int getWorldY();
    
    int getWorldX();
    
    Animable getAnimable();
    
}
