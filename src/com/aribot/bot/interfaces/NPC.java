package com.aribot.bot.interfaces;

public interface NPC extends Character {

	NPCDefinition getNPCDefinition();
	
	Models getModel();
	
}
