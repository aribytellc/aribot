package com.aribot.bot.interfaces;

public interface WallDecorationObject {
    
    int getHash();
    
    int getWorldY();
    
    int getWorldX();
    
    int getMultiplierHash();
    
    int getMultiplierWorldY();
    
    int getMultiplierWorldX();
    
    Animable getAnimable();
    
}
