package com.aribot.bot.interfaces;

public interface IntegerNode {

	int getValue();
	
}
