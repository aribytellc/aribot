package com.aribot.bot.interfaces;

public interface GroundItems {

	Node getHead();
	
	Node getCurrent();
	
}
