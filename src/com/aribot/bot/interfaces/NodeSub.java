package com.aribot.bot.interfaces;

public interface NodeSub extends Node {
	
	NodeSub getNext();
	
	NodeSub getPrev();
	
	long getID();

}
