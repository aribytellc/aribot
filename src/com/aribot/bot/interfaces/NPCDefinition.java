package com.aribot.bot.interfaces;

public interface NPCDefinition {

	String getName();

	String[] getMenuOptions();

	int getMultiplierLevel();
	
	int getMultiplierHeadIcon();
	
	int getMultiplierWalkAnimation();
	
	int getMultiplierStandAnimation();
	
	int getLevel();
	
	boolean isClickable();
	
	boolean isVisible();
	
	int getHeadIcon();
	
	int getWalkAnimation();
	
	int getStandAnimation();

	Cache getCache();
	
	int getLID();
	
	int getMultiplierLID();
	
}
