package com.aribot.bot.interfaces;

public interface Character extends Animable {

	int getMultiplierY();
	int getMultiplierX();
	int getY();
	int getX();
	int getAnimation();
	int getMultiplierAnimation();
	int getMultiplierInteractingID();
	int getInteractingID();
	int getMoving();
	int getMultiplierMoving();
	int getHealth();
	int getMaxHealth();
	int getCycle();
	int getMultiplierHealth();
	int getMultiplierMaxHealth();
	int getMultiplierCycle();
	int getRotation();
	int getMultiplierRotation();
	
}
