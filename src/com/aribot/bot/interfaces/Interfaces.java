package com.aribot.bot.interfaces;

public interface Interfaces {

	int getMultiplierX();
	int getMultiplierY();
	int getMultiplierUID();
	int getMultiplierParentID();
	int getMultiplierWidth();
	int getMultiplierHeight();
	int getMultiplierZRotation();
	int getMultiplierXRotation();
	int getX();
	int getY();
	int getUID();
	int getParentID();
	int[] getSlotIDs();
	int getWidth();
	int getHeight();
	String getText();
	int[] getStackSize();
	int getZRotation();
	int getXRotation();
	boolean isHidden();
	Interfaces[][] getInterfaces();
	Interfaces[] getComponents();
	String[] getActions();
	Interfaces getParent();
	boolean isInventory();
	int getScrollX();
	int getScrollY();
	int getMultiplierScrollX();
	int getMultiplierScrollY();
	int getTextureID();
	int getMultiplierTextureID();
	int getBorderThickness();
	int getMultiplierBorderThickness();
	int getModelID();
	int getMultiplierModelID();
	String getToolTip();
	String getName();
	
}
