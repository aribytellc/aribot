package com.aribot.bot.interfaces;

import java.awt.EventQueue;

public interface SignLink {

	EventQueue getEventQueue();
	
}
