package com.aribot.bot.interfaces;

public interface World {
	
	Ground[][][] getGroundArray();
	
	SceneObject[] getSceneObjCache();
	
	FloorDecorationObject getFloorDecorationObjCache();

}
