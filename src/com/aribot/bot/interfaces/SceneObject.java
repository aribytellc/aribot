package com.aribot.bot.interfaces;

public interface SceneObject {

	int getMultiplierWorldX();	
	
	int getMultiplierWorldY();
	
	int getWorldX();
	
	int getWorldY();
	
	int getHash();
	
	int getMultiplierHash();
	
	Animable getAnimable();
	
	int getMultiplierHeight();
	
	int getHeight();
	
}
