package com.aribot.bot.interfaces;

public interface Ground {

	SceneObject[] getSceneObject();
	
	FloorDecorationObject getFloorDecorationObject();
	
	WallObject getWallObject();
	
	WallDecorationObject getWallDecorationObject();
	
}
