package com.aribot.bot.interfaces.io;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public abstract class Keyboard extends Focus implements KeyListener {
	public abstract void _keyPressed(KeyEvent e);

	public abstract void _keyReleased(KeyEvent e);

	public abstract void _keyTyped(KeyEvent e);

	@Override
	public void keyPressed(final KeyEvent e) {
		_keyPressed(e);
	}

	@Override
	public void keyReleased(final KeyEvent e) {
		_keyReleased(e);
	}

	@Override
	public void keyTyped(final KeyEvent e) {
		_keyTyped(e);
	}
}
