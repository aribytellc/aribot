package com.aribot.bot.interfaces.io;

import java.awt.AWTEvent;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.FocusEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.awt.image.DirectColorModel;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.util.Hashtable;

import com.aribot.bot.loader.CustomClassLoader.GameLoader;
import com.aribot.bot.loader.GUI.main;

public class Canvas extends java.awt.Canvas {
    
    private static final long serialVersionUID = -2276037172265300477L;
    
    private boolean visible;
    private boolean focused;
    
    Graphics bufferGraphics;
    Image offscreen;
    private static final BufferedImage gameImage = new BufferedImage(800, 800, BufferedImage.TYPE_INT_ARGB);
    public static final int LEN = 2;
    public static final int[] frameCount = new int[Canvas.LEN];
    public static int lastIdx = 0;
    
    @Override
    public final Graphics getGraphics() {
	try {
	    Thread.sleep(main.selectedGraphicsDelay);
	} catch (final InterruptedException ignored) {
	}
	
	try {
	    Graphics buffer = gameImage.getGraphics();
	    GameLoader.bufferGraphicsDraw(buffer);
	    super.getGraphics().drawImage(gameImage, 0, 0, null);
	    return buffer;
	} catch (Exception e) {
	    return super.getGraphics();
	}
	
    }
    
    public static final BufferedImage getGameImage() {
	return gameImage;
    }
    
    @Override
    public final boolean hasFocus() {
	return focused;
    }
    
    @Override
    public final boolean isValid() {
	return visible;
    }
    
    @Override
    public final boolean isVisible() {
	return visible;
    }
    
    @Override
    public final boolean isDisplayable() {
	return true;
    }
    
    @Override
    public final Dimension getSize() {
	return new Dimension(771, 531);
    }
    
    @Override
    public final void setVisible(final boolean visible) {
	super.setVisible(visible);
	this.visible = visible;
    }
    
    public final void setFocused(final boolean focused) {
	if (focused && !this.focused) {
	    super.processEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED, false, null));
	} else if (this.focused) {
	    super.processEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST, true, null));
	}
	this.focused = focused;
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public Image createImage(final int width, final int height) {
	final int[] pixels = new int[height * width];
	final DataBufferInt databufferint = new DataBufferInt(pixels, pixels.length);
	final DirectColorModel directcolormodel = new DirectColorModel(32, 0xff0000, 0xff00, 255);
	final WritableRaster writableraster = Raster.createWritableRaster(directcolormodel.createCompatibleSampleModel(width, height), databufferint, null);
	return new BufferedImage(directcolormodel, writableraster, false, new Hashtable());
    }
    
    @Override
    protected final void processEvent(final AWTEvent e) {
	if (!(e instanceof FocusEvent)) {
	    super.processEvent(e);
	}
    }
    
}
