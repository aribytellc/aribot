package com.aribot.bot.interfaces;

public interface PlayerDefinition {

    Cache getCache();
    
    int[] getEquipment();
    
    long getMultiplierLID();
    
    long getLID();
    
    int getMultiplierNPCID();
    
    int getNPCID();
    
    boolean isFemale();
    
}
