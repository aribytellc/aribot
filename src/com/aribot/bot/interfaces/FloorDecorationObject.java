package com.aribot.bot.interfaces;

public interface FloorDecorationObject {

	Animable getAnimable();
	
	int getHash();
	int getMultiplierHash();
	int getWorldY();
	int getWorldX();
	int getHeight();
	int getMultiplierWorldY();
	int getMultiplierWorldX();
	int getMultiplierHeight();
	
}
