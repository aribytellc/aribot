package com.aribot.bot.interfaces;

public interface ItemDef extends NodeSub {

	String[] getGroundActions();
	
	String[] getActions();
	
	String getName();
	
	Cache getCache();
	
	int getLID();
	
	int getMultiplierLID();
	
}
