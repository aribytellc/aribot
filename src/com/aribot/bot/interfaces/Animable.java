package com.aribot.bot.interfaces;

public interface Animable extends Interactable {
	
	int getModelHeight();
	int getMultiplierModelHeight();
	
}
