package com.aribot.bot.interfaces;

public interface Models extends Animable {

	 int[] getZVerticies(); 
	 int[] getXVerticies();
	 int[] getYVerticies(); 
	 int[] getXTriangles(); 
	 int[] getYTriangles();
	 int[] getZTriangles(); 
}
