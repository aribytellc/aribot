package com.aribot.bot.interfaces;

public interface Node {

	Node getNext();
	
	Node getPrev();
	
	long getID();
	
}
