package com.aribot.bot.interfaces;

import com.aribot.bot.interfaces.io.Canvas;
import com.aribot.bot.interfaces.io.Keyboard;
import com.aribot.bot.interfaces.io.Mouse;

public interface Client {

	Mouse getMouse();

	Keyboard getKeyboard();

	Canvas getCanvas();

	Player[] getPlayerArray();

	Player getMyPlayer();

	Interfaces[][] getInterfaces();

	int getLoginIndex();

	int getPlayerCount();

	boolean isMenuOpen();

	int getMultiplierLoginIndex();

	int getMultiplierPlayerCount();

	int getMultiplierNPCCount();

	int getMultiplierInterfaceIndex();

	int getMultiplierPlane();

	int getNPCCount();

	int[] getNPCIndexArray();

	int getPlane();

	int getInterfaceIndex();

	//String[] getMenuOptions();

	int getMultiplierMenuX();

	int getMultiplierMenuY();

	int getMultiplierMenuWidth();

	int getMultiplierMenuHeight();

	int getMenuX();

	int getMenuY();

	int getMenuWidth();

	int getMenuHeight();

	boolean isMenuCollapsed();
	
	int getMultiplierMapBaseX();
	
	int getMultiplierMapBaseY();
	
	int getMapBaseX();
	
	int getMapBaseY();
	
	int getMinimapRotation();
	
	int getViewRotation();
	
	int getMinimapZoom();
	
	int getMultiplierMinimapRotation();
	
	int getMultiplierViewRotation();
	
	int getMultiplierMinimapZoom();
	
	String[] getMenuActions();
			
	String[] getMenuOptions();
	
	int getYaw();
	
	int getCameraX();
	
	int getCameraY();
	
	int getPitch();
	
	int getCameraZ();
	
	int getMultiplierYaw();
	
	int getMultiplierCameraX();
	
	int getMultiplierCameraY();
	
	int getMultiplierPitch();
	
	int getMultiplierCameraZ();
	
	World getWorld();
	
	int getEnergy();
	
	int getWeight();
	
	int getMultiplierEnergy();
	
	int getMultiplierWeight();
	
	TileSettings[] getTileSettings();
	
	byte[][][] getGroundByteArray();
	
	int[][][] getGroundIntArray();
	
	int getMenuActionRow();
	
	int getMultiplierMenuActionRow();
	
	Models getModels();
	
	NPC[] getNPCArray();
	
	int[] getCurrentLevels();
	
	int[] getCurrentLevelsMax();
	
	int[] getCurrentExperience();
	
	GroundItems[][][] getGroundItems();
	
	Cache getCache();
	
	int getCycle();
	
	int getMultiplierCycle();
	
	ItemDef getItemDef(int index);
	
	ObjectDefinition getObjDef(int index);
	
	String[] getChatNames();
	
	String[] getChat();
	
	int[] getSettings();
	
	int[] getChatTypes();
	
	Callback getCallback();
	
	void setCallback(Callback c);
	
	int getSelfInteratctingIndex();
	
	int getMultiplierSelfInteratctingIndex();
	
}
