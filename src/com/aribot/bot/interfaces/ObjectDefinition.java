package com.aribot.bot.interfaces;

public interface ObjectDefinition {
	
	String[] getActions();
	
	String getName();

}
