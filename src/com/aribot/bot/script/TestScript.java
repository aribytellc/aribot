package com.aribot.bot.script;

import java.awt.Point;
import java.awt.event.ActionEvent;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.loader.GUI.main;

@Manifest(name = "Test Script", description = "This is a test script and will not do anything useful.", version = 9000.01, authors = "Nobody here")
public class TestScript extends Script {

	@Override
	protected boolean onStart() {
		System.out.println("This does nothing. I guess you can't read.");
		main.debugMenuListener.actionPerformed(new ActionEvent("", 0, "Mouse"));
		return true;
	}

	@Override
	protected void onFinish() {
		System.out.println("You can relax.");
		System.out.println("You must have thought that something went wrong.");
		System.out.println("I said that this script will not do anything useful.");
		main.debugMenuListener.actionPerformed(new ActionEvent("", 0, "Mouse"));
	}

	@Override
	public int loop() {
		Mouse.move(2, new Point(Calculations.random(0, 761), Calculations.random(0, 501)));
		return 50;
	}

}
