package com.aribot.bot.script;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.text.NumberFormat;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import com.aribot.bot.api.internal.wrappers.Message;
import com.aribot.bot.api.internal.wrappers.MessageListener;
import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Camera;
import com.aribot.bot.api.methods.Chat;
import com.aribot.bot.api.methods.Login;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.Walking;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.methods.interactables.Interactables;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.methods.tabs.Inventory;
import com.aribot.bot.api.methods.tabs.Skills;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.util.StringDrawing;
import com.aribot.bot.api.wrappers.Interactable;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.events.events.DrawDebug;
import com.aribot.bot.interfaces.io.Canvas;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

@Manifest(name = "Willow Chopper", authors = "Perfectionist", version = 1.00, description = "Chops down willow trees at the Draynor bank.")
public class PowerChop extends Script implements MessageListener, Painter {
    
    private int startExp, startLvl, trips, logCount = 0;
    private long startTime;
    
    protected boolean onStart() {
	if (!Login.isLoggedIn()) {
	    System.out.println("You must be logged in to use the script!");
	    return false;
	}
	startTime = System.currentTimeMillis();
	try {
	    startExp = Skills.getExperience(Skills.WOODCUTTING);
	    startLvl = Skills.getMaxLevel(Skills.WOODCUTTING);
	} catch (Exception e) {
	}
	return true;
    }
    
    protected void onFinish() {
	System.out.println("Time run: " + timeFormat(System.currentTimeMillis() - startTime));
	System.out.println("Experience gained: " + NumberFormat.getInstance().format(Skills.getExperience(Skills.WOODCUTTING) - startExp));
	System.out.println("Levels gained: " + NumberFormat.getInstance().format(Skills.getMaxLevel(Skills.WOODCUTTING) - startLvl));
	System.out.println("Trips made: " + NumberFormat.getInstance().format(trips));
	System.out.println("Chopped logs: " + NumberFormat.getInstance().format(logCount));
		JFrame loaderStatus = new JFrame("Thanks for using AriBot.");
	SwingUtilities.updateComponentTreeUI(loaderStatus);
	loaderStatus.setSize(300,100);
	Dimension frameSize2 = loaderStatus.getSize();
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	loaderStatus.setLocation((screenSize.width - frameSize2.width) / 2, (screenSize.height - frameSize2.height) / 2);
		JLabel statusLabel = new JLabel("Thanks for using AriBot.", SwingConstants.CENTER);
		statusLabel.setText("<html><body><center>Thanks for using AriBot.<br>Be sure to check out the site for updates and more.</center></body></html>");
	
	loaderStatus.add(statusLabel, BorderLayout.CENTER);
	
	loaderStatus.setVisible(true);
    }
    
    @Override
    public void onPaint(Graphics g) {
	final Point loc = Mouse.getLocation();
	g.setColor(Color.BLUE);
	g.drawLine(loc.x, 0, loc.x, GameLoader.getApplet().getHeight());
	g.drawLine(0, loc.y, GameLoader.getApplet().getWidth(), loc.y);
	StringDrawing.drawLine(g, DrawDebug.lineIndex++, "Time run: [magenta]" + timeFormat(System.currentTimeMillis() - startTime));
	StringDrawing.drawLine(g, DrawDebug.lineIndex++,
		"Experience gained: [magenta]" + NumberFormat.getInstance().format(Skills.getExperience(Skills.WOODCUTTING) - startExp));
	StringDrawing.drawLine(g, DrawDebug.lineIndex++,
		"Levels gained: [magenta]" + NumberFormat.getInstance().format(Skills.getMaxLevel(Skills.WOODCUTTING) - startLvl));
	StringDrawing.drawLine(g, DrawDebug.lineIndex++,
		"Exp to level: [magenta]" + NumberFormat.getInstance().format(Skills.getXPToNextLevel(Skills.WOODCUTTING)));
	StringDrawing.drawLine(g, DrawDebug.lineIndex++, "Trips made: [magenta]" + NumberFormat.getInstance().format(trips));
	StringDrawing.drawLine(g, DrawDebug.lineIndex++, "Chopped logs: [magenta]" + NumberFormat.getInstance().format(logCount));
	
    }
    
    public int loop() {
	if ((Login.isLoggedIn()) && (Players.getMyPlayer().getAnimation() == -1) && (Players.getMyPlayer().getMoving() == 0) && (Players.getMyPlayer().getInteracting() == -1)) {
	    if (Inventory.isFull()) {
		Walking.walkTileMM(new Tile(3092, 3243, 0));
		sleep(5000);
		Interactable bank = Interactables.getNearest(new int[] { 23961 }); // 15: 2213
		if (bank != null) {
		    reclick: if (bank.isOnScreen()) {
			bank.getModel().paint(Canvas.getGameImage().getGraphics());
			bank.click(true);
			sleep(6000);
			if (!Inventory.getItem(1520).interact("Store All")) {
			    break reclick;
			}
			trips++;
			Walking.walkTileMM(new Tile(3085, 3237, 0));
		    } else {
			Camera.turnTo(bank);
			sleep(2000);
		    }
		}
	    } else {
		Interactable tree = Interactables.getNearest(new int[] { 5553, 5551, 1308 });
		if (tree != null) {
		    if (tree.isOnScreen()) {
			tree.getModel().paint(Canvas.getGameImage().getGraphics());
			tree.clickCenter(true);
			Camera.setPitch(true);
			sleep(2000);
		    } else {
			Camera.turnTo(tree);
			sleep(2000);
		    }
		}
	    }
	} else if (Players.getMyPlayer().getAnimation() == 876 || Players.getMyPlayer().getAnimation() == 873) {
	    int randSwitch = random(1, 200);
	    switch (randSwitch) {
		case 100:
		    Camera.setAngle(random(1, 359));
		    break;
		default:
		    break;
	    }
	}
	return 1000;
    }
    
    private static String timeFormat(long amount) {
	long hours = TimeUnit.MILLISECONDS.toHours(amount);
	long minutes = TimeUnit.MILLISECONDS.toMinutes(amount) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(amount));
	long seconds = TimeUnit.MILLISECONDS.toSeconds(amount) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(amount));
	return hours + ":" + minutes + ":" + seconds;
    }
    
    @Override
    public void messageInbound(Message e) {
	if (e.getID() == Chat.SERVER && e.getMessage().contains("You get some")) {
	    logCount++;
	}
    }
    
}