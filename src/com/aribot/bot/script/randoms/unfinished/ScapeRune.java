package com.aribot.bot.script.randoms.unfinished;

import com.aribot.bot.api.internal.wrappers.Message;
import com.aribot.bot.api.internal.wrappers.MessageListener;
import com.aribot.bot.api.methods.Chat;
import com.aribot.bot.script.randoms.Randoms;

public class ScapeRune extends Randoms implements MessageListener {

    // Drain X1: Aaaarrrgh!
    // Server: Welcome to ScapeRune.
    private boolean messageRecieved = false;
    
    @Override
    public boolean shouldActivate() {
	return messageRecieved;
    }

    @Override
    public int loop() {
	return 500;
    }

    @Override
    public boolean onExecute() {
	return false;
    }

    @Override
    protected void onFinish() {
	messageRecieved = false;
    }

    @Override
    public void messageInbound(Message e) {
	if (e.getID() == Chat.SERVER && e.getMessage().equals("Welcome to ScapeRune.")) {
		messageRecieved = true;
	}
    }
    
}
