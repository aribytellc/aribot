package com.aribot.bot.script.randoms.unfinished;

import com.aribot.bot.api.internal.wrappers.Message;
import com.aribot.bot.api.internal.wrappers.MessageListener;
import com.aribot.bot.api.methods.Chat;
import com.aribot.bot.script.randoms.Randoms;

public class MysteriousBox extends Randoms implements MessageListener {

	private boolean messageRecieved = false;
	
	// 0 :  : You have been given a Mysterious box! Click on it and answer the question
	// 0 :  : correctly to get the prize inside!
	
	@Override
	public void messageInbound(Message e) {
		if (e.getID() == Chat.SERVER && e.getMessage().equals("You have been given a Mysterious box! Click on it and answer the question")) {
			messageRecieved = true;
		}
	}

	@Override
	public boolean shouldActivate() {
		return messageRecieved;
	}

	@Override
	public int loop() {
		return 500;
	}

	@Override
	public boolean onExecute() {
		return false;
	}

	@Override
	protected void onFinish() {
		messageRecieved = false;
	}

}
