package com.aribot.bot.script.randoms.unfinished;

import com.aribot.bot.script.randoms.Randoms;

/**
 * Abyss, 4 levers.
 */
public class LostAndFound extends Randoms {

    @Override
    public boolean shouldActivate() {
	return false;
    }

    @Override
    public int loop() {
	return 500;
    }

    @Override
    public boolean onExecute() {
	return false;
    }

    @Override
    protected void onFinish() {
    }

}
