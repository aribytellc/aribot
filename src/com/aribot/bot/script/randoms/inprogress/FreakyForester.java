package com.aribot.bot.script.randoms.inprogress;

import com.aribot.bot.api.methods.Camera;
import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.methods.interactables.Interactables;
import com.aribot.bot.api.methods.interactables.NPCs;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.methods.nodes.GroundItems;
import com.aribot.bot.api.methods.tabs.Inventory;
import com.aribot.bot.api.util.Condition;
import com.aribot.bot.api.util.Time;
import com.aribot.bot.api.wrappers.GroundItem;
import com.aribot.bot.api.wrappers.Interactable;
import com.aribot.bot.api.wrappers.NPC;
import com.aribot.bot.api.wrappers.Widget;
import com.aribot.bot.script.randoms.Randoms;

public class FreakyForester extends Randoms {
    
    private static final int FREAKY_FORESTER_ID = 5460;
    private static final int PORTAL_ID = -1; //TODO: Get the portal ID
    private static final int CHATBOX_INTERFACE = 243;
    private static final int RAW_PHEASANT_ID = -1; //TODO: Get the raw pheasant ID
    
    private static final String FREAKY_FORESTER_NAME = "Freaky Forester";
    
    private static final int ONE_TAIL = 6206;
    private static final int TWO_TAIL = 6208;
    private static final int THREE_TAIL = 6207;
    private static final int FOUR_TAIL = 6205;
    
    private NPC forester = null;
    private NPC target = null;
    private boolean finished = false;
    
    @Override
    public boolean shouldActivate() {
	return (forester = NPCs.getNearest(FREAKY_FORESTER_ID)) != null;
    }
    
    @Override
    public boolean onExecute() {
	target = null;
	forester = null;
	finished = false;
	return true;
    }
    
    @Override
    public int loop() {
	switch(getState()) {
	    case UNASSIGNED:
		unassigned();
		break;
	    case KILL:
		return kill();
	    case LOOT:
		return loot();
	    case RETURN:
		return returnMeat();
	    case EXIT:
		exit();
		break;
	}
	return 500;
    }
    
    private void unassigned() {
	forester = NPCs.getNearest(FREAKY_FORESTER_ID);
	if (forester != null) {
	    if (forester.isOnScreen()) {
		if (!Widgets.getParent(CHATBOX_INTERFACE).isHidden() || forester.interact("Talk-to")) { //If the interface is not visible, interact.
		    if (Time.sleepUntil(new Condition() { //Returns true immediately if interface already visible
			public boolean meet() {
			    return !Widgets.getParent(CHATBOX_INTERFACE).isHidden();
			}
		    }, 4000)) {
			String chatboxText = getChatboxText();
			if (chatboxText.contains("one")) {
			    target = NPCs.getNearest(ONE_TAIL);
			}
			else if (chatboxText.contains("two")) {
			    target = NPCs.getNearest(TWO_TAIL);
			}
			else if (chatboxText.contains("three")) {
			    target = NPCs.getNearest(THREE_TAIL);
			}
			else if (chatboxText.contains("four")) {
			    target = NPCs.getNearest(FOUR_TAIL);
			}
			else {
			    System.out.println("Can't determine which pheasant to kill from the given chatbox text:\n" + chatboxText);
			}
		    }
		    else {
			System.out.println("Sleep timed out. Failed to detect chat box to confirm speaking with the FF");
		    }
		}
	    }
	    else {
		Camera.turnTo(forester);
	    }
	}
	else {
	    System.out.println("Forester returned null. Cannot interact with him to complete this AntiRandom.");
	}
    }
    
    private int kill() {
	if (target != null) {
	    if (target.getHPPercent() == 0) {
		target = null;
		return 500;
	    }
	    if (target.isOnScreen()) {
		if (target.interact("Attack")) {
		    if (Time.sleepUntil(
			    new Condition() {
				public boolean meet() {
				    return GroundItems.getNearest(RAW_PHEASANT_ID) != null;
				}
			    }, 
			    new Condition() {
				public boolean meet() {
				    return Players.getMyPlayer().isMoving();
				}
			    }, 
			    5500)) {
			return 200;
		    }
		    System.out.println("Failed to interact with the pheasant. Trying again.");
		}
		else {
		    System.out.println("Interaction with pheasant failed completely.");
		}
	    }
	    else {
		Camera.turnTo(target);
	    }
	}
	else {
	    System.out.println("State was KILL even though our target is null.");
	}
	return 500;
    }
    
    private static int loot() {
	GroundItem meat = GroundItems.getNearest(RAW_PHEASANT_ID);
	if (meat != null) {
	    if (meat.getLocation().isOnScreen()) {
		if (meat.interact("Take")) {
		    if (!Time.sleepUntil(new Condition() {
			public boolean meet() {
			    return Inventory.getCount(RAW_PHEASANT_ID) > 0;
			}
		    }, 5000)) {
			System.out.println("Timed out waiting to loot pheasant meat. ID: " + RAW_PHEASANT_ID);
		    }
		}
	    }
	    else {
		//Camera.turnTo(meat.getLocation());
	    }
	}
	else {
	    System.out.println("State was LOOT, but there is no pheasant meat on the ground.");
	}
	return 200;
    }
    
    private int returnMeat() {
	if (Inventory.contains(RAW_PHEASANT_ID)) {
	    forester = NPCs.getNearest(FREAKY_FORESTER_ID);
	    if (forester != null) {
		if (forester.isOnScreen()) {
		    if (!Widgets.getParent(CHATBOX_INTERFACE).isHidden() || forester.interact("Talk-to")) {
			if (Time.sleepUntil(new Condition() { //Returns true immediately if interface already visible
				public boolean meet() {
				    return !Widgets.getParent(CHATBOX_INTERFACE).isHidden();
				}
			    }, 4000)) {
			    String chatboxText = getChatboxText();
			    if (chatboxText.contains("Thank you") || chatboxText.contains("leave")) {
				finished = true;
			    }
			}
		    }
		}
		else {
		    Camera.turnTo(forester);
		}
	    }
	    else {
		System.out.println("Forester returned null. Cannot interact with him to complete this AntiRandom.");
	    }
	}
	else {
	    System.out.println("State was RETURN but we have no meat in the inventory. ID: " + RAW_PHEASANT_ID);
	}
	return 200;
    }
    
    private void exit() {
	Interactable portal = Interactables.getNearest(PORTAL_ID);
	if (portal != null) {
	    if (portal.isOnScreen()) {
		if (portal.interact(portal.getActions()[0])) {
		    if (Time.sleepUntil(new Condition() {
			@Override
			public boolean meet() {
			   return NPCs.getNearest(PORTAL_ID) == null;
			}
		    }, new Condition() {
			@Override
			public boolean meet() {
			    return Players.getMyPlayer().isMoving();
			}
		    }, 5000)) {
			FreakyForester.this.onFinish();
		    }
		}
	    }
	    else {
		Camera.turnTo(portal);
	    }
	}
	else {
	    System.out.println("Unable to find the exit portal. ID: " + PORTAL_ID);
	}
    }
    
    @Override
    protected void onFinish() {
	
    }
    
    private State getState() {
	if (target == null) { //We haven't assigned a target yet
	    return State.UNASSIGNED;
	}
	if (Inventory.getCount("Raw pheasant") == 0) {
	    return finished ? State.EXIT : (GroundItems.getNearest(RAW_PHEASANT_ID) == null) ? State.KILL : State.LOOT;
	}
	return State.RETURN;
	
    }
    
    private static String getChatboxText() {
	Widget parent = Widgets.getParent(CHATBOX_INTERFACE);
	String text = "";
	for (Widget child : parent.getChildren()) {
	    if (!child.getText().equals("Freaky Forester") && !child.getText().contains("continue")) {
		text += child.getText();
	    }
	}
	System.out.println("Interface text: " + text);
	return text;
    }
    
    private enum State {
	UNASSIGNED,
	KILL,
	LOOT,
	RETURN,
	EXIT
    }
    
}
