package com.aribot.bot.script.randoms.inprogress;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Camera;
import com.aribot.bot.api.methods.interactables.Interactables;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.methods.interactables.WallObjects;
import com.aribot.bot.api.methods.walking.Walking;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.util.Random;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.api.wrappers.WallSceneObject;
import com.aribot.bot.api.wrappers.Tile.Flag;
import com.aribot.bot.script.randoms.Randoms;

/**
 * Obsolete. Offsets do not work
 */
public class Maze extends Randoms {
    
    private static int ANGLE_NORTH = 0, ANGLE_EAST = 1500, ANGLE_SOUTH = 1000, ANGLE_WEST = 500;
    
    private int doorId = -1;
    private HashSet<Tile> blacklist = new HashSet<>();
    
    @Override
    public boolean shouldActivate() {
	return Interactables.getNearest("Strange shrine") != null;
    }
    
    @Override
    public boolean onExecute() {
	blacklist.clear();
	return (doorId = getDoorId()) != -1;
    }
    
    @Override
    public int loop() {
	Camera.setPitch(true);
	//Start finding doors
	if (Calculations.distanceBetween(Players.getMyPlayer().getLocation(), Interactables.getNearest("Strange shrine").getLocation()) != 1) {
	    for (WallSceneObject o : WallObjects.getLoaded(new Filter<WallSceneObject>() {
		public boolean apply(WallSceneObject t) {
		    return t.getId() == doorId;
		} })) {
		Tile reachable = canReach(o);
		if (!blacklist.contains(o.getLocation()) && reachable != null) {
		    if (Walking.walkTileMM(reachable)) {
			Camera.setAngle(getAngleFor(o.getLocation()));
		    }
		}
	    }
	}
	return 500;
    }
    
    @Override
    protected void onFinish() {
    }
    
    private Tile canReach(WallSceneObject door) {
	Tile doorTile = door.getLocation();
	for (Tile t : new Tile[] { doorTile.derive(0, 1), doorTile.derive(1, 0), doorTile.derive(0, -1), doorTile.derive(-1, 0) }) {
	    if (t.canReach()) {
		return t;
	    }
	}
	return null;
    }
    
    private int getAngleFor(Tile doorTile) {
	if (doorTile.isBlockedAt(Flag.N) || doorTile.isBlockedAt(Flag.S)) {
	    return Random.nextInt(0, 1) == 0 ? ANGLE_NORTH : ANGLE_SOUTH;
	}
	return Random.nextInt(0, 1) == 0 ? ANGLE_EAST : ANGLE_WEST;
    }
    
    private static int getDoorId() {
	WallSceneObject[] loaded = WallObjects.getLoaded();
	HashMap<Integer, Integer> countMap = new HashMap<>(); //id, count
	for (WallSceneObject o : loaded) {
	    if (!countMap.keySet().contains(o.getId())) {
		countMap.put(o.getId(), 1);
	    }
	    else {
		countMap.put(o.getId(), countMap.get(o.getId()) + 1);
	    }
	}
	int value = thirdHighest(countMap.values());
	for (int key : countMap.keySet()) {
	    if (countMap.get(key) == value) {
		return key;
	    }
	}
	return -1;
    }
    
    private static int thirdHighest(Collection<Integer> counts) {
	counts.remove(highest(counts));
	counts.remove(highest(counts));
	return highest(counts);
    }
    
    private static int highest(Collection<Integer> counts) {
	int highest = 0;
	for (int i : counts) {
	    if (i > highest) {
		highest = i;
	    }
	}
	return highest;
    }
    
}
