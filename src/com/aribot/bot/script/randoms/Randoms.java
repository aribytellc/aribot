package com.aribot.bot.script.randoms;

import com.aribot.bot.events.LoopTask;

public abstract class Randoms extends LoopTask {

	/**
	 * Should this Random event activate. Give conditions to activate on
	 * @return boolean, true is it should activate
	 */
	public abstract boolean shouldActivate();

	/**
	 * Initialize anything here and double check to make sure that this Random is required.
	 * @return False will exit out of Random, true will continue to solve Random.
	 */
	public abstract boolean onExecute();
	
	/**
	 * The task that this Random event should execute.
	 * @return int
	 */
	public abstract int loop();
	
	/**
	 * Any cleanup or resets required to trigger it again.
	 */
	protected abstract void onFinish();
	
}
