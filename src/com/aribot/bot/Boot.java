package com.aribot.bot;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.aribot.bot.events.events.DrawDebug;
import com.aribot.bot.events.events.LogPane;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;
import com.aribot.bot.loader.modparser.ModParser;

public class Boot {

    public static JProgressBar progressBar;
    public static GameLoader gl = null;

    public static void main(String[] args) {

	try {
	    UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceGraphiteGlassLookAndFeel");
	} catch (Exception e) {
	    e.printStackTrace();
	}

	for (String arg : args) {
	    if (arg.equalsIgnoreCase("log")) {
		LogPane.redirectSystemStreams(true);
		Thread logPane = new Thread(new LogPane());
		logPane.start();
	    } else if (arg.equals("8qWx8JuBEQCgCTPbKzrQbsqC")) {
		Config.devOverride = true;
	    } else {
		System.out.println("The only CLI option is \"log\" which defaults the logpane to show.");
		return;
	    }
	}

	File binFold = new File("bin/");
	if (!binFold.exists()) {
	    binFold.mkdirs();
	}
	File srcFold = new File("src/");
	if (!srcFold.exists()) {
	    srcFold.mkdirs();
	}
	File compBat = new File("Compile.bat");
	if (!compBat.exists()) {
	    try {
		compBat.createNewFile();
		BufferedWriter bw = new BufferedWriter(new FileWriter(compBat.getAbsoluteFile()));
		BufferedReader br = new BufferedReader(new InputStreamReader(Downloader.getResourceURL("resources/Compile.bat").openStream()));
		String currentLine = "";
		while ((currentLine = br.readLine()) != null) {
		    bw.write(currentLine + System.lineSeparator());
		}
		br.close();
		bw.close();
	    } catch (Exception e) {
		e.printStackTrace();
	    }
	}
	
	
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	JFrame loaderStatus = new JFrame("Loading AriBot");
	SwingUtilities.updateComponentTreeUI(loaderStatus);
	loaderStatus.setSize(300,100);
	Dimension frameSize2 = loaderStatus.getSize();
	loaderStatus.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	loaderStatus.setLocation((screenSize.width - frameSize2.width) / 2, (screenSize.height - frameSize2.height) / 2);
	JLabel statusLabel = new JLabel("Starting AriBot", SwingConstants.CENTER);
	statusLabel.setText("Downloading 2007 Runescape.");

	loaderStatus.add(statusLabel, BorderLayout.CENTER);

	loaderStatus.setVisible(true);
	ModParser mParse = new ModParser();

	if (Config.updated) {

	    progressBar = new JProgressBar(0, 100);
	    progressBar.setStringPainted(true);
	    progressBar.setValue(0); 
	    loaderStatus.add(progressBar, BorderLayout.SOUTH);
	    loaderStatus.revalidate();
	    new Downloader();
	    progressBar.setVisible(false);
	    statusLabel.setText("Working on loading AriBot.");
	    mParse.inject();
	    statusLabel.setText("Loading AriBot.");
	    gl = new GameLoader("client");
	    GameLoader.getInstance().addListener(DrawDebug.class);

	}
	loaderStatus.dispose();
    }

}