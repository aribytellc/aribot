package com.aribot.bot.events;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class TaskContainer {
	private final Map<Integer, LoopTask> tasks = new HashMap<>();

	public TaskContainer() {
	}

	public int pool(final LoopTask loopTask) {
		for (int off = 0; off < tasks.size(); ++off) {
			if (!tasks.containsKey(off)) {
				tasks.put(off, loopTask);
				loopTask.setID(off);
				return off;
			}
		}
		loopTask.container = this;
		loopTask.setID(tasks.size());
		tasks.put(tasks.size(), loopTask);
		return tasks.size() - 1;
	}

	private boolean start(final int loopTaskID) {
		final LoopTask task = tasks.get(loopTaskID);
		if (task != null) {
			if (task.doExecution()) {
				task.init(GameLoader.service.submit(task, task));
				return true;
			}
		}
		return false;
	}

	public boolean invoke(final int loopTaskID) {
		return start(loopTaskID);
	}

	public void invokeAll() {
		for (LoopTask task : tasks.values()) {
			start(task.id);
		}
	}

	public void stop(final int loopTaskID) {
		final LoopTask task = tasks.get(loopTaskID);
		if (task != null) {
			task.stop();
			tasks.remove(loopTaskID);
		}
	}

	public void stop() {
		for (LoopTask task : tasks.values()) {
			task.stop();
		}
	}

	public boolean remove(final int loopTaskID) {
		final LoopTask task = tasks.get(loopTaskID);
		if (task != null) {
			task.stop();
			tasks.remove(loopTaskID);
			return true;
		}
		return false;
	}

	protected Map<Integer, LoopTask> getTasks() {
		return Collections.unmodifiableMap(tasks);
	}
}
