package com.aribot.bot.events;

import java.util.concurrent.Future;

public interface Task extends Runnable {
	
	void init(Future<?> f);
	
	boolean isCompleted();

	void stop();

	void join();

	
}
