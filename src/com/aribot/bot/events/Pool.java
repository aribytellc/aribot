package com.aribot.bot.events;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class Pool implements ThreadFactory {
	private static final AtomicInteger threadNumber = new AtomicInteger(1);

	public Thread newThread(Runnable r) {
		Thread thread = new Thread(new ThreadGroup("Scripts"), r, "Scripts-" + threadNumber.getAndIncrement());
		if (thread.isDaemon()) {
			thread.setDaemon(false);
		}
		if (thread.getPriority() != Thread.NORM_PRIORITY) {
			thread.setPriority(Thread.NORM_PRIORITY);
		}
		return thread;
	}
}