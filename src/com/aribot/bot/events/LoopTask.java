package com.aribot.bot.events;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.Iterator;

import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;
import com.aribot.bot.loader.GUI.main;
import com.aribot.bot.script.randoms.Randoms;
import com.aribot.bot.script.randoms.finished.*;

public abstract class LoopTask extends Container implements EventListener {
    protected int id = -1;
    private boolean running = false;
    protected TaskContainer container;
    private ArrayList<Randoms> randoms = new ArrayList<>();

    protected boolean doExecution() {
	return true;
    }

    protected void onRun() {
    }

    protected void onFinish() {
    }

    protected int pausedIterationDelay() {
	return 500;
    }

    /**
     * The main loop. Return an int to sleep after execution. Return -1 to stop the script.
     */
    protected abstract int loop();

    protected boolean runOnce() {
	int timeOut = -1;
	try {
	    // Randoms here?
	    evalRandoms();
	    timeOut = loop();
	} catch (ThreadDeath td) {
	    return false;
	} catch (Exception e) {
	    System.out.println("Uncaught exception from task: " + e.getMessage());
	    e.printStackTrace();
	}
	if (timeOut == -1) {
	    return false;
	}
	try {
	    Calculations.sleep(timeOut);
	} catch (ThreadDeath td) {
	    return false;
	}
	return true;
    }

    protected void register() {
	GameLoader.getInstance().getManager().addListener(this);
    }

    protected void unregister() {
	GameLoader.getInstance().getManager().removeListener(this);
    }

    public void run() {
	setRandoms();
	running = true;
	try {
	    onRun();
	} catch (Exception e) {
	    System.out.println("Error starting task: " + e.getMessage());
	}
	try {
	    while (running) {
		if (!runOnce()) {
		    break;
		}
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	try {
	    onFinish();
	} catch (Exception e) {
	    e.printStackTrace();
	}
	stop();
    }

    protected void setID(final int id) {
	this.id = id;
    }

    public void setRunning(final boolean running) {
	this.running = running;
    }

    public final boolean isRunning() {
	return running;
    }

    @Override
    public void stop() {
	this.running = false;
	main.setStopOn(false);
	main.setPlayOn(true);
	main.setPauseOn(false);
	unregister();
	super.stop();
    }

    public int getId() {
	return id;
    }
    
    protected void setRandoms() {
	//randoms.add(new asd());
    }
    
    private void evalRandoms() {
	Iterator<Randoms> ti = randoms.iterator();
	while (ti.hasNext()) {
	    Randoms at = ti.next();
	    if (at.shouldActivate()) {
		if (at.onExecute()) {
		    boolean running2 = true;
		    int time = 0;
		    while(running2) {
			time = at.loop(); 
			if (time == -1) {
			    running2 = false;
			    this.onFinish();
			}
		    }
		}
	    }
	}
    }
    
}