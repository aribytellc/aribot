package com.aribot.bot.events;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public abstract class Container implements Task {
	private Future<?> futur;

	public void init(Future<?> f) {
		this.futur = f;
	}
	
	public boolean isCompleted() {
		return futur.isDone();
	}

	public void stop() {
		futur.cancel(true);
	}

	public void join() {
		try {
			futur.get();
		} catch (InterruptedException ignored) {
		} catch (ExecutionException ignored) {
		}
	}

	
}
