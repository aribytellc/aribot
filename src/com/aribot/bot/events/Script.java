package com.aribot.bot.events;

import java.util.Collections;
import java.util.Map;

public class Script extends TaskContainer {
	
	public Script() {
	}

	public void stopScript(final int id) {
		final LoopTask script = getTasks().get(id);
		if (script != null) {
			script.stop();
			remove(id);
		}
	}
	
	public int runScript(final com.aribot.bot.api.methods.Script script) {
		final int id = pool(script);
		invoke(id);
		return id;
	}

	public void stopAllScripts() {
		stop();
	}
	
	public Map<Integer, LoopTask> getRunningScripts() {
		return Collections.unmodifiableMap(getTasks());
	}
	
}
