package com.aribot.bot.events.events;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.text.DefaultCaret;

public class LogPane extends JFrame implements Runnable {

	private static final long serialVersionUID = 6918066619982525455L;
	private JPanel contentPane;
	private static JTextArea txtArea;
	private static PrintStream stdOut = System.out;
	private static PrintStream stdErr = System.err;
	private static boolean custom = false;

	@Override
	public void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					LogPane frame = new LogPane();

					frame.addWindowListener(new WindowAdapter() {
						@Override
						public void windowClosing(WindowEvent arg0) {
							LogPane.redirectSystemStreams(false);
						}
					});

					Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
					Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration());
					Dimension frameSize = frame.getSize();
					frame.setLocation((screenSize.width - screenInsets.left - screenInsets.right - frameSize.width) / 2,
							(screenSize.height - screenInsets.top - screenInsets.bottom - frameSize.height) / 2);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public LogPane() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setTitle("Log Pane");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scrollPane, BorderLayout.CENTER);

		txtArea = new JTextArea();
		DefaultCaret caret = (DefaultCaret) txtArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		txtArea.setText("");
		txtArea.setEditable(false);
		scrollPane.setViewportView(txtArea);
	}

	public static void updateTextArea(final String text) {
		updateTextArea(text, null);
	}
	
	public static void updateTextArea(final String text, final Color color) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				txtArea.setSelectionColor(color);
				txtArea.append(text);
			}
		});
	}

	public static boolean isRedirected() {
		return custom;
	}

	public static void redirectSystemStreams(boolean redirect) {
		custom = redirect;
		if (custom) {
			OutputStream out = new OutputStream() {
				@Override
				public void write(int b) throws IOException {
					updateTextArea(String.valueOf((char) b));
				}

				@Override
				public void write(byte[] b, int off, int len) throws IOException {
					updateTextArea(new String(b, off, len));
				}

				@Override
				public void write(byte[] b) throws IOException {
					write(b, 0, b.length);
				}
			};

			System.setOut(new PrintStream(out, true));
			System.setErr(new PrintStream(out, true));
		} else {
			System.setOut(stdOut);
			System.setErr(stdErr);
		}
	}

}
