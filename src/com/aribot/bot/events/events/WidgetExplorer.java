package com.aribot.bot.events.events;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

import com.aribot.bot.api.internal.events.InputManager;
import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.wrappers.Widget;
import com.aribot.bot.interfaces.Interfaces;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;
import com.aribot.bot.loader.GUI.main;

public class WidgetExplorer extends JFrame implements Painter, Runnable  {
    
    private static final long serialVersionUID = 1097174858121198625L;
    private JPanel contentPane;
    public static Widget wget;
    
    @Override
    public void run() {
	EventQueue.invokeLater(new Runnable() {
	    @SuppressWarnings("synthetic-access")
	    public void run() {
		try {
		    WidgetExplorer frame = new WidgetExplorer();
		    frame.addWindowListener(new WindowExitAdapter());
		    frame.setVisible(true);
		    
		} catch (Exception e) {
		    e.printStackTrace();
		}
	    }
	});
	GameLoader.getInstance().getManager().addListener(this);
    }
    
    public WidgetExplorer() {
	setPreferredSize(new Dimension(400, 550));
	contentPane = new JPanel();
	contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	contentPane.setLayout(new BorderLayout(0, 0));
	setContentPane(contentPane);
	
	JMenuBar menuBar = new JMenuBar();
	contentPane.add(menuBar, BorderLayout.NORTH);
	
	final JTextField filterText = new JTextField(10);
	
	final JButton btnNewButton = new JButton("Filter");
	
	menuBar.add(filterText);
	menuBar.add(btnNewButton);
	
	final JSplitPane splitPane = new JSplitPane();
	splitPane.setDividerLocation(getBounds().width / 4);
	contentPane.add(splitPane, BorderLayout.CENTER);
	
	final JPanel panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	splitPane.setRightComponent(panel);
	
	JScrollPane scrollPane = new JScrollPane();
	splitPane.setLeftComponent(scrollPane);
	
	final JTree tree = new JTree();
	scrollPane.setViewportView(tree);
	
	btnNewButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent arg0) {
		tree.removeAll();
		tree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("Widgets") {
		    private static final long serialVersionUID = -4321571122195454875L;
		    {
			DefaultMutableTreeNode node_1;
			Interfaces[][] ifaces = Widgets.getInterfaces();
			for (int i = 0; i < ifaces.length; i++) {
			    if (ifaces[i] != null) {
				node_1 = new DefaultMutableTreeNode(i);
				for (int j = 0; j < ifaces[i].length; j++) {
				    if (ifaces[i][j] != null && ifaces[i][j].getText().toLowerCase().contains(filterText.getText().toLowerCase())) {
					node_1.add(new DefaultMutableTreeNode(j));
				    }
				}
				add(node_1);
			    }
			}
		    }
		}));
		tree.validate();
		tree.repaint();
	    }
	});
	
	tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
	
	tree.addTreeSelectionListener(new TreeSelectionListener() {
	    
	    private void addInfo(final String key, final String value) {
		final JPanel row = new JPanel();
		row.setAlignmentX(Component.LEFT_ALIGNMENT);
		row.setLayout(new BoxLayout(row, BoxLayout.X_AXIS));
		for (final String data : new String[] { key, value }) {
		    final JLabel label = new JLabel(data);
		    label.setAlignmentY(Component.TOP_ALIGNMENT);
		    row.add(label);
		}
		panel.add(row);
	    }
	    
	    @Override
	    public void valueChanged(TreeSelectionEvent e) {
		int par = 0;
		int cur = 0;
		try {
		    par = Integer.parseInt(e.getPath().getParentPath().getLastPathComponent().toString());
		    cur = Integer.parseInt(e.getPath().getLastPathComponent().toString());
		} catch (Exception e2) {
		}
		try {
		    wget = Widgets.getInterface(par, cur);
		    int parentID = 0;
		    int count = 0;
		    panel.removeAll();
		    do {
			String sep = ((count > 0) ? String.format("%" + count + "s", "").replaceAll(" ", "-") : "");
			Widget parent = ((parentID == 0) ? Widgets.getInterface(par, cur) : Widgets.getParent(parentID));
			addInfo(sep + "Offset X: ", "" + parent.getOffsetX());
			addInfo(sep + "Offset Y: ", "" + parent.getOffsetY());
			addInfo(sep + "Width: ", "" + parent.getWidth());
			addInfo(sep + "Height: ", "" + parent.getHeight());
			addInfo(sep + "Text: ", "" + parent.getText());
			addInfo(sep + "Actions: ", "" + Arrays.toString(parent.getActions()));
			addInfo(sep + "Tooltip: ", "" + parent.getTooltip());
			addInfo(sep + "Name: ", "" + parent.getName());
			addInfo(sep + "Hidden: ", "" + parent.isHidden());
			addInfo(sep + "Inventory: ", "" + parent.isInventory());
			addInfo(sep + "ScrollX: ", "" + parent.getScrollX());
			addInfo(sep + "ScrollY: ", "" + parent.getScrollY());
			addInfo(sep + "ID: ", "" + parent.getID());
			addInfo(sep + "TextureID: ", "" + parent.getTextureID());
			addInfo(sep + "ModelID: ", "" + parent.getModelID());
			addInfo(sep + "BorderThickness: ", "" + parent.getBorderThickness());
			//addInfo(sep + "SlotIDs: ", "" + ((parent.getSlotIDs().length < 100) ? Arrays.toString(parent.getSlotIDs()) : "Too many to list safely"));
			//addInfo(sep + "StackSizes: ", "" + ((parent.getStackSizes().length < 100) ? Arrays.toString(parent.getStackSizes()) : "Too many to list safely"));
			addInfo(sep + "HasChildren: ", "" + ((parent.getChildren() != null) ? parent.getChildren().length : "False"));
			addInfo(sep + "ParentID: ", "" + parent.getParentID());
			parentID = parent.getParentID();
			count += 2;
		    } while (parentID != -1 && count < (2 * 10));
		    
		    System.out.println("Value changed");
		    panel.revalidate();
		    panel.repaint();
		} catch (Exception e3) {
		}
	    }
	});
	
	tree.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("Widgets") {
	    private static final long serialVersionUID = -6941654797351542737L;
	    {
		DefaultMutableTreeNode node_1;
		Interfaces[][] ifaces = Widgets.getInterfaces();
		for (int i = 0; i < ifaces.length; i++) {
		    if (ifaces[i] != null) {
			node_1 = new DefaultMutableTreeNode(i);
			for (int j = 0; j < ifaces[i].length; j++) {
			    if (ifaces[i][j] != null) {
				node_1.add(new DefaultMutableTreeNode(j));
			    }
			}
			add(node_1);
		    }
		}
	    }
	}));
	
	pack();
	splitPane.setDividerLocation(getBounds().width / 4);
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(getGraphicsConfiguration());
	Dimension frameSize = getSize();
	setLocation((screenSize.width - screenInsets.left - screenInsets.right - frameSize.width) / 2,
		(screenSize.height - screenInsets.top - screenInsets.bottom - frameSize.height) / 2);
	
    }
    
    private class WindowExitAdapter extends WindowAdapter {
	public void windowClosing(WindowEvent e) {
	    int remIndex = main.inactiveDebug.indexOf("Interface Explorer");
	    if (remIndex == -1) {
		remIndex = main.activeDebug.indexOf("Interface Explorer");
		if (remIndex == -1) {
		    DrawDebug.isRunning = false;
		    return;
		}
		main.inactiveDebug.add("Interface Explorer");
		main.activeDebug.remove(remIndex);
		InputManager.getTarget().getComponent(0).requestFocus();
		DrawDebug.isRunning = false;
		return;
	    }
	    DrawDebug.isRunning = false;
	}
    }
    
    @Override
    public void onPaint(Graphics graphics) {
	if (wget != null) {
	    wget.drawOutline(graphics);
	}
    }
}
