package com.aribot.bot.events.events;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Arrays;
import java.util.Iterator;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Camera;
import com.aribot.bot.api.methods.Chat;
import com.aribot.bot.api.methods.Game;
import com.aribot.bot.api.methods.Settings;
import com.aribot.bot.api.methods.interactables.FloorDecorationObjects;
import com.aribot.bot.api.methods.interactables.Interactables;
import com.aribot.bot.api.methods.interactables.NPCs;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.methods.interactables.WallDecorationObjects;
import com.aribot.bot.api.methods.interactables.WallObjects;
import com.aribot.bot.api.methods.nodes.Menu;
import com.aribot.bot.api.methods.tabs.Inventory;
import com.aribot.bot.api.methods.tabs.Skills;
import com.aribot.bot.api.methods.widgets.Bank;
import com.aribot.bot.api.util.StringDrawing;
import com.aribot.bot.api.wrappers.BankItem;
import com.aribot.bot.api.wrappers.FloorDecorationSceneObject;
import com.aribot.bot.api.wrappers.GroundItem;
import com.aribot.bot.api.wrappers.Interactable;
import com.aribot.bot.api.wrappers.Item;
import com.aribot.bot.api.wrappers.NPC;
import com.aribot.bot.api.wrappers.Player;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.api.wrappers.WallDecorationSceneObject;
import com.aribot.bot.api.wrappers.WallSceneObject;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.interfaces.io.Canvas;
import com.aribot.bot.interfaces.io.Mouse;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;
import com.aribot.bot.loader.GUI.main;

public class DrawDebug implements Painter {
    
    public static int lineIndex = 0;
    public static boolean isRunning = false;
    private static final int TIMEOUT = 5000;
    
    private int[] lastSettings = new int[0];
    private long[] settingsAge = new long[0];
    
    public static Client getClient() {
	return GameLoader.getClient();
    }
    
    @Override
    public void onPaint(Graphics buffer) {
	lineIndex = 0;
	Iterator<String> activeDebugDrawing = main.activeDebug.iterator();
	while (activeDebugDrawing.hasNext()) {
	    switch (activeDebugDrawing.next()) {
		case "Mouse":
		    final int mouse_x = Mouse.getX();
		    final int mouse_y = Mouse.getY();
		    final String off = Mouse.isPresent() ? "" : " (off)";
		    StringDrawing.drawLine(buffer, lineIndex++, "Mouse Position: (" + mouse_x + "," + mouse_y + ")" + off);
		    int mouse_press_x = Mouse.getPressX();
		    int mouse_press_y = Mouse.getPressY();
		    long mouse_press_time = Mouse.getPressTime();
		    buffer.setColor(Color.GREEN);
		    buffer.drawLine(mouse_x - 5, mouse_y - 5, mouse_x + 5, mouse_y + 5);
		    buffer.drawLine(mouse_x + 5, mouse_y - 5, mouse_x - 5, mouse_y + 5);
		    if (System.currentTimeMillis() - mouse_press_time < 1000) {
			buffer.setColor(Color.RED);
			buffer.drawLine(mouse_press_x - 5, mouse_press_y - 5, mouse_press_x + 5, mouse_press_y + 5);
			buffer.drawLine(mouse_press_x + 5, mouse_press_y - 5, mouse_press_x - 5, mouse_press_y + 5);
		    }
		    break;
		    
		case "Login Index":
		    try {
			StringDrawing.drawLine(buffer, lineIndex++, "Login Index: " + Game.getLoginIndex());
		    } catch (Exception e) {
		    }
		    break;
		    
		case "Rotation & Zoom":
		    try {
			StringDrawing.drawLine(
				buffer,
				lineIndex++,
				"MinimapRotation: " + Game.getMinimapRotation() + " ViewRotation: " + Game.getViewRotation() + " MinimapZoom: " + Game.getMinimapZoom());
		    } catch (Exception e) {
		    }
		    break;
		    
		case "FPS":
		    final int secTime = (int) (System.currentTimeMillis() / 1000);
		    final int prevIdx = (secTime - 1) % Canvas.LEN;
		    StringDrawing.drawLine(buffer, lineIndex++, String.format("%2d fps", Canvas.frameCount[prevIdx]));
		    final int curIdx = secTime % Canvas.LEN;
		    if (Canvas.lastIdx != curIdx) {
			Canvas.lastIdx = curIdx;
			Canvas.frameCount[curIdx] = 1;
		    } else {
			Canvas.frameCount[curIdx]++;
		    }
		    break;
		    
		case "Menu Options":
		    for (int i = 0; i < Menu.getCount(); i++) {
			StringDrawing.drawLine(buffer, lineIndex++, i + ": [red]" + Menu.getActions()[i] + " " + Menu.getOptions()[i]);
		    }
		    break;
		    
		case "Menu":
		    StringDrawing.drawLine(buffer, lineIndex++, "Menu: " + ((Menu.isOpen()) ? "Open" : "Closed") + " Options: " + Menu.getCount());
		    StringDrawing.drawLine(buffer, lineIndex++, "Menu Location: (" + Menu.getX() + "," + Menu.getY() + ")");
		    StringDrawing.drawLine(buffer, lineIndex++, "Menu Size (W, H): (" + Menu.getWidth() + ", " + Menu.getHeight() + ")");
		    break;
		    
		case "Bases":
		    StringDrawing.drawLine(
			    buffer,
			    lineIndex++,
			    "Current X, Y, and Z positions: " + Players.getMyPlayer().getLocation().toString()
			    + " Current Base: (" + Game.getMapBaseX() + ", " + Game.getMapBaseY() + ", " + Game.getPlane() + ")"
			    + " Offsets: (" + (Players.getMyPlayer().rawOffsetX() >> 7) + ", " + (Players.getMyPlayer().rawOffsetY() >> 7) + ")");
		    
		    break;
		    
		case "Camera":
		    StringDrawing.drawLine(buffer, lineIndex++,
			    "Camera (X, Y, Z): (" + Game.getCameraX() + ", " + Game.getCameraY() + ", " + Game.getCameraZ() + ")");
		    StringDrawing.drawLine(buffer, lineIndex++, "Pitch: " + Game.getPitch() + " Yaw: " + Camera.getAngle());
		    break;
		    
		case "Inventory":
		    buffer.setColor(Color.WHITE);
		    final Item[] inventoryItems = Inventory.getItems();
		    for (final Item inventoryItem : inventoryItems) {
			buffer.drawString(Integer.toString(inventoryItem.getID()), inventoryItem.getScreenLocation().x, inventoryItem.getScreenLocation().y);
		    }
		    
		    break;
		    
		case "Players":
		    com.aribot.bot.api.wrappers.Player[] players = Players.getLoaded();
				StringDrawing.drawLine(buffer, lineIndex++, "Players loaded: " + players.length + " SelfInteractingIndex: " + Game.getSelfInteratctingIndex());
		    for (int i = 0; i < players.length; i++) {
			if (players[i] != null) {
			    players[i].getLocation().draw(buffer, new Color(0, 0, 50, 155), Color.GREEN,
				    players[i].getName() + " (" + players[i].getLevel() + ")" + " Anim: " + players[i].getAnimation() + " Interacting: " + players[i].getInteracting() + " NPCID: " + players[i].getNPCID(),
				    "Moving: " + players[i].getMoving() + " Health Percent: " + players[i].getHPPercent() + " Fighting: " + players[i].isFighting() + " Rot: " + players[i].getRotation(),
				    "Equipment: " + Arrays.toString(players[i].getRawEquipment()),
				    "Location: " + players[i].getLocation().getX() + ", " + players[i].getLocation().getY() + ", " + players[i].getLocation().getZ());
			    Point pointOnMM = Calculations.tileToMinimap(players[i].getLocation());
			    buffer.setColor(Color.ORANGE);
			    buffer.drawRect(pointOnMM.x, pointOnMM.y, 2, 2);
			    buffer.setColor(Color.CYAN);
			}
		    }
		    
		    break;
		    
		case "NPC":
		    NPC[] npcs = NPCs.getLoaded();
		    StringDrawing.drawLine(buffer, lineIndex++, "NPCs loaded: " + npcs.length);
		    for (int i = 0; i < npcs.length; i++) {
			if (npcs[i] != null) {
			    npcs[i].getLocation().draw(buffer, new Color(50, 0, 0, 155), Color.GREEN,
				    "ID: " + npcs[i].getId() + " " + npcs[i].getName() + " (" + npcs[i].getLevel() + ")" + " Anim: " + npcs[i].getAnimation() + " Interacting: " + npcs[i].getInteracting(),
				    "Moving: " + npcs[i].getMoving() + " Health Percent: " + npcs[i].getHPPercent() + " Fighting: " + npcs[i].isFighting() + " Rot: " + npcs[i].getRotation());
			    Point pointOnMM = Calculations.tileToMinimap(npcs[i].getLocation());
			    buffer.setColor(Color.PINK);
			    buffer.drawRect(pointOnMM.x, pointOnMM.y, 2, 2);
			    buffer.setColor(Color.CYAN);
			}
		    }
		    break;
		    
		case "Scene Models":
		    try {
			Interactable[] loadedInteractables = Interactables.getLoaded();
			for (int i = 0; i < loadedInteractables.length; i++) {
			    if (loadedInteractables[i].getModel() != null) {
				loadedInteractables[i].getModel().paint(buffer);
			    }
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    StringDrawing.drawLine(buffer, lineIndex++, "Scene Models: WireFraming");
		    break;
		    
		case "Interface Explorer":
		    if (!isRunning) {
			isRunning = true;
			Thread interfaceExplorer = new Thread(new WidgetExplorer());
			interfaceExplorer.start();
		    }
		    break;
		    
		case "Scene Interactables":
		    Interactable[] loadedInteractables = Interactables.getLoaded();
		    StringDrawing.drawLine(buffer, lineIndex++, "Interactables loaded: " + loadedInteractables.length);
		    for (int i = 0; i < loadedInteractables.length; i++) {
			loadedInteractables[i].getLocation().draw(buffer, new Color(0, 50, 0, 155), Color.GREEN,
				"ID: " + loadedInteractables[i].getId() + " " + loadedInteractables[i].getName() + " " + loadedInteractables[i].getLocation().toString(),
				Arrays.toString(loadedInteractables[i].getActions()));
		    }
		    
		    break;
		    
		case "Tile Boundries":
		    for (int queryX = 3; queryX < 102; queryX++) {
			for (int queryY = 3; queryY < 102; queryY++) {
			    final Tile analysisTile = new Tile(Game.getMapBaseX() + queryX, Game.getMapBaseY() + queryY, Game.getPlane());
			    analysisTile.draw(buffer, new Color(((!analysisTile.isWalkable() && !analysisTile.isSpecial()) ? 250 : 0), // Red if not walkable
				    ((analysisTile.isQuestionable()) ? 250 : 0), // Green if it doesn't have any direction flags
				    ((analysisTile.isSpecial()) ? 250 : 0), 155), Color.CYAN, "");
			    
			    if (analysisTile.isOnScreen()) {
				Point minimapPoint = Calculations.tileToMinimap(analysisTile);
				buffer.setColor(new Color(((!analysisTile.isWalkable() && !analysisTile.isSpecial()) ? 250 : 0), // Red if not walkable
					((analysisTile.isQuestionable()) ? 250 : 0), // Green if it doesn't have any direction flags
					((analysisTile.isSpecial()) ? 250 : 0), 155));
				buffer.drawRect(minimapPoint.x, minimapPoint.y, 2, 2);
				buffer.setColor(Color.CYAN);
			    }
			    
			}
		    }
		    break;
		case "Skill info":
		    StringDrawing.drawLine(buffer, lineIndex++, "Exp: " + Arrays.toString(Skills.getExperience()).replaceAll("\\[", "^").replaceAll("\\]", "^"));
		    StringDrawing.drawLine(buffer, lineIndex++,
			    "Real Lvls: " + Arrays.toString(Skills.getRealLevels()).replaceAll("\\[", "^").replaceAll("\\]", "^"));
		    StringDrawing.drawLine(buffer, lineIndex++,
			    "Max Lvls: " + Arrays.toString(Skills.getMaxLevels()).replaceAll("\\[", "^").replaceAll("\\]", "^"));
		    break;
		case "Item Definition":
		    try {
			final Item[] inventoryItems2 = Inventory.getItems();
			for (final Item inventoryItem : inventoryItems2) {
			    StringDrawing.drawLine(buffer, lineIndex++, Arrays.toString(inventoryItem.getActions()).replaceAll("\\[", "^").replaceAll("\\]", "^"));
			}
		    } catch (Exception e) {
			
		    }
		    break;
		    
		case "Chat":
		    try {
			StringDrawing.drawLine(buffer, lineIndex++, Chat.getLastLine());
		    } catch (Exception e) {
		    }
		    break;
		    
		case "Ground Items":
		    GroundItem[] loadedGroundItems = com.aribot.bot.api.methods.nodes.GroundItems.getLoaded();
		    for (int i = 0; i < loadedGroundItems.length; i++) {
			loadedGroundItems[i].getLocation().draw(buffer, new Color(0, 50, 0, 155), Color.CYAN,
				"ID: " + loadedGroundItems[i].getID() + " " + loadedGroundItems[i].getName() + " " + loadedGroundItems[i].getLocation().toString(),
				"Actions: " + Arrays.toString(loadedGroundItems[i].getGroundActions()));
		    }
		    
		    StringDrawing.drawLine(buffer, lineIndex++, "Ground Items: Displaying at locations");
		    break;
		    
		case "Setting Explorer":
		    int[] settings = Settings.getSettings();
		    if (settings.length > lastSettings.length) {
			lastSettings = Arrays.copyOf(lastSettings, settings.length);
			settingsAge = Arrays.copyOf(settingsAge, settings.length);
		    }
		    long curTime = System.currentTimeMillis();
		    long cutoffTime = curTime - TIMEOUT;
		    for (int i = 0; i < settings.length; i++) {
			if (settingsAge[i] == 0) {
			    settingsAge[i] = ((settings[i] != 0) ? cutoffTime : curTime);
			}
			if (lastSettings[i] != settings[i]) {
			    settingsAge[i] = curTime;
			    lastSettings[i] = settings[i];
			}
			if (settingsAge[i] > cutoffTime) {
			    StringDrawing.drawLine(buffer, lineIndex++, "Changed settings: " + String.format("%4d: %d", i, settings[i]));
			}
		    }
		    break;
		    
		case "Player Models":
		    try {
			Player[] loadedPlayers = Players.getLoaded();
			for (int i = 0; i < loadedPlayers.length; i++) {
			    if (loadedPlayers[i].getModel() != null) {
				loadedPlayers[i].getModel().paint(buffer);
			    }
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    StringDrawing.drawLine(buffer, lineIndex++, "Player Models: WireFraming");
		    break;
		    
		case "NPC Models":
		    try {
			NPC[] loadedNPCs = NPCs.getLoaded();
			for (int i = 0; i < loadedNPCs.length; i++) {
			    if (loadedNPCs[i] != null && loadedNPCs[i].getModel() != null) {
				loadedNPCs[i].draw(buffer);
			    }
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    StringDrawing.drawLine(buffer, lineIndex++, "NPC Models: WireFraming");
		    break;
		    
		case "Floor Decoration Models":
		    try {
			FloorDecorationSceneObject[] loadedFloorDecorObjs = FloorDecorationObjects.getLoaded();
			for (int i = 0; i < loadedFloorDecorObjs.length; i++) {
			    if (loadedFloorDecorObjs[i] != null && loadedFloorDecorObjs[i].getModel() != null) {
				loadedFloorDecorObjs[i].getModel().paint(buffer);
			    }
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    
		    break;
		case "Wall Decoration Models":
		    try {
			WallDecorationSceneObject[] loadedWallDecorObjs = WallDecorationObjects.getLoaded();
			for (int i = 0; i < loadedWallDecorObjs.length; i++) {
			    if (loadedWallDecorObjs[i] != null && loadedWallDecorObjs[i].getModel() != null) {
				loadedWallDecorObjs[i].getModel().paint(buffer);
			    }
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    
		    break;
		    
		case "Wall Object Models":
		    try {
			WallSceneObject[] loadedWallObjs = WallObjects.getLoaded();
			for (int i = 0; i < loadedWallObjs.length; i++) {
			    if (loadedWallObjs[i] != null && loadedWallObjs[i].getModel() != null) {
				loadedWallObjs[i].getModel().paint(buffer);
			    }
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    
		    break;
		    
		case "Ground Item Models":
		    try {
			GroundItem[] loadedWallObjs = com.aribot.bot.api.methods.nodes.GroundItems.getLoaded();
			for (int i = 0; i < loadedWallObjs.length; i++) {
			    if (loadedWallObjs[i] != null) {
				loadedWallObjs[i].getModel().paint(buffer);
			    }
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    
		    break;
		    
		    
		case "Floor Decorations":
		    try {
			FloorDecorationSceneObject[] loadedFloorDecs = FloorDecorationObjects.getLoaded();
			StringDrawing.drawLine(buffer, lineIndex++, "Floor Decorations loaded: " + loadedFloorDecs.length);
			for (int i = 0; i < loadedFloorDecs.length; i++) {
			    loadedFloorDecs[i].getLocation().draw(buffer, new Color(0, 50, 0, 155), Color.GREEN,
				    "ID: " + loadedFloorDecs[i].getID() + " " + loadedFloorDecs[i].getName() + " " + loadedFloorDecs[i].getLocation().toString(),
				    Arrays.toString(loadedFloorDecs[i].getActions()));
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    break;
		    
		case "Wall Decorations":
		    try {
			WallDecorationSceneObject[] loadedWallDecorObjs = WallDecorationObjects.getLoaded();
			StringDrawing.drawLine(buffer, lineIndex++, "Wall Decorations loaded: " + loadedWallDecorObjs.length);
			for (int i = 0; i < loadedWallDecorObjs.length; i++) {
			    loadedWallDecorObjs[i].getLocation().draw(buffer, new Color(0, 50, 0, 155), Color.GREEN,
				    "ID: " + loadedWallDecorObjs[i].getID() + " " + loadedWallDecorObjs[i].getName() + " " + loadedWallDecorObjs[i].getLocation().toString(),
				    Arrays.toString(loadedWallDecorObjs[i].getActions()));
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    break;
		    
		case "Wall Objects":
		    try {
			WallSceneObject[] loadedWallObjs = WallObjects.getLoaded();
			StringDrawing.drawLine(buffer, lineIndex++, "Wall Objects loaded: " + loadedWallObjs.length);
			for (int i = 0; i < loadedWallObjs.length; i++) {
			    loadedWallObjs[i].getLocation().draw(buffer, new Color(0, 50, 0, 155), Color.GREEN,
				    "ID: " + loadedWallObjs[i].getId() + " " + loadedWallObjs[i].getName() + " " + loadedWallObjs[i].getLocation().toString(),
				    Arrays.toString(loadedWallObjs[i].getActions()));
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    break;
		    
			case "Bank Items":
			    try {
				for (BankItem b : Bank.getItems()) {
				    b.draw(buffer);
				}
			    } catch (Exception e) {
				StringDrawing.drawLine(buffer, lineIndex++, "Can't Debug Bank Items");
			    }
			    break;
			    
			    
			    
	    }
	}
    }
}