package com.aribot.bot.events.events;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.util.ManifestReader;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;
import com.aribot.bot.loader.GUI.main;

public class ScriptSelector extends JFrame implements Runnable {

	private static final long serialVersionUID = 3758773769026127593L;
	private JPanel contentPane;
	private JTable table;
	public String[] headers = new String[] { "Script name", "Authors", "Version", "Description", "Integrated" };
	public int scriptCount = 0;
	static boolean softClose = false;

	public static void main(String[] arg) {
		try {
			ScriptSelector frame = new ScriptSelector();
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration());
			Dimension frameSize = frame.getSize();
			frame.setLocation((screenSize.width - screenInsets.left - screenInsets.right - frameSize.width) / 2,
					(screenSize.height - screenInsets.top - screenInsets.bottom - frameSize.height) / 2);
			softClose = false;
			frame.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					if (!softClose) {
						main.setPlayOn(true);
						main.setStopOn(false);
						main.setPauseOn(false);
					}
				}
			});

			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ScriptSelector frame = new ScriptSelector();
					Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
					Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration());
					Dimension frameSize = frame.getSize();
					frame.setLocation((screenSize.width - screenInsets.left - screenInsets.right - frameSize.width) / 2,
							(screenSize.height - screenInsets.top - screenInsets.bottom - frameSize.height) / 2);
					softClose = false;
					frame.addWindowListener(new WindowAdapter() {
						public void windowClosing(WindowEvent e) {
							if (!softClose) {
								main.setPlayOn(true);
								main.setStopOn(false);
								main.setPauseOn(false);
							}
						}
					});

					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ScriptSelector() {
		setBounds(100, 100, 500, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setTitle("Script Selector");

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);

		DefaultTableModel model;
		model = new DefaultTableModel(headers, scriptCount);
		table = new JTable(model) {

			private static final long serialVersionUID = -7058094064010153623L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);

		JMenuBar menuBar = new JMenuBar();
		contentPane.add(menuBar, BorderLayout.NORTH);

		JButton btnRefresh = new JButton("Refresh");
		menuBar.add(btnRefresh);
		btnRefresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO: Fix this mess of a script loader ;)
			}
		});

		Component horizontalGlue = Box.createHorizontalGlue();
		menuBar.add(horizontalGlue);

		final ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		final HashMap<String, String> nameLink = new HashMap<>();
		final HashMap<String, Class<?>> classLink = new HashMap<>();
		try {

			int integratedScriptsListOffset = 0;

			model.addRow(new String[] { "" });
			table.setValueAt("Willow Chopper", 0, 0);
			table.setValueAt("Perfectionist", 0, 1);
			table.setValueAt("1.0", 0, 2);
			table.setValueAt("Chops down willow trees at the Draynor bank.", 0, 3);
			table.setValueAt("true", 0, 4);
			nameLink.put("Willow Chopper", "com.aribot.bot.script.PowerChop");
			integratedScriptsListOffset++;
			// name = "Willow Chopper", authors = "Perfectionist", version = 1.00, description = "Chops down willow trees at the Draynor bank.", local = true

			model.addRow(new String[] { "" });
			table.setValueAt("Test Script", 1, 0);
			table.setValueAt("Nobody here", 1, 1);
			table.setValueAt("9000.01", 1, 2);
			table.setValueAt("This is a test script and will not do anything useful.", 1, 3);
			table.setValueAt("true", 1, 4);
			nameLink.put("Test Script", "com.aribot.bot.script.TestScript");
			integratedScriptsListOffset++;
			//name = "Test Script", description = "This is a test script and will not do anything useful.", version = 9000.01, authors = "Nobody here", local = true

			if (new File("bin/").exists()) {
				int k = 0;

				String[] files = getClasses(new File("bin/"));
				for (int i = 0; i < files.length; i++) {
					File file = new File(files[i]);
					try {
						if (file.getName().endsWith(".class") && !file.getName().contains("$")) {
							final URL src = new File("bin/").getCanonicalFile().toURI().toURL();
							final ClassLoader cl = new URLClassLoader(new URL[] { src });
							String className = file.getCanonicalPath().substring(new File("bin/").getCanonicalPath().length() + 1);
							className = className.substring(0, className.lastIndexOf('.'));
							className = className.replace(File.separatorChar, '.');
							final Class<?> clazz = cl.loadClass(className);
							if (Script.class.isAssignableFrom(clazz)) {
								final Class<? extends Script> scriptClass = clazz.asSubclass(Script.class);
								if (scriptClass.isAnnotationPresent(Manifest.class)) {
									classLink.put(scriptClass.getName(), clazz);
									HashMap<String, String> def = ManifestReader.getManiDef(scriptClass);
									nameLink.put(def.get("0"), scriptClass.getName());
									model.addRow(new String[] { "" });
									for (int j = 0; j < def.size(); j++) {
										table.setValueAt(def.get("" + j), integratedScriptsListOffset + k, j);
									}
									k++;
								}
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			} else {
				System.out.println("bin/ does not exist.");
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		JButton btnPlay = new JButton("Play");
		menuBar.add(btnPlay);
		btnPlay.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				if (nameLink.containsKey(table.getModel().getValueAt(table.getSelectedRow(), 0))) {
					try { // Local scripts
						GameLoader.getInstance().getSh().runScript(
								classLink.get(nameLink.get(table.getModel().getValueAt(table.getSelectedRow(), 0))).asSubclass(Script.class).newInstance());
					} catch (Exception e1) {
						try { // Integrated scripts
							GameLoader.getInstance().getSh().runScript(
									classLoader.loadClass(nameLink.get(table.getModel().getValueAt(table.getSelectedRow(), 0))).asSubclass(Script.class).newInstance());
						} catch (Exception e2) {
							e2.printStackTrace();
						}
					}
					softClose = true;
					setVisible(false);
					dispose();
				}
			}
		});

	}

	private static String[] getClasses(File initFolder) {
		ArrayList<String> files = new ArrayList<>();
		String[] scriptList = initFolder.list();
		for (int i = 0; i < scriptList.length; i++) {
			File file = new File(initFolder + "/" + scriptList[i]);
			if (file.isDirectory()) {
				String[] list2 = getClasses(file);
				for (int j = 0; j < list2.length; j++) {
					files.add(list2[j]);
				}
			} else {
				try {
					if (file.getName().endsWith(".class") && !file.getName().contains("$")) {
						files.add(file.getCanonicalPath());
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return files.toArray(new String[files.size()]);
	}

}
