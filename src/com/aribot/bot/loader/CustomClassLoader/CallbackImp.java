package com.aribot.bot.loader.CustomClassLoader;

import com.aribot.bot.api.internal.wrappers.Message;
import com.aribot.bot.interfaces.Callback;

public class CallbackImp implements Callback {

	@Override
	public void notifyMessage(int ID, String sender, String message) {
		Message m = new Message(ID, sender, message);
		GameLoader.getInstance().getManager().dispatchEvent(m);
	}

}
