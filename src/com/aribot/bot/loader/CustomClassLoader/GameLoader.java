package com.aribot.bot.loader.CustomClassLoader;

import java.applet.Applet;
import java.awt.Dimension;
import java.awt.Graphics;
import java.lang.reflect.Constructor;
import java.util.EventListener;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.aribot.bot.Boot;
import com.aribot.bot.Downloader;
import com.aribot.bot.api.internal.events.InputManager;
import com.aribot.bot.api.internal.events.PaintEvent;
import com.aribot.bot.events.Manager;
import com.aribot.bot.events.Pool;
import com.aribot.bot.events.Script;
import com.aribot.bot.interfaces.Client;
import com.aribot.bot.loader.Context;
import com.aribot.bot.loader.GUI.main;

public class GameLoader {
    public static HashMap<String, byte[]> clientfiles;
    public static Client client;
    static Applet loader;
    private Class<?> clientClass;

    private final PaintEvent paintEvent;
    private final Manager eventManager;
    private final InputManager im;
    private final Map<String, EventListener> listeners;
    private final Script sh;
    public static ExecutorService service = null;

    public Script getSh() {
	return sh;
    }

    public GameLoader(String startingClass) {
	Context context = new Context();
	context.putParameter("codebase", "http://oldschool" + Downloader.worldNum + ".runescape.com/");
	context.putParameter("archive", Downloader.params.get("archive"));
	context.putParameter("code", Downloader.params.get("code"));
	context.setCodeBase(Downloader.getCodeBase());
	context.setDocumentBase(Downloader.getDocumentBase());
	for (int i = 1; i <= 9; i++) {
	    context.putParameter(Integer.toString(i), Downloader.params.get(Integer.toString(i)));
	}

	try {
	    ClassLoader loader2 = Downloader.getClassLoader();
	    try {
		clientClass = loader2.loadClass(startingClass);
	    } catch (ClassNotFoundException e) {
		e.printStackTrace();
	    }
	    loader = (Applet) clientClass.newInstance();
	    setClient((Client) loader);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	sh = new Script();
	im = new InputManager();
	paintEvent = new PaintEvent();
	eventManager = new Manager();
	listeners = new TreeMap<>();
	loader.setStub(context);
	eventManager.start();
	loader.init();
	loader.start();
	loader.setPreferredSize(new Dimension(771, 531));

	main.createGUI();
	main.addGame(loader);


    }

    public static GameLoader getInstance() {
	return Boot.gl;
    }

    public Manager getManager() {
	return eventManager;
    }

    public static void bufferGraphicsDraw(Graphics g) {
	getInstance().paintEvent.graphics = g;
	getInstance().eventManager.processEvent(getInstance().paintEvent);
    }

    public static void setClient(final Client cl) {
	client = cl;
	cl.setCallback(new CallbackImp());
	service = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60L, TimeUnit.SECONDS, new SynchronousQueue<Runnable>(), new Pool(), new ThreadPoolExecutor.AbortPolicy());
    }

    public static Client getClient() {
	return client;
    }

    public static Applet getApplet() {
	return loader;
    }

    public void addListener(final Class<?> clazz) {
	final EventListener el = instantiateListener(clazz);
	listeners.put(clazz.getName(), el);
	eventManager.addListener(el);
    }

    public void removeListener(final Class<?> clazz) {
	final EventListener el = listeners.get(clazz.getName());
	listeners.remove(clazz.getName());
	eventManager.removeListener(el);
    }

    public boolean hasListener(final Class<?> clazz) {
	return clazz != null && listeners.get(clazz.getName()) != null;
    }

    private EventListener instantiateListener(final Class<?> clazz) {
	try {
	    EventListener listener;
	    try {
		final Constructor<?> constructor = clazz.getConstructor(GameLoader.class);
		listener = (EventListener) constructor.newInstance(this);
	    } catch (final Exception e) {
		listener = clazz.asSubclass(EventListener.class).newInstance();
	    }
	    return listener;
	} catch (final Exception ignored) {
	}
	return null;
    }

}
