package com.aribot.bot.loader.GUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;

import com.aribot.bot.api.internal.events.InputManager;

public class debugMenuListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("Clear all")) {

			for (String i : main.activeDebug) {
				main.inactiveDebug.add(i);
			}
			main.activeDebug.clear();

			for (JCheckBoxMenuItem[] w : main.menuDebugHashMap.values()) {
				for (JCheckBoxMenuItem men : w) {
					men.setState(false);
				}
			}

			InputManager.getTarget().getComponent(0).requestFocus();
			return;
		}

		int remIndex = main.inactiveDebug.indexOf(e.getActionCommand());
		if (remIndex == -1) {
			remIndex = main.activeDebug.indexOf(e.getActionCommand());
			if (remIndex == -1) {
				return;
			}
			main.inactiveDebug.add(e.getActionCommand());
			main.activeDebug.remove(remIndex);
			InputManager.getTarget().getComponent(0).requestFocus();
			return;
		}
		main.activeDebug.add(e.getActionCommand());
		main.inactiveDebug.remove(remIndex);
		InputManager.getTarget().getComponent(0).requestFocus();
	}
}