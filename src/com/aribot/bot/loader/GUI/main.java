package com.aribot.bot.loader.GUI;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import com.aribot.bot.Config;
import com.aribot.bot.Downloader;
import com.aribot.bot.api.internal.events.InputManager;
import com.aribot.bot.events.events.LogPane;
import com.aribot.bot.events.events.ScriptSelector;
import com.aribot.bot.loader.CustomClassLoader.GameLoader;

public class main {

	static JFrame frame;
	static JMenuBar menubar;
    static JMenu menuDebugOptions, menuDebugModelOptions, menuDebugCameraOptions, menuDebugCharacterOptions, menuDebugInteractableOptions, menuDebugItemOptions, menuDebugMenuOptions, menuDebugExplorerOptions;
	static JButton inputButton;
	static JTabbedPane tabsPane;
	static JPanel firstTab, secondTab;
	public static ArrayList<String> activeDebug = new ArrayList<>();
	public static ArrayList<String> inactiveDebug = new ArrayList<>();
	private static JMenu mnInput;
	public static JButton btnPlay;
	public static JButton btnPause;
	public static JButton btnStop;
	public static Thread powerChop;
	static boolean suspended = false;
	private static JMenu mnRendering;
	public static int selectedGraphicsDelay = 6;
	public static debugMenuListener debugMenuListener = new debugMenuListener();
	public static HashMap<Integer, JCheckBoxMenuItem[]> menuDebugHashMap = new HashMap<>();
	public static HashMap<Integer, String[]> menuDebugStringHashMap = new HashMap<>();
	public static HashMap<Integer, JMenu> menuHashMap = new HashMap<>();

    static String[] modelDebug = { "Scene Models", "Player Models", "NPC Models", "Floor Decoration Models", "Wall Decoration Models", "Wall Object Models", "Ground Item Models" };
	static String[] cameraDebug = { "Camera", "Rotation & Zoom" };
    static String[] characterDebug = { "Players", "NPC" };
    static String[] interactableDebug = { "Scene Interactables", "Floor Decorations", "Wall Decorations", "Wall Objects" };
    static String[] itemDebug = { "Item Definition", "Ground Items", "Bank Items" };
	static String[] menuDebug = { "Menu", "Menu Options" };
	static String[] explorerDebug = { "Interface Explorer", "Setting Explorer" };
    static String[] debug = { "FPS", "Mouse", "Bases", "Inventory", "Skill info", "Login Index", "Chat", "Tile Boundries", "Clear all" };


	/*public static void main(String[] args) {
		createGUI();
		frame.setVisible(true);
	}*/

	/**
	 * @wbp.parser.entryPoint
	 */
	public static void createGUI() {

		menuDebugOptions = new JMenu("Debug");
		menuDebugModelOptions = new JMenu("Model Debug");
		menuDebugCameraOptions = new JMenu("Camera Debug");
		menuDebugCharacterOptions = new JMenu("Character Debug");
	menuDebugInteractableOptions = new JMenu("Interactables Debug");
	menuDebugItemOptions = new JMenu("Item Debug");
		menuDebugMenuOptions = new JMenu("Menu Debug");
		menuDebugExplorerOptions = new JMenu("Explorers");
		menuDebugOptions.add(menuDebugModelOptions);
		menuDebugOptions.add(menuDebugCameraOptions);
		menuDebugOptions.add(menuDebugCharacterOptions);
	menuDebugOptions.add(menuDebugInteractableOptions);
	menuDebugOptions.add(menuDebugItemOptions);
		menuDebugOptions.add(menuDebugMenuOptions);
		menuDebugOptions.add(menuDebugExplorerOptions);

		menuDebugHashMap.put(0, new JCheckBoxMenuItem[debug.length]);
		menuDebugStringHashMap.put(0, debug);
		menuHashMap.put(0, menuDebugOptions);

		menuDebugHashMap.put(1, new JCheckBoxMenuItem[modelDebug.length]);
		menuDebugStringHashMap.put(1, modelDebug);
		menuHashMap.put(1, menuDebugModelOptions);

		menuDebugHashMap.put(2, new JCheckBoxMenuItem[cameraDebug.length]);
		menuDebugStringHashMap.put(2, cameraDebug);
		menuHashMap.put(2, menuDebugCameraOptions);

		menuDebugHashMap.put(3, new JCheckBoxMenuItem[characterDebug.length]);
		menuDebugStringHashMap.put(3, characterDebug);
		menuHashMap.put(3, menuDebugCharacterOptions);

	menuDebugHashMap.put(4, new JCheckBoxMenuItem[interactableDebug.length]);
	menuDebugStringHashMap.put(4, interactableDebug);
	menuHashMap.put(4, menuDebugInteractableOptions);

	menuDebugHashMap.put(5, new JCheckBoxMenuItem[itemDebug.length]);
	menuDebugStringHashMap.put(5, itemDebug);
	menuHashMap.put(5, menuDebugItemOptions);
	
	menuDebugHashMap.put(6, new JCheckBoxMenuItem[explorerDebug.length]);
	menuDebugStringHashMap.put(6, explorerDebug);
	menuHashMap.put(6, menuDebugExplorerOptions);
	
	menuDebugHashMap.put(7, new JCheckBoxMenuItem[menuDebug.length]);
	menuDebugStringHashMap.put(7, menuDebug);
	menuHashMap.put(7, menuDebugMenuOptions);


		try {
			UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceGraphiteGlassLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace();
		}
		frame = new JFrame("AriBot v" + Config.BOT_VERSION + ((Config.BETA_VERSION) ? " Beta" : ""));
		frame.setPreferredSize(new Dimension(775, 584));
		JFrame.setDefaultLookAndFeelDecorated(true);
		SwingUtilities.updateComponentTreeUI(frame);
		frame.setResizable(false);

		try {
			frame.setIconImage(ImageIO.read(Downloader.getResourceURL("resources/a_logo_bl.png"))); // "http://puu.sh/2G53i.png"
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		menubar = new JMenuBar();
		menuDebugOptions.setName("Debugger");

		for (int i = 0; i < menuDebugStringHashMap.size(); i++) {
			String[] temp = menuDebugStringHashMap.get(i);
			JCheckBoxMenuItem[] temp2 = menuDebugHashMap.get(i);
			JMenu temp3 = menuHashMap.get(i);
			for (int j = 0; j < temp.length; j++) {
				temp2[j] = new JCheckBoxMenuItem(temp[j]);
				temp3.add(temp2[j]);
				inactiveDebug.add(temp[j]);
				temp2[j].addActionListener(debugMenuListener);
			}
		}

		menuDebugOptions.setLocation(menubar.getWidth(), 0);
		menuDebugOptions.setOpaque(true);

		menubar.add(menuDebugOptions, BorderLayout.CENTER);

		frame.setJMenuBar(menubar);

		JMenu mnSkins = new JMenu("Skins");
		menubar.add(mnSkins);

		JButton mntmShow = new JButton("Log Pane");
		mntmShow.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				LogPane.redirectSystemStreams(true);
				Thread logPane = new Thread(new LogPane());
				logPane.start();
				InputManager.getTarget().getComponent(0).requestFocus();
			}

		});
		menubar.add(mntmShow);


		menubar.add(Box.createHorizontalGlue());

		mnRendering = new JMenu("Rendering");
		menubar.add(mnRendering);

		String[] render = { "SPEEDY_GRAPHICS_DELAY", "GRAPHICS_DELAY", "SLOW_GRAPHICS_DELAY", "REALLY_SLOW_GRAPHICS_DELAY", "SUPER_SLOW_GRAPHICS_DELAY" };
		ButtonGroup group = new ButtonGroup();
		JRadioButtonMenuItem[] mntmrender = new JRadioButtonMenuItem[render.length];
		for (int i = 0; i < render.length; i++) {
			mntmrender[i] = new JRadioButtonMenuItem(render[i]);
			mnRendering.add(mntmrender[i]);
			mntmrender[i].addActionListener(new renderMenuListener());
			group.add(mntmrender[i]);
		}
		group.setSelected(mntmrender[1].getModel(), true);

		mnInput = new JMenu("");

		String[] inputs = { "None", "Mouse", "Keyboard", "Both" };
		ButtonGroup inputGroup = new ButtonGroup();
		JRadioButtonMenuItem[] mntmInput = new JRadioButtonMenuItem[inputs.length];
		for (int i = 0; i < inputs.length; i++) {
			mntmInput[i] = new JRadioButtonMenuItem(inputs[i]);
			mnInput.add(mntmInput[i]);
			mntmInput[i].addActionListener(new inputMenuListener());
			inputGroup.add(mntmInput[i]);
		}
		inputGroup.setSelected(mntmInput[0].getModel(), true);

		// String[] iconURLs = { "http://puu.sh/2HEoD.png", "http://puu.sh/2HEmu.png", "http://puu.sh/2HEnP.png", "http://puu.sh/2HEm1.png" };
		String[] iconURLs = { "play.png", "pause.png", "stop.png", "settings.png" };
		ImageIcon[] icons = new ImageIcon[iconURLs.length];
		try {
			for (int i = 0; i < icons.length; i++) {
				// icons[i] = new ImageIcon(new URL(iconURLs[i]));
				icons[i] = new ImageIcon(Downloader.getResourceURL("resources/" + iconURLs[i]));
				Image img = icons[i].getImage();
				Image newimg = img.getScaledInstance(15, 15, java.awt.Image.SCALE_SMOOTH);
				icons[i] = new ImageIcon(newimg);
			}

			btnPlay = new JButton(icons[0]);
			btnPlay.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {

					Thread scriptSelector = new Thread(new ScriptSelector());
					scriptSelector.start();

					InputManager.getTarget().getComponent(0).requestFocus();
					btnStop.setEnabled(true);
					btnPlay.setEnabled(false);
				}
			});

			btnPause = new JButton(icons[1]);
			btnPause.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					// This will be later.
					suspended = !suspended;
					try {
						if (suspended) {
							powerChop.resume();
						} else {
							powerChop.interrupt();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					InputManager.getTarget().getComponent(0).requestFocus();
				}
			});
			
			btnStop = new JButton(icons[2]);
			btnStop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					GameLoader.getInstance().getSh().stopAllScripts();
					InputManager.getTarget().getComponent(0).requestFocus();
					btnPlay.setEnabled(true);
					btnStop.setEnabled(false);
				}
			});
			btnStop.setFocusable(false);
			btnPlay.setFocusable(false);
			btnPause.setFocusable(false);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		btnPause.setEnabled(false);
		btnStop.setEnabled(false);
		menubar.add(btnPlay);
		menubar.add(btnPause);
		menubar.add(btnStop);

		mnInput.setIcon(icons[3]);
		menubar.add(mnInput);

		/*
		 * String[] skins = { "Autumn", "BusinessBlackSteel", "BusinessBlueSteel", "Business", "ChallengerDeep", "CremeCoffee", "Creme", "DustCoffee", "Dust", "EmeraldDusk"
		 * , "Gemini", "GraphiteAqua", "GraphiteGlass", "Graphite", "Magellan" , "MistAqua", "MistSilver", "Moderate", "Nebula", "NebulaBrickWall", "OfficeBlue2007"
		 * , "OfficeBlack2007", "OfficeSilver2007", "Raven", "Sahara", "Twilight" };
		 */
		String[] skins = { "Autumn", "ChallengerDeep", "GraphiteGlass", "Magellan", "Nebula", "Sahara", "Raven" };
		ButtonGroup skinGroup = new ButtonGroup();
		JRadioButtonMenuItem[] mntmSkin = new JRadioButtonMenuItem[skins.length];
		for (int i = 0; i < skins.length; i++) {
			mntmSkin[i] = new JRadioButtonMenuItem(skins[i]);
			mnSkins.add(mntmSkin[i]);
			mntmSkin[i].addActionListener(new skinMenuListener());
			skinGroup.add(mntmSkin[i]);
		}
		skinGroup.setSelected(mntmSkin[2].getModel(), true);

		frame.getContentPane().setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		tabsPane = new JTabbedPane();
		tabsPane.enableInputMethods(false);
		tabsPane.setFocusable(false);
		secondTab = new JPanel(new BorderLayout());
		firstTab = new JPanel();
		tabsPane.addTab("Bot", firstTab);
		firstTab.setLayout(new BorderLayout(0, 0));
		JLabel statusLabel = new JLabel("Not quite yet. ;)", SwingConstants.CENTER);
		secondTab.add(statusLabel, BorderLayout.CENTER);
		tabsPane.addTab("+", secondTab);
		frame.getContentPane().add(tabsPane);

		menubar.setFocusable(false);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void addGame(Applet game) {
		game.setLayout(new BorderLayout());
		firstTab.add(game, BoxLayout.X_AXIS);
		frame.addWindowFocusListener(new WindowFocusListener() {
			@Override
			public void windowGainedFocus(WindowEvent arg0) {
				InputManager.getTarget().getComponent(0).requestFocus();
			}

			@Override
			public void windowLostFocus(WindowEvent e) {
			}
		});
		frame.pack();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration());
		Dimension frameSize = frame.getSize();
		frame.setLocation((screenSize.width - screenInsets.left - screenInsets.right - frameSize.width) / 2,
				(screenSize.height - screenInsets.top - screenInsets.bottom - frameSize.height) / 2);
		frame.setVisible(true);
	}

	static void redispatch(final KeyEvent e) {
		final Component c = InputManager.getTarget().getComponent(0);
		c.dispatchEvent(e);
	}

	public void appletResize(int width, int height) {
	}

	public static void setPlayOn(boolean btnPlay) {
		main.btnPlay.setEnabled(btnPlay);
	}

	public static void setPauseOn(boolean btnPause) {
		main.btnPause.setEnabled(btnPause);
	}

	public static void setStopOn(boolean btnStop) {
		main.btnStop.setEnabled(btnStop);
	}

}

class skinMenuListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.Substance" + e.getActionCommand() + "LookAndFeel");
			SwingUtilities.updateComponentTreeUI(main.frame);
		} catch (Exception e2) {
			e2.printStackTrace();
		}
		InputManager.getTarget().getComponent(0).requestFocus();
	}
}

class inputMenuListener implements ActionListener {

	private static MouseListener mouseListen;
	private static MouseMotionListener mouseMoveListen;
	private static KeyListener keyboardListen;
	private static boolean mouse = true;
	private static boolean keyboard = true;

	@Override
	public void actionPerformed(ActionEvent e) {

		try {
			switch (e.getActionCommand()) {

			case "None":
				if (!mouse) {
					InputManager.getTarget().getComponent(0).addMouseListener(mouseListen);
					InputManager.getTarget().getComponent(0).addMouseMotionListener(mouseMoveListen);
				}
				if (!keyboard) {
					InputManager.getTarget().getComponent(0).addKeyListener(keyboardListen);
				}
				InputManager.getTarget().getComponent(0).requestFocus();
				mouse = true;
				keyboard = true;

				break;
			case "Both":
				if (keyboard) {
					keyboardListen = InputManager.getTarget().getComponent(0).getKeyListeners()[0];
					InputManager.getTarget().getComponent(0).removeKeyListener(keyboardListen);
				}
				if (mouse) {
					mouseListen = InputManager.getTarget().getComponent(0).getMouseListeners()[0];
					mouseMoveListen = InputManager.getTarget().getComponent(0).getMouseMotionListeners()[0];
					InputManager.getTarget().getComponent(0).removeMouseListener(mouseListen);
					InputManager.getTarget().getComponent(0).removeMouseMotionListener(mouseMoveListen);
				}
				mouse = false;
				keyboard = false;

				break;

			case "Mouse":
				if (mouse) {
					if (!keyboard) {
						InputManager.getTarget().getComponent(0).addKeyListener(keyboardListen);
						keyboard = true;
					}
					mouseListen = InputManager.getTarget().getComponent(0).getMouseListeners()[0];
					mouseMoveListen = InputManager.getTarget().getComponent(0).getMouseMotionListeners()[0];
					InputManager.getTarget().getComponent(0).removeMouseListener(mouseListen);
					InputManager.getTarget().getComponent(0).removeMouseMotionListener(mouseMoveListen);
					mouse = false;
				}
				break;

			case "Keyboard":
				if (keyboard) {
					if (!mouse) {
						InputManager.getTarget().getComponent(0).addMouseListener(mouseListen);
						InputManager.getTarget().getComponent(0).addMouseMotionListener(mouseMoveListen);
						mouse = true;
					}
					keyboardListen = InputManager.getTarget().getComponent(0).getKeyListeners()[0];
					InputManager.getTarget().getComponent(0).removeKeyListener(keyboardListen);
					keyboard = false;
				}
				break;

			}
		} catch (Exception e2) {
			e2.printStackTrace();
		}

	}

	public static void force(boolean on) {
		if (!on) {
			if (keyboard) {
				keyboardListen = InputManager.getTarget().getComponent(0).getKeyListeners()[0];
				InputManager.getTarget().getComponent(0).removeKeyListener(keyboardListen);
			}
			if (mouse) {
				mouseListen = InputManager.getTarget().getComponent(0).getMouseListeners()[0];
				mouseMoveListen = InputManager.getTarget().getComponent(0).getMouseMotionListeners()[0];
				InputManager.getTarget().getComponent(0).removeMouseListener(mouseListen);
				InputManager.getTarget().getComponent(0).removeMouseMotionListener(mouseMoveListen);
			}
			mouse = false;
			keyboard = false;
		} else {
			if (!keyboard) {
				InputManager.getTarget().getComponent(0).addKeyListener(keyboardListen);
			}
			if (!mouse) {
				InputManager.getTarget().getComponent(0).addMouseListener(mouseListen);
				InputManager.getTarget().getComponent(0).addMouseMotionListener(mouseMoveListen);
			}
			InputManager.getTarget().getComponent(0).requestFocus();
			mouse = true;
			keyboard = true;
		}
	}

}

class renderMenuListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			switch (e.getActionCommand()) {
			case "SPEEDY_GRAPHICS_DELAY":
				main.selectedGraphicsDelay = 3;
				break;
			case "GRAPHICS_DELAY":
				main.selectedGraphicsDelay = 6;
				break;
			case "SLOW_GRAPHICS_DELAY":
				main.selectedGraphicsDelay = 50;
				break;
			case "REALLY_SLOW_GRAPHICS_DELAY":
				main.selectedGraphicsDelay = 150;
				break;
			case "SUPER_SLOW_GRAPHICS_DELAY":
				main.selectedGraphicsDelay = 500;
				break;
			}

		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
}