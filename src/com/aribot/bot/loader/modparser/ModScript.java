package com.aribot.bot.loader.modparser;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import com.aribot.bot.loader.modparser.adapters.AddFieldAdapter;
import com.aribot.bot.loader.modparser.adapters.AddGetterAdapter;
import com.aribot.bot.loader.modparser.adapters.AddInterfaceAdapter;
import com.aribot.bot.loader.modparser.adapters.AddMethodAdapter;
import com.aribot.bot.loader.modparser.adapters.InsertCodeAdapter;
import com.aribot.bot.loader.modparser.adapters.MultiplierGetAdapter;
import com.aribot.bot.loader.modparser.adapters.MultiplierLongGetAdapter;
import com.aribot.bot.loader.modparser.adapters.OverrideClassAdapter;
import com.aribot.bot.loader.modparser.adapters.SetSignatureAdapter;
import com.aribot.bot.loader.modparser.adapters.SetSuperAdapter;
import com.aribot.bot.loader.modparser.hooks.MethodHook;

public class ModScript {
    
    public static interface Opcodes {
	int ATTRIBUTE = 1;
	int GET_STATIC = 2;
	int GET_FIELD = 3;
	int ADD_FIELD = 4;
	int ADD_METHOD = 5;
	int ADD_INTERFACE = 6;
	int SET_SUPER = 7;
	int SET_SIGNATURE = 8;
	int INSERT_CODE = 9;
	int OVERRIDE_CLASS = 10;
	int ADD_MULTIPLIER = 12;
	int CHANGE_METHOD_NAME = 13;
	int ADD_MULTIPLIER_LONG = 14;
	int EOF = -1;
    }
    
    private static final int MAGIC = 0xFADFAD;
    
    private String name;
    private int version;
    private Map<String, String> attributes;
    private Map<String, ClassAdapter> adapters;
    private Map<String, ClassWriter> writers;
    private Map<String, ClassAdapter> adapters2;
    private Map<String, ClassWriter> writers2;
    public static ArrayList<MethodHook> methods = new ArrayList<>();
    
    public ModScript(final byte[] data) throws ParseException {
	this(new ByteArrayInputStream(data));
    }
    
    public ModScript(final InputStream in) throws ParseException {
	load(new Scanner(in));
    }
    
    public String getName() {
	return name;
    }
    
    public int getVersion() {
	return version;
    }
    
    public String getAttribute(final String key) {
	return attributes.get(key);
    }
    
    public byte[] process(final String key, final byte[] data) {
	final ClassAdapter adapter = adapters.get(key);
	if (adapter != null) {
	    final ClassReader reader = new ClassReader(data);
	    reader.accept(adapter, ClassReader.SKIP_FRAMES);
	    return writers.get(key).toByteArray();
	}
	return data;
    }
    
    public byte[] processMultipliers(final String key, final byte[] data) {
	final ClassAdapter adapter2 = adapters2.get(key);
	if (adapter2 != null) {
	    final ClassReader reader2 = new ClassReader(data);
	    reader2.accept(adapter2, ClassReader.SKIP_FRAMES);
	    return writers2.get(key).toByteArray();
	}
	return data;
    }
    
    public byte[] process(final String key, final InputStream is) throws IOException {
	final ClassAdapter adapter = adapters.get(key);
	if (adapter != null) {
	    final ClassReader reader = new ClassReader(is);
	    reader.accept(adapter, ClassReader.SKIP_FRAMES);
	    return writers.get(key).toByteArray();
	}
	final ByteArrayOutputStream os = new ByteArrayOutputStream();
	final byte[] buffer = new byte[4096];
	int n;
	while ((n = is.read(buffer)) != -1) {
	    os.write(buffer, 0, n);
	}
	return os.toByteArray();
    }
    
    /**
     * Loads ModScript and parses it out to give the correct visit methods.
     * 
     * @param scan The Scanner that has the ModScript file in it for reading
     * @throws ParseException If something didn't go right.
     */
    private void load(final Scanner scan) throws ParseException {
	if (scan.readInt() != ModScript.MAGIC) {
	    throw new ParseException("invalid patch format", 0);
	}
	attributes = new HashMap<>();
	adapters = new HashMap<>();
	writers = new HashMap<>();
	adapters2 = new HashMap<>();
	writers2 = new HashMap<>();
	name = scan.readString();
	version = scan.readInt();
	int num = scan.readShort();
	while (num-- > 0) {
	    final String _class;
	    int count = 0, ptr = 0;
	    final int op = scan.readByte();
	    switch (op) {
		case Opcodes.ATTRIBUTE:
		    final String key = scan.readString();
		    final String value = scan.readString();
		    attributes.put(key, new StringBuilder(value).reverse().toString());
		break;
		case Opcodes.GET_STATIC:
		case Opcodes.GET_FIELD:
		    _class = scan.readString();
		    count = scan.readShort();
		    final AddGetterAdapter.Field[] fieldsGet = new AddGetterAdapter.Field[count];
		    while (ptr < count) {
			final AddGetterAdapter.Field f = new AddGetterAdapter.Field();
			f.getAccess = scan.readInt();
			f.getName = scan.readString();
			f.getType = scan.readString();
			f.owner = scan.readString();
			f.name = scan.readString();
			f.desc = scan.readString();
			fieldsGet[ptr++] = f;
		    }
		    adapters.put(_class, new AddGetterAdapter(delegate(_class), op == Opcodes.GET_FIELD, fieldsGet));
		break;
		case Opcodes.ADD_FIELD:
		    _class = scan.readString();
		    count = scan.readShort();
		    final AddFieldAdapter.Field[] fieldsAdd = new AddFieldAdapter.Field[count];
		    while (ptr < count) {
			final AddFieldAdapter.Field f = new AddFieldAdapter.Field();
			f.access = scan.readInt();
			f.name = scan.readString();
			f.desc = scan.readString();
			fieldsAdd[ptr++] = f;
		    }
		    adapters.put(_class, new AddFieldAdapter(delegate(_class), fieldsAdd));
		break;
		case Opcodes.ADD_METHOD:
		    _class = scan.readString();
		    count = scan.readShort();
		    final AddMethodAdapter.Method[] methods = new AddMethodAdapter.Method[count];
		    while (ptr < count) {
			final AddMethodAdapter.Method m = new AddMethodAdapter.Method();
			m.access = scan.readInt();
			m.name = scan.readString();
			m.desc = scan.readString();
			final byte[] code = new byte[scan.readInt()];
			scan.readSegment(code, code.length, 0);
			m.code = code;
			m.max_locals = scan.readByte();
			m.max_stack = scan.readByte();
			methods[ptr++] = m;
		    }
		    adapters.put(_class, new AddMethodAdapter(delegate(_class), methods));
		break;
		case Opcodes.ADD_INTERFACE:
		    _class = scan.readString();
		    final String inter = scan.readString();
		    adapters.put(_class, new AddInterfaceAdapter(delegate(_class), inter));
		break;
		case Opcodes.SET_SUPER:
		    _class = scan.readString();
		    final String superName = scan.readString();
		    adapters.put(_class, new SetSuperAdapter(delegate(_class), superName));
		break;
		case Opcodes.SET_SIGNATURE:
		    _class = scan.readString();
		    count = scan.readShort();
		    final SetSignatureAdapter.Signature[] signatures = new SetSignatureAdapter.Signature[count];
		    while (ptr < count) {
			final SetSignatureAdapter.Signature s = new SetSignatureAdapter.Signature();
			s.name = scan.readString();
			s.desc = scan.readString();
			s.new_access = scan.readInt();
			s.new_name = scan.readString();
			s.new_desc = scan.readString();
			signatures[ptr++] = s;
		    }
		    adapters.put(_class, new SetSignatureAdapter(delegate(_class), signatures));
		break;
		
		case Opcodes.INSERT_CODE:
		    _class = scan.readString();
		    final String name2 = scan.readString();
		    final String desc = scan.readString();
		    count = scan.readByte();
		    final Map<Integer, byte[]> fragments = new HashMap<>();
		    while (count-- > 0) {
			final int off = scan.readShort();
			final byte[] code = new byte[scan.readInt()];
			scan.readSegment(code, code.length, 0);
			fragments.put(off, code);
		    }
		    adapters.put(_class, new InsertCodeAdapter(delegate(_class), name2, desc, fragments, scan.readByte(), scan.readByte()));
		break;
		
		case Opcodes.OVERRIDE_CLASS:
		    final String old = scan.readString();
		    final String newC = scan.readString();
		    count = scan.readByte();
		    while (count-- > 0) {
			final String current = scan.readString();
			adapters.put(current, new OverrideClassAdapter(delegate(current), old, newC));
		    }
		break;
		case Opcodes.ADD_MULTIPLIER:
		    _class = scan.readString();
		    count = scan.readShort();
		    final MultiplierGetAdapter.MultiplierHook[] multiplierAdd = new MultiplierGetAdapter.MultiplierHook[count];
		    while (ptr < count) {
			final MultiplierGetAdapter.MultiplierHook f = new MultiplierGetAdapter.MultiplierHook();
			f.classHook = scan.readString();
			f.fieldHook = scan.readString();
			f.multiplierName = scan.readString();
			f.multiplierValue = scan.readInt();
			multiplierAdd[ptr++] = f;
		    }
		    adapters2.put(_class, new MultiplierGetAdapter(delegateMultipliers(_class), multiplierAdd));
		break;
		case Opcodes.ADD_MULTIPLIER_LONG:
		    _class = scan.readString();
		    count = scan.readShort();
		    final MultiplierLongGetAdapter.MultiplierLongHook[] multiplierLongAdd = new MultiplierLongGetAdapter.MultiplierLongHook[count];
		    while (ptr < count) {
			final MultiplierLongGetAdapter.MultiplierLongHook f = new MultiplierLongGetAdapter.MultiplierLongHook();
			f.classHook = scan.readString();
			f.fieldHook = scan.readString();
			f.multiplierName = scan.readString();
			f.multiplierValue = scan.readLong();
			multiplierLongAdd[ptr++] = f;
		    }
		    adapters2.put(_class, new MultiplierLongGetAdapter(delegateMultipliers(_class), multiplierLongAdd));
		break;
		case Opcodes.CHANGE_METHOD_NAME:
		    _class = scan.readString();
		    count = scan.readShort();
		    final MethodHook[] MethodNameChange = new MethodHook[count];
		    while (ptr < count) {
			String name = scan.readString();
			String origName = scan.readString();
			String origDesc = scan.readString();
			String isStatic = scan.readString();
			final MethodHook f = new MethodHook(_class, _class, name, origName, origDesc, Boolean.parseBoolean(isStatic));
			ModScript.methods.add(f);
			MethodNameChange[ptr++] = f;
		    }
		break;
		case Opcodes.EOF:
		    return;
		default:
		
		break;
	    
	    }
	}
    }
    
    private ClassVisitor delegate(final String clazz) {
	final ClassAdapter delegate = adapters.get(clazz);
	if (delegate == null) {
	    final ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
	    writers.put(clazz, writer);
	    return writer;
	}
	return delegate;
	
    }
    
    private ClassVisitor delegateMultipliers(final String clazz) { // Must be done separately due to class refactoring and conflicts.
	final ClassAdapter delegate2 = adapters2.get(clazz);
	if (delegate2 == null) {
	    final ClassWriter writer2 = new ClassWriter(ClassWriter.COMPUTE_MAXS);
	    writers2.put(clazz, writer2);
	    return writer2;
	}
	return delegate2;
	
    }
    
}