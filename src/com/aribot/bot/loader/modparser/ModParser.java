package com.aribot.bot.loader.modparser;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import sun.misc.BASE64Decoder;

import com.aribot.bot.Config;
import com.aribot.bot.Downloader;
import com.aribot.bot.loader.modparser.adapters.Refactorer;

public class ModParser {

	public static int version = -1;
	public static HashMap<String, byte[]> modifiedJar;
	private static ModScript script;

	@SuppressWarnings("unused")
	public ModParser() {

		String mod = "";
		if (!new File("ModScript.dat").exists() || !Config.DEV_VERSION || !Config.devOverride) {
			String line2 = "";
			HttpURLConnection connection = null;

			try {
				URL url = new URL("http://aribot.org/dev/modscript.php");
				connection = (HttpURLConnection) url.openConnection();
				connection.addRequestProperty("User-Agent", "uGqWl4cEkq1ZsBtF81DL");
				connection.setUseCaches(false);
				connection.setDoOutput(true);
				connection.setDoInput(true);
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line = "";
				while ((line = in.readLine()) != null) {
					line2 = line;
				}
				in.close();
			} catch (Exception e) {
				e.printStackTrace();
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				JFrame loaderstatus = new JFrame("Site is offline");
				loaderstatus.setSize(400, 100);
				Dimension frameSize2 = loaderstatus.getSize();
				loaderstatus.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				loaderstatus.setLocation((screenSize.width - frameSize2.width) / 2, (screenSize.height - frameSize2.height) / 2);
				Container theInfo = new JPanel(new BorderLayout());
				JLabel statusLabel = new JLabel("Please wait a little bit for the site to come back online", SwingConstants.CENTER);
				theInfo.add(statusLabel = new JLabel("", SwingConstants.CENTER));
				statusLabel.setText("Please wait a little bit for the site to come back online.");
				loaderstatus.getContentPane().add(statusLabel, BorderLayout.CENTER);

				loaderstatus.setVisible(true);
				return;

			}
			connection.disconnect();

			HashMap<String, String> postData = new HashMap<>();
			// This is going to get buff'ed soon.
			postData.put("uDuUe0GxY4lqYdfTgDqw", line2.substring(42)); // 31
			postData.put("gcAchl1LKWWfglbaGity", line2.substring(9, 41));
			mod = postData(postData, "m0dscript.php");
		}

		byte[] data;
		try {
			if (new File("ModScript.dat").exists() && (Config.DEV_VERSION || Config.devOverride)) {
				System.out.println("Using Local MS.");
				Path path = Paths.get("ModScript.dat");
				data = Files.readAllBytes(path);
			} else {
				BASE64Decoder decoder = new BASE64Decoder();
				data = decoder.decodeBuffer(mod);
			}
			script = new ModScript(data);
			
			version = Integer.parseInt(Integer.toString(script.getVersion()).substring(6));
			if (Config.DEV_VERSION) {
				System.out.println(version);
			}

			String minVersion = script.getAttribute("minbotversion");

			if (Config.BOT_VERSION < Double.parseDouble(minVersion)) {
				Config.updated = false;
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				JFrame loaderstatus = new JFrame("Please update");
				loaderstatus.setSize(400, 100);
				Dimension frameSize2 = loaderstatus.getSize();
				loaderstatus.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				loaderstatus.setLocation((screenSize.width - frameSize2.width) / 2, (screenSize.height - frameSize2.height) / 2);
				Container theInfo = new JPanel(new BorderLayout());
				JLabel statusLabel = new JLabel("Please update AriBot", SwingConstants.CENTER);
				theInfo.add(statusLabel = new JLabel("", SwingConstants.CENTER));
				statusLabel.setText("Please update by going to the official AriBot website.");
				loaderstatus.getContentPane().add(statusLabel, BorderLayout.CENTER);

				loaderstatus.setVisible(true);
				return;
			}
			int liveVersion = Downloader.getLiveVersion(ModParser.version);
			System.out.println("Live: " + liveVersion + " Mod: " + ModParser.version);
			if (liveVersion != ModParser.version) {
				Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
				JFrame loaderstatus = new JFrame("RuneScape Updated");
				loaderstatus.setSize(410, 100);
				Dimension frameSize2 = loaderstatus.getSize();
				loaderstatus.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				loaderstatus.setLocation((screenSize.width - frameSize2.width) / 2, (screenSize.height - frameSize2.height) / 2);
				Container theInfo = new JPanel(new BorderLayout());
				JLabel statusLabel = new JLabel("", SwingConstants.CENTER);
				theInfo.add(statusLabel = new JLabel("", SwingConstants.CENTER));
				statusLabel.setText("<html><body><center>Please wait for the developers to update the client to a usable state.<br>Check the forums for more updates.</center></body></html>");
				loaderstatus.getContentPane().add(statusLabel, BorderLayout.CENTER);

				loaderstatus.setVisible(true);
				Config.updated = false;
				return;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void inject() {
		ModParser.modifiedJar = Downloader.jar;

		for (Map.Entry<String, byte[]> entry : ModParser.modifiedJar.entrySet()) {
			entry.setValue(script.process(entry.getKey().replace(".class", ""), entry.getValue()));
			entry.setValue(script.processMultipliers(entry.getKey().replace(".class", ""), entry.getValue()));
		}

		ModParser.modifiedJar = Refactorer.refactor(ModParser.modifiedJar);
		if (Config.DEV_VERSION) {
			saveInj("Injected.jar");
		}
	}

	private static String postData(HashMap<String, String> postData, String link) {
		HttpURLConnection connection = null;
		String ret = "";
		try {
			Set<String> keys = postData.keySet();
			Iterator<String> keyIter = keys.iterator();
			String postContent = "";
			for (int i = 0; keyIter.hasNext(); i++) {
				Object key = keyIter.next();
				if (i != 0) {
					postContent += "&"; // Don't add & at the beginning of the string
				}
				postContent += key + "=" + URLEncoder.encode(postData.get(key), "UTF-8"); // Add the key and value to the string.
			}
			// Create connection
			URL url = new URL("http://aribot.org/dev/" + link);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			connection.setRequestProperty("Content-Length", Integer.toString(postContent.getBytes().length));
			connection.addRequestProperty("User-Agent", "Ua2UeoQ8qaFQMXV0xPpy");
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			connection.setDoInput(true);

			DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
			wr.writeBytes(postContent);
			wr.flush();
			wr.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line = "";
			while ((line = in.readLine()) != null) {
				ret += line;
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		connection.disconnect();
		return ret;
	}

	public static void saveInj(String file) {
		Downloader.buildJar(modifiedJar);
		Downloader.save(new File(file));
	}

}