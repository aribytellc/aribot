package com.aribot.bot.loader.modparser.adapters;

import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public class MultiplierLongGetAdapter extends ClassAdapter implements Opcodes {

	private MultiplierLongHook[] multiHooks;

	public static class MultiplierLongHook {
		public String classHook;
		public String fieldHook;
		public String multiplierName;
		public long multiplierValue;
	}

	public MultiplierLongGetAdapter(ClassVisitor cv, MultiplierLongHook[] hook) {
		super(cv);
		this.multiHooks = hook;
	}

	public void visitEnd() {
		for (final MultiplierLongHook hook : multiHooks) {
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC, hook.multiplierName, "()J", null, null);
			mv.visitLdcInsn(hook.multiplierValue);
			mv.visitInsn(Type.getType("J").getOpcode(IRETURN));
			mv.visitMaxs(1, 1);
			mv.visitEnd();
			super.visitEnd();
		}
	}

}