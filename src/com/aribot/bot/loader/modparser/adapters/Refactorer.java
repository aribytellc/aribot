package com.aribot.bot.loader.modparser.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.commons.RemappingClassAdapter;

import com.aribot.bot.loader.modparser.ModScript;
import com.aribot.bot.loader.modparser.hooks.MethodHook;

public class Refactorer {
    
    private static Map<String, String> mapClasses = new HashMap<>();
    private static Map<String, String> mapFields = new HashMap<>();
    private static Map<String, String> mapMethods = new HashMap<>();
    private static List<String> classes = new ArrayList<>();
    private static List<String> fields = new ArrayList<>();
    private static List<String> methods = new ArrayList<>();
    
    private static Map<String, ClassAdapter> adapters = new HashMap<>();
    private static Map<String, ClassWriter> writers = new HashMap<>();
    
    private static Remapper remapper = new Remapper() {
	
	/**
	 * Maps a class to the new class name.
	 */
	@SuppressWarnings("synthetic-access")
	@Override
	public String map(String typeName) {
	    String newTypeName = mapClasses.get(typeName);
	    if (newTypeName == null) {
		newTypeName = typeName;
		while (classes.contains(newTypeName))
		    newTypeName += "_";
	    }
	    return newTypeName;
	}
	
	/**
	 * Maps a method to the new method name.
	 */
	@SuppressWarnings("synthetic-access")
	@Override
	public String mapMethodName(String owner, String name, String desc) {
	    
	    String newName = mapMethods.get(owner + name + desc);
	    if (newName == null) {
		newName = name;
		while (methods.contains(owner + newName + desc))
		    newName += "_";
	    }
	    return newName;
	}
	
    };
    
    public static void addClass(String newName, String name) {
	mapClasses.put(name, newName);
	classes.add(newName);
    }
    
    public static void addField(String newName, String owner, String name, String desc) {
	mapFields.put(owner + name + desc, newName);
	fields.add(owner + newName + desc);
    }
    
    public static void addMethod(String newName, String owner, String name, String desc) {
	mapMethods.put(owner + name + desc, newName);
	methods.add(owner + newName + desc);
    }
    
    public static void addSuper(String newName, String name) {
	adapters.put(name, new SetSuperAdapter(delegate(name), newName));
    }
    
    public static void addNewField(String fieldName, String name, String desc, int access) {
	final NewFieldAdapter.Field fieldsAdd = new NewFieldAdapter.Field();
	fieldsAdd.access = access;
	fieldsAdd.name = fieldName;
	fieldsAdd.desc = desc;
	adapters.put(name, new NewFieldAdapter(delegate(name), fieldsAdd));
    }
    
    public void clear() {
	mapClasses.clear();
	mapFields.clear();
	mapMethods.clear();
	classes.clear();
	fields.clear();
	methods.clear();
    }
    
    public static Map.Entry<String, byte[]> refactor(Map.Entry<String, byte[]> classCont) {
	try {
	    final ClassAdapter adapter = adapters.get(classCont.getKey());
	    if (adapter != null) {
		final ClassReader reader = new ClassReader(classCont.getValue());
		reader.accept(adapter, ClassReader.SKIP_DEBUG | ClassReader.SKIP_FRAMES);
		classCont.setValue(writers.get(classCont.getKey()).toByteArray());
	    }
	    
	    ClassReader cr = new ClassReader(classCont.getValue());
	    ClassWriter cw = new ClassWriter(0);
	    cr.accept(new RemappingClassAdapter(cw, remapper), 0);
	    classCont.setValue(cw.toByteArray());
	    return classCont;
	} catch (Exception e) {
	    return classCont;
	}
    }
    
    private static ClassVisitor delegate(final String clazz) {
	final ClassAdapter delegate = adapters.get(clazz);
	if (delegate == null) {
	    final ClassWriter writer = new ClassWriter(0);
	    writers.put(clazz, writer);
	    return writer;
	}
	return delegate;
	
    }
    
    public static HashMap<String, byte[]> refactor(HashMap<String, byte[]> jar) {
	Iterator<String> chi2 = jar.keySet().iterator();
	while (chi2.hasNext()) {
	    String ch2 = chi2.next();
	    Iterator<MethodHook> chi = ModScript.methods.iterator();
	    while (chi.hasNext()) {
		MethodHook ch = chi.next();
		if (ch.getParentClass().equals(ch2.replace(".class", ""))) {
		    Refactorer.addMethod(ch.getName(), ch.getParentClass(), ch.getOrigName(), ch.getOrigDesc());
		}
	    }
	}
	HashMap<String, byte[]> tempJar = new HashMap<>();
	Iterator<Entry<String, byte[]>> qwer = jar.entrySet().iterator();
	while (qwer.hasNext()) {
	    Entry<String, byte[]> qwer2 = qwer.next();
	    tempJar.put(qwer2.getKey(), Refactorer.refactor(qwer2).getValue());
	}
	return tempJar;
    }
    
}
