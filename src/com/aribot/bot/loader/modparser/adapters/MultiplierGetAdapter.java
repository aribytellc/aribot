package com.aribot.bot.loader.modparser.adapters;

import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

public class MultiplierGetAdapter extends ClassAdapter implements Opcodes {

	private MultiplierHook[] multiHooks;

	public static class MultiplierHook {
		public String classHook;
		public String fieldHook;
		public String multiplierName;
		public int multiplierValue;
	}

	public MultiplierGetAdapter(ClassVisitor cv, MultiplierHook[] hook) {
		super(cv);
		this.multiHooks = hook;
	}

	public void visitEnd() {
		for (final MultiplierHook hook : multiHooks) {
			MethodVisitor mv = cv.visitMethod(ACC_PUBLIC, hook.multiplierName, "()I", null, null); // Multipliers are always ints so we don't need a type and can hardcode it
			mv.visitLdcInsn(hook.multiplierValue);
			mv.visitInsn(Type.getType("I").getOpcode(IRETURN));
			mv.visitMaxs(1, 1);
			mv.visitEnd();
			super.visitEnd();
		}
	}

}