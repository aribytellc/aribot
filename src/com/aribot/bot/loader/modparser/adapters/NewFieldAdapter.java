package com.aribot.bot.loader.modparser.adapters;

import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassVisitor;

public class NewFieldAdapter extends ClassAdapter {

	public static class Field {
		public int access;
		public String name;
		public String desc;
	}

	private final Field field;

	public NewFieldAdapter(final ClassVisitor delegate, final Field fields) {
		super(delegate);
		this.field = fields;
	}

	@Override
	public void visitEnd() {
		cv.visitField(field.access, field.name, field.desc, null, null);
		cv.visitEnd();
	}
}
