package com.aribot.bot.loader.modparser.hooks;

public class MethodHook {
    
    private String classHook;
    
    private String _class = "";
    private String name = "";
    private String origName = "";
    private String origDesc = "";
    private boolean _static = false;
    
    public MethodHook(String string, String _class, String name, String origName, String origDesc, boolean _static) {
	this.classHook = string;
	this._class = _class;
	this.name = name;
	this.origName = origName;
	this.origDesc = origDesc;
	this._static = _static;
    }
    
    public String getClassHook() {
	return classHook;
    }
    
    public String getParentClass() {
	return _class;
    }
    
    public String getName() {
	return name;
    }
    
    public String getOrigName() {
	return origName;
    }
    
    public String getDesc() {
	return origDesc;
    }
    
    public String getOrigDesc() {
	return origDesc;
    }
    
    public boolean isStatic() {
	return _static;
    }
    
}