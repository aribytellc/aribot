package com.aribot.bot;

import java.applet.AppletContext;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Random;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;

import com.aribot.bot.loader.modparser.ModParser;

public class Downloader {

	public static HashMap<String, String> params = new HashMap<>();
	public static HashMap<String, byte[]> jar = new HashMap<>();
	protected static byte[] data = new byte[0];
	private static Random worldRand = new Random();
	public static int[] unusableWorlds = { 7, 15, 23, 24, 26, 31, 32, 39, 40, 47, 48, 54, 55, 56, 63, 64, 71, 72, 73 };
	public static int worldNum = getValidWorld();
	private int fileSize = 1;

	public Downloader() {

		parseParameters();

		try {
			load(new URL(getUrl()));
			update();
			if (Config.DEV_VERSION) {
				save(new File("RS.jar"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static int getValidWorld() {
		int tempWorld = 1 + worldRand.nextInt(77);
		Arrays.sort(unusableWorlds);
		while (Arrays.binarySearch(unusableWorlds, tempWorld) >= 0) {
			System.out.println("In unusable world list: " + tempWorld + " " + Arrays.binarySearch(unusableWorlds, tempWorld));
			tempWorld = 1 + worldRand.nextInt(77);
			System.out.println("New world: " + tempWorld);
		}
		System.out.println("Final world: " + tempWorld);
		return tempWorld;
	}

	private static ClassLoader classLoader = new ClassLoader() {

		@Override
		public Class<?> loadClass(String name) throws ClassNotFoundException {

			byte[] data = ModParser.modifiedJar.get(name.replace('.', '/') + ".class");
			if (data != null) {
				return defineClass(name, data, 0, data.length);
			}
			return super.loadClass(name);
		}

	};

	public static ClassLoader getClassLoader() {
		return classLoader;
	}

	public boolean load(InputStream stream) {
		try {
			byte[] data = new byte[0];
			byte[] oldData;
			byte[] buffer = new byte[1024];
			int size = 0;
			double speed = 0.00;
			int additive = 0;
			long start = System.currentTimeMillis();
			while ((size = stream.read(buffer)) > 0) {
				oldData = data;
				data = new byte[oldData.length + size];
				System.arraycopy(oldData, 0, data, 0, oldData.length);
				System.arraycopy(buffer, 0, data, oldData.length, size);
				int percent = (int) Math.round(((double) data.length / fileSize) * 100);
				if (System.currentTimeMillis() - start >= 1000) {
					speed = ((double) additive / 1024);
					additive = 0;
					start = System.currentTimeMillis();
				} else {
					additive += (data.length - oldData.length);
				}
				Boot.progressBar.setValue(percent);
				Boot.progressBar.setString(percent + "% - " + Math.round(((double) data.length / 1024)) + " KB / " + Math.round(((double) fileSize / 1024)) + " KB - " + String.format(
						"%1$.2f", speed) + " KB/s");
			}
			Downloader.data = data;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@SuppressWarnings("resource")
	public boolean load(URL url) {
		try {
			long start = System.currentTimeMillis();
			System.out.println("Start: " + (System.currentTimeMillis() - start));
			InputStream is;
			if (Config.DEV_VERSION && new File("RS.jar").exists()) {
				URL file = new File("RS.jar").toURI().toURL();
				fileSize = file.openConnection().getContentLength();
				is = file.openStream();
			} else {
				fileSize = url.openConnection().getContentLength();
				is = url.openStream();
			}
			boolean result = load(is);
			is.close();
			System.out.println("End: " + (System.currentTimeMillis() - start));
			return result;
		} catch (Exception e) {
			return false;
		}
	}

	@SuppressWarnings("resource")
	void update() {
		if (data.length == 0) {
			jar.clear();
			return;
		}
		try {
			ByteArrayInputStream bias = new ByteArrayInputStream(data);
			JarInputStream jis = new JarInputStream(bias);
			jar.clear();
			JarEntry je = null;
			while ((je = jis.getNextJarEntry()) != null) {
				byte[] data = new byte[0];
				byte[] oldData;
				byte[] buffer = new byte[512];
				int size = 0;
				while ((size = jis.read(buffer)) > 0) {
					oldData = data;
					data = new byte[oldData.length + size];
					System.arraycopy(oldData, 0, data, 0, oldData.length);
					System.arraycopy(buffer, 0, data, oldData.length, size);
				}
				jar.put(je.getName(), data);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("resource")
	public static boolean buildJar(HashMap<String, byte[]> asdf) {
		try {
			ByteArrayOutputStream bios = new ByteArrayOutputStream();
			JarOutputStream jos = new JarOutputStream(bios);
			Iterator<Entry<String, byte[]>> it = asdf.entrySet().iterator();
			while (it.hasNext()) {
				Entry<String, byte[]> e = it.next();
				JarEntry je = new JarEntry(e.getKey());
				jos.putNextEntry(je);
				jos.write(e.getValue());
			}
			jos.close();
			bios.close();
			data = bios.toByteArray();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@SuppressWarnings("resource")
	public static boolean save(File file) {
		try {
			FileOutputStream fos = new FileOutputStream(file);
			boolean result = save(fos);
			return result;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean save(OutputStream stream) {
		try {
			stream.write(data);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public final static URL getCodeBase() {
		try {
			return new URL(getCodeBaseParam());
		} catch (Exception e) {
			return null;
		}
	}

	public static final URL getDocumentBase() {
		try {
			return new URL(getCodeBaseParam());
		} catch (Exception e) {
			return null;
		}
	}

	public static final String getParameter(String name) {
		return params.get(name);
	}

	@SuppressWarnings("static-method")
	public final AppletContext getAppletContext() {
		return null;
	}

	@SuppressWarnings("resource")
	void parseParameters() {
		try {
			//System.out.println("------Parsing--Site------");
			URL rsserver = new URL("http://oldschool" + worldNum + ".runescape.com/jav_config.ws");
			// URL rsserver = new URL("http://world" + worldNum + ".runescape.com/jav_config.ws"); // EOC Servers. For speed test with the length of time that it takes for the download

			URLConnection connection = null;
			connection = rsserver.openConnection();
			connection.addRequestProperty("User-Agent", "Opera/9.80 (Windows NT 6.0; U; en) Presto/2.10.229 Version/12.50");

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				inputLine = inputLine.replaceAll("\">'", "\"").replaceAll("'", "").replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\"", "").replaceAll(
						" ", "").replaceAll("param=", "").replaceAll(";", "").replaceAll("value", "");
				String[] splitted = inputLine.split("=");
				if (splitted.length == 1) {
					addParam(splitted[0]);
				} else if (splitted.length == 2) {
					addParam(splitted[0], splitted[1]);
				} else if (splitted.length == 3) {
					addParam(splitted[0], splitted[1] + "=" + splitted[2]);
				} else if (splitted.length == 4) {
					addParam(splitted[0], splitted[1] + "=" + splitted[2] + "=" + splitted[3]);
				}
				//System.out.println(inputLine);
			}
			in.close();
		} catch (Exception e) {
			//System.out.println("Error parsing parameters.");
			//System.out.println("Error " + e);
			//System.out.println("");
			return;
		}
		//System.out.println("---Done--Parsing--Site---");
		//System.out.println("");
	}

	void addParam(final String str1) {
		addParam(str1, "");
	}

	void addParam(final String str1, final String str2) {
		params.put(str1, str2);
	}

	String getUrl() throws Exception {
		return params.get("codebase") + params.get("initial_jar");
	}

	String getStartCode() throws Exception {
		return params.get("initial_class").replaceAll("\\.class", "");
	}

	static String getCodeBaseParam() throws Exception {
		return params.get("codebase");
	}

	public static int getLiveVersion(int start) {
		String host = "oldschool" + Downloader.worldNum + ".runescape.com";
		for (int i = start; i < start + 5; i++) {
			try (Socket sock = new Socket(host, 43594)) {
				byte[] check = new byte[] { 15, 0, 0, ((byte) (i >> 8)), ((byte) i) };
				sock.getOutputStream().write(check, 0, check.length);
				if (sock.getInputStream().read() == 0) {
					sock.close();
					return i;
				}
			} catch (Exception e) {
			}
		}
		return -1;
	}

	public static URL getResourceURL(final String path) throws MalformedURLException {
		boolean isJar = false;
		try {
			String path2 = Downloader.class.getProtectionDomain().getCodeSource().getLocation().getPath();
			String decodedPath = URLDecoder.decode(path2, "UTF-8");
			if (decodedPath.endsWith(".jar")) {
				isJar = true;
			}
		} catch (Exception e) {
		}
		return ((isJar) ? Boot.class.getResource("/" + path) : new File("src/" + path).toURI().toURL());
	}

}