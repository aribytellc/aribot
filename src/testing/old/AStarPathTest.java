package testing.old;
import java.awt.Graphics;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.methods.walking.Path;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.Tile;

@Manifest(authors="Xundir", name="AStar Path Test")
public class AStarPathTest extends Script implements Painter {

    private Tile[] path = null;
    
    @Override
    protected boolean onStart() {
	Tile start = Players.getMyPlayer().getLocation();
	Tile finish = new Tile(8402, 4699, 1);
	path = Path.generatePath(start, finish).toTileArray();
	return true;
    }

    @Override
    protected int loop() {
	return 0;
    }

    @Override
    protected void onFinish() {
    }
    
    @Override
    public void onPaint(Graphics graphics) {
	if (path != null) {
	    for (Tile t : path) {
		t.draw(graphics);
	    }
	}
    }

    
}
