package testing.old;
import java.awt.Graphics;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.interactables.WallObjects;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.api.wrappers.Tile.Flag;
import com.aribot.bot.api.wrappers.WallSceneObject;

@Manifest(authors = "Xundir", name = "Wall Object")
public class NPCIds extends Script implements Painter {
    
    @Override
    protected boolean onStart() {
	return true;
    }
    
    @Override
    protected void onFinish() {
    }
    
    @Override
    protected int loop() {
	WallSceneObject door = WallObjects.getNearest(3645);
	Tile t = door.getLocation();
	System.out.println("Blocked at N? " + t.isBlockedAt(Flag.N));
	System.out.println("Blocked at S? " + t.isBlockedAt(Flag.S));
	System.out.println("Blocked at E? " + t.isBlockedAt(Flag.E));
	System.out.println("Blocked at W? " + t.isBlockedAt(Flag.W));
	return 1000;
    }
    
    @Override
    public void onPaint(Graphics graphics) {
    }
    
}
