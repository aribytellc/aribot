package testing.old;
import com.aribot.bot.api.methods.Camera;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.interactables.NPCs;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.NPC;

@Manifest(authors="Xundir", name="Interact Test")
public class InteractTest extends Script {

	@Override
	protected boolean onStart() {
		return true;
	}

	@Override
	protected void onFinish() {
	}

	@Override
	protected int loop() {
		NPC man = NPCs.getNearest("Man");
		if (man != null) {
			if (man.isOnScreen()) {
				man.interact("Examine", "Man");
			}
			else {
				Camera.turnTo(man);
			}
		}
		return 500;
	}

}
