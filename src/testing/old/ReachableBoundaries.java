package testing.old;
import java.awt.Graphics;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.Tiles;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.Tile;

@Manifest(authors="Xundir", name="Reachable Boundaries Checker")
public class ReachableBoundaries extends Script implements Painter {
    
    @Override
    protected boolean onStart() {
	return true;
    }
    
    @Override
    protected void onFinish() {
    }
    
    @Override
    protected int loop() {
	for (Tile t : Tiles.getLoaded()) {
	    if (t.isDiagonalTo(Players.getMyPlayer().getLocation())) {
	    }
	}
	return 5000;
    }
    
    @Override
    public void onPaint(Graphics graphics) {
	    for (Tile t : Tiles.getLoaded(new Filter<Tile>() {
		@Override
		public boolean apply(Tile t) {
		    Tile me = Players.getMyPlayer().getLocation();
		    return t.isAdjacentTo(me) && me.canWalkTo(t);
		}
	    })) {
		t.draw(graphics);
	    }
	}
    
    
    
}
