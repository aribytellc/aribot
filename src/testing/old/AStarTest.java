package testing.old;
import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.Tiles;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.Tile;


@Manifest(authors="Xundir", name="AStarTest")
public class AStarTest extends Script implements Painter {
    
    private HashMap<Tile, String> map = new HashMap<>();
    
    @Override
    protected boolean onStart() {
	try {
	    Tile[] tiles = Tiles.getLoaded(new Filter<Tile>() {
		@Override
		public boolean apply(Tile t) {
		    return t.isOnScreen() && Calculations.distanceBetween(Players.getMyPlayer().getLocation(), t) <= 5;
		}
	    });
	    System.out.println("Loaded tiles: " + tiles.length);
	    for (Tile t : tiles) {
		map.put(t, ""+t.canReach());
	    }
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return true;
    }
    
    @Override
    protected void onFinish() {
    }
    
    @Override
    protected int loop() {
	return 100;
    }
    
    @Override
    public void onPaint(Graphics graphics) {
	for (Tile t : map.keySet()) {
	    t.draw(graphics, null, Color.white, ""+map.get(t));
	}
    }
    
    
    
    
}
