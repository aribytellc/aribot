package testing.old;
import com.aribot.bot.api.methods.Camera;
import com.aribot.bot.api.methods.Camera.Angle;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.util.Manifest;

@Manifest(authors="Xundir", name="Camera Test")
public class CameraTest extends Script {

    @Override
    protected boolean onStart() {
	Camera.setAngle(Angle.NORTHEAST);
	return false;
    }

    @Override
    protected void onFinish() {
    }

    @Override
    protected int loop() {
	return 0;
    }
    
}
