package testing.old;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Polygon;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.input.Mouse;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.util.Manifest;

@Manifest(authors="Xundir", name="Model Viewer")
public class Models extends Script implements Painter {

    @Override
    protected boolean onStart() {
	
	return true;
    }

    @Override
    protected void onFinish() {
    }

    @Override
    protected int loop() {
	System.out.println(Players.getMyPlayer().getModel().contains(Mouse.getLocation()));
	return 1000;
    }
    
    @Override
    public void onPaint(Graphics graphics) {
	graphics.setColor(Color.white);
	for (Polygon p : Players.getMyPlayer().getModel().getTriangles()) {
	    graphics.drawPolygon(p);
	}
    }
    
}
