package testing.old;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.Tile;

@Manifest(authors="Xundir", name="Tile Grabber")
public class TileList extends Script {

	private ArrayList<Tile> tiles = new ArrayList<>();

	@Override
	protected boolean onStart() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				JFrame frame = new JFrame("Tiles");

				JButton add = new JButton("Add Tile");
				{
					add.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							System.out.println("Added: " + Players.getMyPlayer().getLocation());
							tiles.add(Players.getMyPlayer().getLocation());
						}
					});
				}
				
				JButton remove = new JButton("Remove Tile");
				{
					remove.addActionListener(new ActionListener() {
						
						@Override
						public void actionPerformed(ActionEvent e) {
							System.out.println("Removed: " + tiles.remove(tiles.size()-1));
						}
					});
				}
				
				frame.getContentPane().setLayout(new BorderLayout());
				frame.getContentPane().add(add, BorderLayout.NORTH);
				frame.getContentPane().add(remove, BorderLayout.SOUTH);
				frame.pack();
				frame.setAlwaysOnTop(true);
				frame.setLocationRelativeTo(null);
				frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
				frame.setVisible(true);
			}
		});
		return true;
	}

	@Override
	protected void onFinish() {
		if (tiles.size() > 0) { 
			
			System.out.println("Tile[] = { ");
			for (Tile t : tiles) {
				System.out.println("new Tile(" + t.getX() + ", " + t.getY() + ", " + t.getZ() + "),");
			}
			System.out.println("};");
			
			Tile start = tiles.get(0);
			System.out.println("x: " + getX(start));
			System.out.println("y: " + getY(start));
		}
	}
	
	String getX(Tile start) {
		String t = "";
		for (int i = 1; i < tiles.size(); i++) {
			t += (tiles.get(i).getX() - start.getX());
			if (i != tiles.size() -1) {
				t += ", ";
			}
		}
		return t;
	}
	
	String getY(Tile start) {
		String t = "";
		for (int i = 1; i < tiles.size(); i++) {
			t += (tiles.get(i).getY() - start.getY());
			if (i != tiles.size() -1) {
				t += ", ";
			}
		}
		return t;
	}

	@Override
	protected int loop() {
		return 5000;
	}

}
