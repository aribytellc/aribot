package testing.old;
import java.awt.Graphics;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.api.wrappers.Tile.Flag;


@Manifest(authors="Xundir", name="Tile Properties Check")
public class TilePropertiesCheck extends Script implements Painter {
    
    @Override
    protected boolean onStart() {
	return true;
    }
    
    @Override
    protected void onFinish() {
    }
    
    @Override
    protected int loop() {
	Tile t = Players.getMyPlayer().getLocation();
	System.out.println("Walkable: " + t.isWalkable());
	System.out.println("Special: " + t.isSpecial());
	System.out.println("Questionable: " + t.isQuestionable());
	System.out.println(t.isBlockedAt(Flag.NW) + "\t" + t.isBlockedAt(Flag.N) + "\t" + t.isBlockedAt(Flag.NE));
	System.out.println(t.isBlockedAt(Flag.W) + "\t" + "false" + "\t" + t.isBlockedAt(Flag.E));
	System.out.println(t.isBlockedAt(Flag.SW) + "\t" + t.isBlockedAt(Flag.S) + "\t" + t.isBlockedAt(Flag.SE));
	System.out.println();
	System.out.println(t.canWalkTo(t.derive(-1, 1)) + "\t" + t.canWalkTo(t.derive(0, 1)) + "\t" + t.canWalkTo(t.derive(1, 1)));
	System.out.println(t.canWalkTo(t.derive(-1, 0)) + "\t" + "true" + "\t" + t.canWalkTo(t.derive(1, 0)));
	System.out.println(t.canWalkTo(t.derive(-1, -1)) + "\t" + t.canWalkTo(t.derive(0, -1)) + "\t" + t.canWalkTo(t.derive(1, -1)));
	return 10000;
    }
    
    @Override
    public void onPaint(Graphics graphics) {
    }
    
}
