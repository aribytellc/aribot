package testing.old;
import java.awt.Graphics;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.Tiles;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.Tile;


@Manifest(authors="Xundir", name="Walkable Tiles")
public class WalkableTiles extends Script implements Painter {

	@Override
	protected boolean onStart() {
		return true;
	}

	@Override
	protected void onFinish() {
	}

	@Override
	protected int loop() {
		return 1000;
	}

	@Override
	public void onPaint(Graphics graphics) {
		Tile[] loaded = Tiles.getLoaded();
		for (Tile t : loaded) {
			if (t.isQuestionable()) {
				t.draw(graphics);
			}
		}
	}

}
