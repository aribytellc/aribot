package testing;
import java.awt.Graphics;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Calculations;
import com.aribot.bot.api.methods.Camera;
import com.aribot.bot.api.methods.Camera.Angle;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.methods.interactables.Interactables;
import com.aribot.bot.api.methods.interactables.Players;
import com.aribot.bot.api.methods.interactables.WallObjects;
import com.aribot.bot.api.methods.walking.Walking;
import com.aribot.bot.api.util.Condition;
import com.aribot.bot.api.util.Filter;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.util.Random;
import com.aribot.bot.api.util.Time;
import com.aribot.bot.api.wrappers.Interactable;
import com.aribot.bot.api.wrappers.Tile;
import com.aribot.bot.api.wrappers.Tile.Flag;
import com.aribot.bot.api.wrappers.WallSceneObject;
import com.aribot.bot.api.wrappers.Widget;

/**
 * Obsolete. Offsets do not work
 */
@Manifest(authors="Xundir", name="Maze Random")
public class MazeRandom extends Script implements Painter {
    
    private int doorId = -1;
    private WallSceneObject nextDoor;
    private HashSet<Tile> blacklist = new HashSet<>();
    
    public boolean shouldActivate() {
	return Interactables.getNearest("Strange shrine") != null;
    }
    
    @Override
    public boolean onStart() {
	blacklist.clear();
	return (doorId = getDoorId()) != -1;
    }
    
    @Override
    public int loop() {
	Camera.setPitch(true);
	Interactable shrine = Interactables.getNearest("Strange shrine");
	if (shrine != null) { //activate condition, needs to be there
	    //Start finding doors
	    if (Calculations.distanceBetween(Players.getMyPlayer().getLocation(), shrine.getLocation()) != 1) {
		
		nextDoor = WallObjects.getNearest(new Filter<WallSceneObject>() {
		    public boolean apply(WallSceneObject t) {
			return t.getId() == doorId && !blacklist.contains(t.getLocation()) && canReach(t) != null;
		    }});
		if (nextDoor != null) {
		    Tile reachable = canReach(nextDoor);
		    if (!blacklist.contains(nextDoor.getLocation())) {
			if (!nextDoor.getLocation().isOnScreen()) {
			    Walking.walkTileMM(reachable);
			}
			Camera.setAngle(getAngleFor(nextDoor.getLocation()));
			if (nextDoor.interact("Open")) {
			    Time.sleep(2000);
			    if (Time.sleepUntil(new Condition() {
				@Override
				public boolean meet() {
				    return !Players.getMyPlayer().isMoving();
				}
			    }, 5000)) {
				boolean bl = true;
				Widget parent = Widgets.getParent(210);
				if (parent != null) {
				    Widget c = Widgets.getInterface(210, 0);
				    if (c != null && !c.isHidden() && c.getText().equalsIgnoreCase("I can't open that!")) {
					bl = false;
				    }
				}
				if (bl)
				    blacklist.add(nextDoor.getLocation());
			    }
			}
		    }
		}
		else {
		    System.out.println("Can't find a door to walk to");
		}
	    }
	    else {
		System.out.println("Ready to get ourself out of here.");
		if (shrine.isOnScreen()) {
		    if (shrine.interact("Touch")) {
			Time.sleepUntil(new Condition() {
			    @Override
			    public boolean meet() {
				return Interactables.getNearest("Strange shrine") == null;
			    }
			}, 15000);
		    }
		}
		else {
		    Camera.turnTo(shrine);
		}
	    }
	}
	return 500;
    }
    
    @Override
    protected void onFinish() {
    }
    
    private boolean checkForBadDoor() {
	return false;
    }
    
    private static Tile canReach(WallSceneObject door) {
	for (Tile t : new Tile[] { door.getLocation(), getOtherSideFor(door) }) {
	    if (t.canReach()) {
		return t;
	    }
	}
	return null;
    }
    
    private static Tile getOtherSideFor(WallSceneObject door) {
	Tile doorTile = door.getLocation();
	Tile otherSide = doorTile;
	if (doorTile.isBlockedAt(Flag.N)){
	    otherSide = doorTile.derive(0, 1);
	}
	else if (doorTile.isBlockedAt(Flag.S)) {
	    otherSide = doorTile.derive(0, -1);
	}
	else if (doorTile.isBlockedAt(Flag.E)) {
	    otherSide = doorTile.derive(1, 0);
	}
	else if (doorTile.isBlockedAt(Flag.W)) {
	    otherSide = doorTile.derive(-1, 0);
	}
	else {
	    System.err.println("This door is not blocked at NSWE");
	    return null;
	}
	return otherSide;
    }
    
    private static int getAngleFor(Tile doorTile) {
	if (doorTile.isBlockedAt(Flag.N) || doorTile.isBlockedAt(Flag.S)) {
	    return Random.nextInt(0, 1) == 0 ? Angle.NORTH.degrees : Angle.SOUTH.degrees;
	}
	return Random.nextInt(0, 1) == 0 ? Angle.EAST.degrees : Angle.WEST.degrees;
    }
    
    private static int getDoorId() {
	WallSceneObject[] loaded = WallObjects.getLoaded();
	HashMap<Integer, Integer> countMap = new HashMap<>(); //id, count
	for (WallSceneObject o : loaded) {
	    if (!countMap.keySet().contains(o.getId())) {
		countMap.put(o.getId(), 1);
	    }
	    else {
		countMap.put(o.getId(), countMap.get(o.getId()) + 1);
	    }
	}
	int value = thirdHighest(countMap.values());
	for (int key : countMap.keySet()) {
	    if (countMap.get(key) == value) {
		System.out.println("Door ID: " + key);
		return key;
	    }
	}
	return -1;
    }
    
    private static int thirdHighest(Collection<Integer> counts) {
	counts.remove(highest(counts));
	counts.remove(highest(counts));
	return highest(counts);
    }
    
    private static int highest(Collection<Integer> counts) {
	int highest = 0;
	for (int i : counts) {
	    if (i > highest) {
		highest = i;
	    }
	}
	return highest;
    }
    
    @Override
    public void onPaint(Graphics graphics) {
	if (nextDoor != null) {
	    nextDoor.getModel().paint(graphics);
	}
    }
    
}
