package testing;

import java.awt.Graphics;

import com.aribot.bot.api.internal.wrappers.Painter;
import com.aribot.bot.api.methods.Script;
import com.aribot.bot.api.methods.Widgets;
import com.aribot.bot.api.util.Manifest;
import com.aribot.bot.api.wrappers.Widget;

@Manifest(authors="Xundir", name="Chatbox Text View")
public class ChatboxText extends Script implements Painter {
    
    @Override
    protected boolean onStart() {
	return true;
    }
    
    @Override
    protected void onFinish() {
    }
    
    @Override
    protected int loop() {
	return 10000;
    }
    
    @Override
    public void onPaint(Graphics graphics) {
	try {
	    Widget chatbox = Widgets.getInterface(210, 0);
	    if (chatbox != null)
		graphics.drawString("Chatbox text: " + chatbox.getText(), 20, 100);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
    
}
